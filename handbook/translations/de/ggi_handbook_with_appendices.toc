\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{3}{section.1}%
\contentsline {subsection}{\numberline {1.1}Kontext}{3}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Über die Good Governance Initiative}{4}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Über die OSPO Alliance}{4}{subsection.1.3}%
\contentsline {subsection}{\numberline {1.4}Übersetzungen}{5}{subsection.1.4}%
\contentsline {subsection}{\numberline {1.5}Beitragende}{5}{subsection.1.5}%
\contentsline {subsection}{\numberline {1.6}Lizenz}{5}{subsection.1.6}%
\contentsline {section}{\numberline {2}Organisation}{7}{section.2}%
\contentsline {subsection}{\numberline {2.1}Fachwortschatz}{7}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Ziele}{7}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Anerkannte Aktivitäten}{8}{subsection.2.3}%
\contentsline {subsubsection}{Beschreibung}{8}{section*.2}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{8}{section*.3}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{8}{section*.4}%
\contentsline {subsubsection}{Werkzeuge}{8}{section*.5}%
\contentsline {subsubsection}{Empfehlungen}{8}{section*.6}%
\contentsline {subsubsection}{Hilfsmittel}{8}{section*.7}%
\contentsline {subsection}{\numberline {2.4}Angepasste Aktivitäts-Scorecard}{9}{subsection.2.4}%
\contentsline {section}{\numberline {3}Methodologie}{10}{section.3}%
\contentsline {subsection}{\numberline {3.1}Die Kulisse bereiten}{10}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Arbeitsablauf}{10}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Manuelle Einrichtung: Angepasste Aktivitäts-Scorecards verwenden}{11}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Automatische Einrichtung: mit der GGI-Bereitstellungsfunktion}{12}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Genießen}{14}{subsection.3.5}%
\contentsline {section}{\numberline {4}Verwendungsziel-Aktivitäten}{15}{section.4}%
\contentsline {subsection}{\numberline {4.1}Verzeichnis der Open-Source-Kompetenzen und -Mittel}{15}{subsection.4.1}%
\contentsline {subsubsection}{Beschreibung}{15}{section*.8}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{15}{section*.9}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{15}{section*.10}%
\contentsline {subsubsection}{Werkzeuge}{16}{section*.11}%
\contentsline {subsubsection}{Empfehlungen}{16}{section*.12}%
\contentsline {subsubsection}{Hilfsmittel}{16}{section*.13}%
\contentsline {subsection}{\numberline {4.2}Steigerung der Open-Source-Kompetenz}{16}{subsection.4.2}%
\contentsline {subsubsection}{Beschreibung}{16}{section*.14}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{17}{section*.15}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{17}{section*.16}%
\contentsline {subsubsection}{Werkzeuge}{17}{section*.17}%
\contentsline {subsubsection}{Empfehlungen}{17}{section*.18}%
\contentsline {subsubsection}{Hilfsmittel}{18}{section*.19}%
\contentsline {subsection}{\numberline {4.3}Open-Source-Überwachung}{18}{subsection.4.3}%
\contentsline {subsubsection}{Beschreibung}{18}{section*.20}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{18}{section*.21}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{18}{section*.22}%
\contentsline {subsubsection}{Werkzeuge}{19}{section*.23}%
\contentsline {subsubsection}{Empfehlungen}{19}{section*.24}%
\contentsline {subsubsection}{Hilfsmittel}{19}{section*.25}%
\contentsline {subsection}{\numberline {4.4}Geschäftliche Open-Source-Software}{19}{subsection.4.4}%
\contentsline {subsubsection}{Beschreibung}{19}{section*.26}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{19}{section*.27}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{20}{section*.28}%
\contentsline {subsubsection}{Werkzeuge}{20}{section*.29}%
\contentsline {subsubsection}{Empfehlungen}{20}{section*.30}%
\contentsline {subsubsection}{Hilfsmittel}{20}{section*.31}%
\contentsline {subsection}{\numberline {4.5}Verwalten von Open-Source-Fähigkeiten und -Ressourcen}{20}{subsection.4.5}%
\contentsline {subsubsection}{Beschreibung}{20}{section*.32}%
\contentsline {paragraph}{Anwendungsbereiche}{20}{section*.33}%
\contentsline {paragraph}{Kategorien}{21}{section*.34}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{21}{section*.35}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{21}{section*.36}%
\contentsline {subsubsection}{Empfehlungen}{21}{section*.37}%
\contentsline {subsubsection}{Hilfsmittel}{21}{section*.38}%
\contentsline {section}{\numberline {5}Vertrauensziel-Aktivitäten}{23}{section.5}%
\contentsline {subsection}{\numberline {5.1}Einhaltung von Rechtsvorschriften verwalten}{23}{subsection.5.1}%
\contentsline {subsubsection}{Beschreibung}{23}{section*.39}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{23}{section*.40}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{23}{section*.41}%
\contentsline {subsubsection}{Werkzeuge}{24}{section*.42}%
\contentsline {subsubsection}{Empfehlungen}{24}{section*.43}%
\contentsline {subsubsection}{Hilfsmittel}{25}{section*.44}%
\contentsline {subsection}{\numberline {5.2}Verwalten von Softwareschwachstellen}{25}{subsection.5.2}%
\contentsline {subsubsection}{Beschreibung}{25}{section*.45}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{25}{section*.46}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{26}{section*.47}%
\contentsline {subsubsection}{Werkzeuge}{26}{section*.48}%
\contentsline {subsubsection}{Hilfsmittel}{26}{section*.49}%
\contentsline {subsection}{\numberline {5.3}Verwalten von Softwareabhängigkeiten}{27}{subsection.5.3}%
\contentsline {subsubsection}{Beschreibung}{27}{section*.50}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{27}{section*.51}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{27}{section*.52}%
\contentsline {subsubsection}{Werkzeuge}{27}{section*.53}%
\contentsline {subsubsection}{Empfehlungen}{28}{section*.54}%
\contentsline {subsubsection}{Hilfsmittel}{28}{section*.55}%
\contentsline {subsection}{\numberline {5.4}Verwalten von Kennzahlen}{28}{subsection.5.4}%
\contentsline {subsubsection}{Beschreibung}{28}{section*.56}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{29}{section*.57}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{29}{section*.58}%
\contentsline {subsubsection}{Werkzeuge}{29}{section*.59}%
\contentsline {subsubsection}{Empfehlungen}{29}{section*.60}%
\contentsline {subsubsection}{Hilfsmittel}{30}{section*.61}%
\contentsline {subsection}{\numberline {5.5}Codeüberprüfungen durchführen}{30}{subsection.5.5}%
\contentsline {subsubsection}{Beschreibung}{30}{section*.62}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{30}{section*.63}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{31}{section*.64}%
\contentsline {subsubsection}{Empfehlungen}{31}{section*.65}%
\contentsline {subsubsection}{Hilfsmittel}{31}{section*.66}%
\contentsline {section}{\numberline {6}Kulturziel-Aktivitäten}{32}{section.6}%
\contentsline {subsection}{\numberline {6.1}Förderung bewährter Verfahrensweisen bei der Open-Source-Entwicklung}{32}{subsection.6.1}%
\contentsline {subsubsection}{Beschreibung}{32}{section*.67}%
\contentsline {paragraph}{Dokumente der Community}{32}{section*.68}%
\contentsline {paragraph}{bewährte Verfahren von REUSE}{32}{section*.69}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{32}{section*.70}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{32}{section*.71}%
\contentsline {subsubsection}{Werkzeuge}{33}{section*.72}%
\contentsline {subsubsection}{Empfehlungen}{33}{section*.73}%
\contentsline {subsubsection}{Hilfsmittel}{33}{section*.74}%
\contentsline {subsection}{\numberline {6.2}Zu Open-Source-Projekten beitragen}{33}{subsection.6.2}%
\contentsline {subsubsection}{Beschreibung}{33}{section*.75}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{33}{section*.76}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{34}{section*.77}%
\contentsline {subsubsection}{Werkzeuge}{34}{section*.78}%
\contentsline {subsubsection}{Empfehlungen}{34}{section*.79}%
\contentsline {subsubsection}{Hilfsmittel}{34}{section*.80}%
\contentsline {subsection}{\numberline {6.3}Gehören Sie zur Open-Source-Community}{34}{subsection.6.3}%
\contentsline {subsubsection}{Beschreibung}{34}{section*.81}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{35}{section*.82}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{35}{section*.83}%
\contentsline {subsubsection}{Empfehlungen}{35}{section*.84}%
\contentsline {subsubsection}{Hilfsmittel}{35}{section*.85}%
\contentsline {subsection}{\numberline {6.4}Sichtweise der Personalabteilung}{35}{subsection.6.4}%
\contentsline {subsubsection}{Beschreibung}{36}{section*.86}%
\contentsline {paragraph}{Verfahren}{36}{section*.87}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{36}{section*.88}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{37}{section*.89}%
\contentsline {subsubsection}{Werkzeuge}{37}{section*.90}%
\contentsline {subsubsection}{Empfehlungen}{37}{section*.91}%
\contentsline {subsubsection}{Hilfsmittel}{38}{section*.92}%
\contentsline {subsection}{\numberline {6.5}Upstream first}{38}{subsection.6.5}%
\contentsline {subsubsection}{Beschreibung}{38}{section*.93}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{38}{section*.94}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{38}{section*.95}%
\contentsline {subsubsection}{Empfehlungen}{38}{section*.96}%
\contentsline {subsubsection}{Hilfsmittel}{38}{section*.97}%
\contentsline {section}{\numberline {7}Engagementziel-Aktivitäten}{40}{section.7}%
\contentsline {subsection}{\numberline {7.1}In Open Source Projekten engagieren}{40}{subsection.7.1}%
\contentsline {subsubsection}{Beschreibung}{40}{section*.98}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{40}{section*.99}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{40}{section*.100}%
\contentsline {subsubsection}{Werkzeuge}{41}{section*.101}%
\contentsline {subsubsection}{Empfehlungen}{41}{section*.102}%
\contentsline {subsubsection}{Hilfsmittel}{41}{section*.103}%
\contentsline {subsection}{\numberline {7.2}Open-Source-Communities unterstützen}{41}{subsection.7.2}%
\contentsline {subsubsection}{Beschreibung}{41}{section*.104}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{41}{section*.105}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{41}{section*.106}%
\contentsline {subsubsection}{Empfehlungen}{42}{section*.107}%
\contentsline {subsubsection}{Hilfsmittel}{42}{section*.108}%
\contentsline {subsection}{\numberline {7.3}Öffentliche Darstellung der Verwendung von Open Source}{42}{subsection.7.3}%
\contentsline {subsubsection}{Beschreibung}{42}{section*.109}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{42}{section*.110}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{42}{section*.111}%
\contentsline {subsubsection}{Empfehlungen}{42}{section*.112}%
\contentsline {subsubsection}{Hilfsmittel}{42}{section*.113}%
\contentsline {subsection}{\numberline {7.4}Mit Open-Source-Anbietern zusammenarbeiten}{43}{subsection.7.4}%
\contentsline {subsubsection}{Beschreibung}{43}{section*.114}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{43}{section*.115}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{43}{section*.116}%
\contentsline {subsubsection}{Empfehlungen}{43}{section*.117}%
\contentsline {subsubsection}{Hilfsmittel}{43}{section*.118}%
\contentsline {subsection}{\numberline {7.5}Open-Source-Beschaffungspolitik}{44}{subsection.7.5}%
\contentsline {subsubsection}{Beschreibung}{44}{section*.119}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{44}{section*.120}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{44}{section*.121}%
\contentsline {subsubsection}{Empfehlungen}{44}{section*.122}%
\contentsline {subsubsection}{Hilfsmittel}{45}{section*.123}%
\contentsline {section}{\numberline {8}Strategieziel-Aktivitäten}{46}{section.8}%
\contentsline {subsection}{\numberline {8.1}Aufstellung einer Strategie für die Verwaltung von Open Source im Unternehmen}{46}{subsection.8.1}%
\contentsline {subsubsection}{Beschreibung}{46}{section*.124}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{46}{section*.125}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{46}{section*.126}%
\contentsline {subsubsection}{Empfehlungen}{46}{section*.127}%
\contentsline {subsubsection}{Hilfsmittel}{47}{section*.128}%
\contentsline {subsection}{\numberline {8.2}Wahrnehmung auf Vorstandsebene}{47}{subsection.8.2}%
\contentsline {subsubsection}{Beschreibung}{47}{section*.129}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{47}{section*.130}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{47}{section*.131}%
\contentsline {subsubsection}{Empfehlungen}{48}{section*.132}%
\contentsline {subsection}{\numberline {8.3}Open Source und die digitale Souveränität}{48}{subsection.8.3}%
\contentsline {subsubsection}{Beschreibung}{48}{section*.133}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{49}{section*.134}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{49}{section*.135}%
\contentsline {subsubsection}{Empfehlungen}{49}{section*.136}%
\contentsline {subsubsection}{Hilfsmittel}{49}{section*.137}%
\contentsline {subsection}{\numberline {8.4}Open Source ermöglicht Innovation}{50}{subsection.8.4}%
\contentsline {subsubsection}{Beschreibung}{50}{section*.138}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{50}{section*.139}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{50}{section*.140}%
\contentsline {subsubsection}{Empfehlungen}{51}{section*.141}%
\contentsline {subsubsection}{Hilfsmittel}{51}{section*.142}%
\contentsline {subsection}{\numberline {8.5}Open Source ermöglicht die digitale Transformation}{51}{subsection.8.5}%
\contentsline {subsubsection}{Beschreibung}{51}{section*.143}%
\contentsline {subsubsection}{Gelegenheitsbeurteilung}{52}{section*.144}%
\contentsline {subsubsection}{Fortschrittsbeurteilung}{52}{section*.145}%
\contentsline {subsubsection}{Empfehlungen}{52}{section*.146}%
\contentsline {subsubsection}{Hilfsmittel}{53}{section*.147}%
\contentsline {section}{\numberline {9}Fazit}{54}{section.9}%
\contentsline {subsection}{\numberline {9.1}Kontakt}{54}{subsection.9.1}%
\contentsline {subsection}{\numberline {9.2}Anhang: Vorlage für eine maßgeschneiderte Aktivitäts-Scorecard}{54}{subsection.9.2}%
