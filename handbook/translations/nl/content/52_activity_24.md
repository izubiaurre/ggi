## Beheren van kritieke indicatoren

Activity ID: [GGI-A-24](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_24.md).

### Beschrijving

In het kader van deze activiteit wordt een reeks indicatoren verzameld en gecontroleerd die de basis vormen voor dagelijkse managementbeslissingen en strategische opties met betrekking tot professioneel beheerde open source software.

Kerncijfers met betrekking tot open source software vormen de achtergrond van hoe goed bestuursprogramma's worden uitgerold. De activiteit omvat het selecteren van enkele indicatoren, het publiceren ervan aan de teams en het management, en het versturen van regelmatige updates over het initiatief, bijvoorbeeld via een nieuwsbrief of bedrijfsnieuws.

Deze activiteit vereist:

- belanghebbenden om de doelstellingen van het programma te bespreken en vast te stellen,
- de implementatie van een meet- en gegevensverzamelingstool die is aangesloten op de ontwikkelingsinfrastructuur,
- de publicatie van ten minste een dashboard voor de belanghebbenden en voor alle bij het initiatief betrokken personen.

Indicatoren zijn gebaseerd op gegevens die moeten worden verzameld uit relevante bronnen. Gelukkig zijn er voldoende bronnen voor open source software engineering. Voorbeelden zijn:

- de ontwikkelingsomgeving, de CI/CD-productieketen,
- de HR afdeling,
- de instrumenten voor het testen en de analyse van de softwaresamenstelling,
- de repositories/opslagplaatsen.

Voorbeelden van indicatoren zijn:

- Aantal opgeloste afhankelijkheden, weergegeven per licentietype.
- Aantal verouderde/kwetsbare afhankelijkheden.
- Aantal gedetecteerde licentie/ip-problemen.
- Bijdragen aan externe projecten.
- Tijd dat bug openstaat.
- Aantal medewerkers aan een component, aantal commits, enz.

Bij deze activiteit gaat het erom deze vereisten en meetbehoeften vast te stellen en een dashboard te implementeren dat op een eenvoudige en efficiënte manier de belangrijkste indicatoren van het programma weergeeft.

### Beoordeling van kansen

Sleutelindicatoren helpen de middelen die aan open source software worden besteed te begrijpen en beter te beheren, en de resultaten te meten om effectief te communiceren en de voordelen van de investering ten volle te benutten. Door breed te communiceren kunnen meer mensen het initiatief volgen en zullen zij zich betrokken voelen, waardoor het uiteindelijk een zorg en een doel op organisatieniveau wordt.

Hoewel elke activiteit evaluatiecriteria heeft die helpen vragen te beantwoorden over de geboekte vooruitgang, blijft er behoefte bestaan aan toezicht met behulp van cijfers en kwantitatieve indicatoren.

Of het nu gaat om een kleine startup of een grote wereldwijde onderneming, belangrijke meetgegevens helpen om teams scherp te houden en de prestaties te controleren. Metrics zijn cruciaal omdat ze de besluitvorming ondersteunen en de basis vormen voor het monitoren van reeds genomen beslissingen.

Met eenvoudige en praktische cijfers en grafieken zullen de leden van de hele organisatie de inspanningen met betrekking tot open source kunnen volgen en synchroniseren, waardoor het een gedeelde zorg en actie wordt. Hierdoor kunnen de verschillende actoren ook beter instappen, bijdragen aan het project en de algemene voordelen ervan plukken.

### Voortgangsbeoordeling

Uit de volgende **verificatiepunten** blijkt dat op dit gebied vooruitgang is geboekt:

- [ ] Er is een lijst met statistieken opgesteld en hoe deze te verzamelen.
- [ ] Er worden instrumenten gebruikt om indicatoren te verzamelen, op te slaan, te verwerken en weer te geven.
- [ ] Er is een algemeen dashboard beschikbaar voor alle deelnemers dat de voortgang van het initiatief laat zien.

### Hulpmiddelen

- [GrimoireLab](https://chaoss.github.io/grimoirelab) van Bitergia.
- Generieke BI-tools (elasticsearch, grafana, R/Python visualisaties...) passen ook goed, wanneer de juiste connectoren zijn ingesteld volgens de gedefinieerde doelstellingen.

### Aanbevelingen

- Schrijf de doelstellingen en de roadmap van de open source governance op.
- Communiceer intern over de acties en de status van het initiatief.
- Betrek mensen bij de vaststelling van KPI's, om ervoor te zorgen dat
   - ze goed worden begrepen,
   - ze een volledig beeld van de behoeften bieden en
   - ze worden overwogen en gevolgd.
- Bouw ten minste één dashboard dat voor iedereen kan worden weergegeven (bv. op een scherm in de zaal), met essentiële indicatoren om de voortgang en de algemene situatie te tonen.

### Middelen

- De [CHAOSS-gemeenschap](https://chaoss.community/) heeft veel goede referenties en bronnen met betrekking tot open source indicatoren.
- Bekijk de statistieken voor [Projectattributen](https://www.ow2.org/view/MRL/Stage2-ProjectAttributes) uit de OW2 Market Readiness Levels [methodologie](https://www.ow2.org/view/MRL/Overview).
- [A New Way of Measuring Openness: The Open Governance Index](https://timreview.ca/article/512) van Liz Laffan is interessante lectuur over openheid in open source projecten.
- [Governance Indicators: A Users' Guide](https://anfrel.org/wp-content/uploads/2012/02/2007_UNDP_goveranceindicators.pdf) is de gids van de VN over bestuursindicatoren. Hoewel deze wordt toegepast op democratie, corruptie en transparantie van naties, zijn de grondbeginselen van meting en indicatoren zoals toegepast op bestuur de moeite van het lezen waard.
