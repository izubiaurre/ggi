## Groei van open source competenties

Activiteit ID: [GGI-A-18](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_18.md).

### Beschrijving

Deze activiteit gaat over het plannen en initiëren van technische vaardigheden en vroege ervaring met OSS nadat een inventarisatie is uitgevoerd (#17). Het is ook de gelegenheid om een basisstappenplan voor de ontwikkeling van lichte vaardigheden op te stellen.

- Ga na wat de vereiste vaardigheden en opleiding zijn.
- Zet een proefproject op om de aanpak op gang te brengen, leer al doende, stel een eerste mijlpaal vast.
- Trek lering uit de opgedane ervaring en bouw kennis op.
- Begin met het identificeren en documenteren van de volgende stappen voor een bredere invoering.
- Werk de komende maanden of een jaar een strategie uit om management en financiële steun aan te trekken.

De reikwijdte van de activiteit:

- Linux, Apache, Debian, administratieve vaardigheden.
- Open source databases MariaDB, MySQL, PostgreSQL, enz.
- Open source virtualisatie en cloud technologieën.
- LAMP-stack en zijn alternatieven.

### Beoordeling van kansen

Zoals elke IT-technologie, en waarschijnlijk nog meer, brengt open source innovatie. Open source groeit snel en verandert snel. Het vereist dat organisaties bijblijven.

Deze activiteit helpt gebieden te identificeren waar opleiding mensen kan helpen efficiënter te worden en zich zekerder te voelen bij het gebruik van open source. Het helpt bij het nemen van beslissingen over de ontwikkeling van werknemers. Het zaaien van basis open source vaardigheden geeft de mogelijkheid om te evalueren:

- IT-oplossingen uitbreiden met bestaande markttechnologieën die door het ecosysteem zijn ontwikkeld.
- Nieuwe manieren van samenwerking binnen en buiten de organisatie ontwikkelen.
- Competenties verwerven in nieuwe en innovatieve technologieën.

### Voortgangsbeoordeling

Uit de volgende **verificatiepunten** blijkt dat op dit gebied vooruitgang is geboekt:

- [ ] Er wordt een vaardighedenmatrix ontwikkeld.
- [ ] De omvang van de gebruikte OSS technologieën wordt proactief gedefinieerd, d.w.z. dat ongecontroleerd gebruik van OSS technologieën wordt vermeden.
- [ ] Voor deze technologieën wordt een voldoende niveau van deskundigheid verworven.
- [ ] De teams hebben een "open source basics" opleiding gekregen om aan de slag te gaan.

### Hulpmiddelen

Een belangrijk instrument hierbij is de activiteitenmatrix (of competentiematrix).

Deze activiteit kan worden uitgevoerd door:

- met behulp van online tutorials (vele zijn gratis op het internet ter verkrijgen),
- deelnemen aan conferenties voor ontwikkelaars,
- het verkrijgen van training van de vendor, etc.

### Aanbevelingen

- Het op een veilige en efficiënte manier gebruiken en ontwikkelen van open source componenten vereist een open, op samenwerking gerichte mentaliteit die zowel van bovenaf (management) als van onderaf (ontwikkelaars) moet worden erkend en uitgedragen.
- Zorg ervoor dat de aanpak actief wordt gesteund en bevorderd door het management. Er zal niets gebeuren als er geen engagement is vanuit de hiërarchie.
- Betrek mensen (ontwikkelaars, belanghebbenden) bij het proces: organiseer rondetafelgesprekken en luister naar ideeën.
- Geef mensen tijd en middelen om deze nieuwe concepten te ontdekken, uit te proberen en ermee te spelen. Maak het zo mogelijk leuk - gamification en beloningen zijn goede stimulansen.

Een proefproject met de volgende stappen zou als katalysator kunnen dienen:

- Bepaal de technologie of het kader om mee te beginnen.
- Vind online training, handleidingen en voorbeeldcode om te experimenteren.
- Bouw een prototype van de eindoplossing.
- Stel enkele deskundigen aan om de uitvoering uit te dagen en te coachen.

### Middelen

- [Wat is een Competentiematrix](https://blog.kenjo.io/what-is-a-competency-matrix): een snelle inleiding.
- [How to Make a Skills Matrix for your Team](http://www.managersresourcehandbook.com/download/Skills-Matrix-Template.pdf): een sjabloon met toelichting.
- [MOOC on Free (libre) culture](https://librecours.net/parcours/upload-lc000/) (Alleen in het Frans): dit is een 6-delige cursus over de vrije cultuur, inleiding tot auteursrechten, intellectuele eigendom, open source licenties
