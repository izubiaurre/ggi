## Open source maakt innovatie mogelijk

Activity ID: [GGI-A-36](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_36.md).

### Beschrijving

> Innovatie is de praktische uitvoering van ideeën die leiden tot de invoering van nieuwe goederen of diensten of de verbetering van het aanbod van goederen of diensten.
>
> &mdash; <cite>Schumpeter, Joseph A.</cite>

Open source kan een sleutelfactor zijn voor innovatie door diversiteit, samenwerking en een vlotte uitwisseling van ideeën. Mensen met verschillende achtergronden en domeinen kunnen verschillende perspectieven hebben en nieuwe, verbeterde of zelfs ontwrichtende antwoorden geven op bekende problemen. Men kan innovatie mogelijk maken door te luisteren naar verschillende standpunten en open samenwerking over projecten en onderwerpen actief te bevorderen.

Evenzo is deelname aan de uitwerking en toepassing van open normen een grote stimulans voor goede praktijken en ideeën om het dagelijkse werk van het bedrijf te verbeteren. Het stelt de entiteit ook in staat innovatie aan te sturen en te beïnvloeden waar en wat zij nodig heeft, en het vergroot haar wereldwijde zichtbaarheid en reputatie.

Door innovatie maakt open source het niet alleen mogelijk om de goederen of diensten die uw bedrijf op de markt brengt te transformeren, maar ook om het hele ecosysteem waarin uw bedrijf wil gedijen te creëren of aan te passen.

Door Android bijvoorbeeld als open source vrij te geven, nodigt Google honderdduizenden bedrijven uit om hun eigen diensten op te bouwen op basis van deze open source-technologie. Google creëert zo een heel ecosysteem waarvan alle deelnemers kunnen profiteren. Natuurlijk zijn maar weinig bedrijven machtig genoeg om op eigen houtje een ecosysteem te creëren. Maar er zijn vele voorbeelden van allianties tussen bedrijven om een dergelijk ecosysteem te creëren.

### Beoordeling van kansen

Het is belangrijk om de positie van uw bedrijf ten opzichte van zijn concurrenten en zijn partners en klanten te beoordelen, omdat het voor een bedrijf vaak riskant zou zijn om te ver af te drijven van de standaarden en technologieën die door zijn klanten, partners en concurrenten worden gebruikt. Innovatie betekent uiteraard anders zijn, maar wat verschilt mag niet te groot zijn; anders zou uw bedrijf niet profiteren van de softwareontwikkelingen van de andere bedrijven van het ecosysteem en van de zakelijke dynamiek die het ecosysteem biedt.

### Voortgangsbeoordeling

Uit de volgende **verificatiepunten** blijkt dat op dit gebied vooruitgang is geboekt:

- [ ] De technologieën -- en de open source gemeenschappen die ze ontwikkelen -- die een impact hebben op het bedrijf zijn geïdentificeerd.
- [ ] De voortgang en publicaties van deze open source gemeenschappen worden gevolgd -- ik ben zelfs op de hoogte van hun strategie voordat de releases openbaar worden gemaakt.
- [ ] Medewerkers van de organisatie zijn lid van (sommige van) deze open-sourcegemeenschappen en beïnvloeden hun roadmaps en technische keuzes door bij te dragen aan coderegels en deel te nemen aan de bestuursorganen van deze gemeenschappen.

### Aanbevelingen

Van alle technologieën die nodig zijn om uw bedrijf te runnen, moet u identificeren:

- de technologieën die dezelfde zouden kunnen zijn als die van uw concurrenten,
- de technologieën die specifiek zijn voor uw bedrijf.

Blijf op de hoogte van opkomende technologieën. Open source is het afgelopen decennium de motor geweest achter innovatie, en veel alledaagse krachtige tools komen daar vandaan (denk aan Docker, Kubernetes, Apache Big Data projecten, of Linux). Je hoeft niet alles van alles te weten, maar je moet wel genoeg weten van de stand van de techniek om interessante nieuwe trends te signaleren.

Sta mensen toe en moedig ze aan om innovatieve ideeën in te dienen en naar voren te brengen. Besteed indien mogelijk middelen aan deze initiatieven en laat ze groeien. Vertrouw op de passie en de wil van mensen om nieuwe ideeën en trends te creëren en te stimuleren.

### Middelen

- [4 innovaties die we te danken hebben aan open source](https://www.techrepublic.com/article/4-innovations-we-owe-to-open-source/).
- [The Innovations of Open Source](https://dirkriehle.com/publications/2019-selected/the-innovations-of-open-source/), van Professor Dirk Riehle.
- [Open source technologie, die innovatie mogelijk maakt](https://www.raconteur.net/technology/cloud/open-source-technology/).
- [Can Open Source Innovation Work in the Enterprise?](https://www.threefivetwo.com/blog/can-open-source-innovation-work-in-the-enterprise).
- [Europa: Open source software strategie](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).
- [Europa: Open source software strategie 2020-2023](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
