# Introductie

Dit handboek voor goed bestuur van open source introduceert een methodiek om het professioneel beheer van open source software in een organisatie te implementeren. Het gaat in op de noodzaak om open source software correct en eerlijk te gebruiken, het bedrijf te beschermen tegen technische, juridische en bedreigingen van het intellectueel eigendom, en de voordelen van open source te maximaliseren. Waar een organisatie ook staat ten aanzien van deze onderwerpen, dit document stelt richtlijnen en ideeën voor om verder te komen en de reis tot een succes te maken.

## Context

De meeste grote eindgebruikers en systeemintegrators maken al gebruik van gratis en open source software (FOSS) in hun informatiesystemen of product- en servicedivisies. Open source compliance is een steeds groter wordende zorg geworden en veel grote bedrijven hebben compliance-functionarissen aangesteld. Hoewel het opschonen van de open source productieketen van een bedrijf - waar compliance om draait - van fundamenteel belang is, *moeten* gebruikers iets teruggeven aan gemeenschappen en bijdragen aan de duurzaamheid van het open source ecosysteem. We zien dat open source beheer het hele ecosysteem omvat, betrokken is bij lokale gemeenschappen en een gezonde relatie onderhoudt met leveranciers van open source software en hun specialisten. Dit brengt compliance naar een hoger niveau, en dit is waar open source *goed* bestuur over gaat.

Dit initiatief gaat verder dan compliance en aansprakelijkheid. Het gaat om bewustwording binnen de community van eindgebruikers (vaak softwareontwikkelaars zelf) en systeemintegrators, en het ontwikkelen van gunstige relaties voor alle partijen binnen het Europese FOSS ecosysteem.

OSS Good Governance stelt allerlei soorten organisaties - kleine en grote bedrijven, gemeentes, universiteiten, verenigingen, enz. - in staat de voordelen van open source te maximaliseren door hen te helpen mensen, processen, technologie en strategie op elkaar af te stemmen. En op dit gebied, het maximaliseren van de voordelen van open source leert en innoveert, vooral in Europa, iedereen nog steeds, terwijl niemand weet waar ze eigenlijk aan toe zijn met betrekking tot de stand van de techniek in het domein.

Dit initiatief heeft tot doel organisaties te helpen deze doelen te bereiken met:

* Een gestructureerd overzicht van **activiteiten**, een stappenplan voor de implementatie van professioneel beheer van open source software.
* Een **managementtool** om de voortgang te definiëren, te monitoren, te rapporteren en te communiceren.
* Een **duidelijk en praktisch verbeterpad** met kleine, betaalbare stappen om risico's te verkleinen, mensen op te leiden, processen aan te passen en om naar binnen en naar buiten te communiceren in de organisatie.
* **Begeleiding** en een reeks van **samengestelde referenties** over open source licenties, best practices, training en de betrokkenheid van ecosystemen om open source bewustzijn en -cultuur te benutten, interne kennis te versterken en leiderschap te vergroten.

Deze gids is ontwikkeld met de volgende eisen in het achterhoofd:

* Elk type organisatie komt aan bod: van MKB tot grote bedrijven en non-profit organisaties, van lokale overheden (bijv. gemeentes) tot grote instellingen (bijv. Europese of overheidsinstellingen). Het raamwerk geeft bouwstenen voor een strategie en handvatten voor de realisatie ervan, maar *hoe* de activiteiten worden uitgevoerd hangt volledig af van de context van het programma en dit is aan de programmamanager. Het kan zeer nuttig zijn om consultancy diensten te zoeken en met collega organisaties kennis uit te wisselen.
* Er wordt geen aanname gedaan over het niveau van technische kennis binnen de organisatie of het domein waarin de activiteiten plaatsvinden. Sommige organisaties zullen bijvoorbeeld een volledig trainingscurriculum moeten opzetten, terwijl andere organisaties misschien gewoon ad-hocmateriaal aan de teams zullen voorstellen.

Sommige activiteiten zullen niet voor alle situaties relevant zijn, maar het hele raamwerk biedt nog steeds een volledige routekaart en maakt de weg vrij voor op maat gemaakte strategieën.

## Over de Good Governance Initiative

Bij OW2 zijn initiatieven een gezamenlijke inspanning om in een marktbehoefte te voorzien. De [Good Governance Initiative](https://www.ow2.org/view/OSS_Governance) stelt een methodologisch raamwerk voor om professioneel beheer van open source software binnen organisaties te implementeren.

De Good Governance initiative is gebaseerd op een uitgebreid model dat is geïnspireerd op de populaire hiërarchie van menselijke behoeften en motivaties van Abraham Maslow, zoals geïllustreerd door de onderstaande afbeelding.

![Maslow en de GGI](resources/images/ggi_maslow.png)

Door middel van ideeën, richtlijnen en activiteiten biedt de Good Governance initiative een blauwdruk voor de implementatie van een organisatorische eenheid die belast is met het professioneel beheer van open source software, ook wel OSPO (Open Source Program Office) genoemd. De methodiek is ook een managementsysteem om prioriteiten te definiëren en de voortgang hiervan te monitoren en te delen.

Bij het implementeren van de OSS Good Governance-methodiek zullen organisaties hun vaardigheden in een aantal richtingen verbeteren, waaronder:

* op de **juiste en veilige** manier gebruik maken van open source software binnen de organisatie om het hergebruik en de onderhoudbaarheid van software, en de snelheid van softwareontwikkeling te verbeteren;
* **mitigeren** van de juridische en technische risico's welke zijn verbonden aan het gebruik van externe code en samenwerking;
* **identificeren** van benodigde traningen voor teams, van developers tot teamleiders en managers, zodat iedereen dezelfde visie deelt;
* **prioriteren** van doelen en activiteiten om een efficiënte open source strategie te ontwikkelen;
* **efficient communiceren** binnen de organisatie en naar de buitenwereld, om het meeste uit de open source strategie te halen;
* het **verbeteren** van het vermogen van de organisatie om te kunnen concurreren en de aantrekkelijkheid van de organisatie voor toptalenten op het gebied van open source.

## Over de OSPO Alliance

De **OSPO Alliance** is gelanceerd door een coalitie van toonaangevende Europese non-profitorganisaties op het gebied van open source, waaronder OW2, Eclipse Foundation, OpenForum Europe en Foundation for Public Code, met als missie om het bewustzijn voor open source in Europa en wereldwijd te vergroten en het gestructureerd en professioneel beheer van open source door bedrijven en administraties te bevorderen.

Terwijl de Good Governance Initiative gericht is op het ontwikkelen van een managementmethodiek, heeft de OSPO Alliance het bredere doel om bedrijven, met name in niet technologiesectoren, en openbare instellingen te helpen open source te ontdekken en te begrijpen, er in hun activiteiten van te profiteren en te groeien naar hun eigen OSPO's hosten.

De OSPO Alliance heeft de **OSPO Alliance**-website opgericht die wordt gehost op <https://ospo-alliance.org>. De OSPO Alliance biedt de gemeenschap een veilige plek om de onderwerpen van OSPO's te bespreken en uit te wisselen, en biedt een repository voor een uitgebreide reeks bronnen voor bedrijven, openbare instellingen en onderzoeks- en academische organisaties. De OSPO Alliance verbindt met OSPO's in heel Europa en de wereld, evenals met ondersteunende maatschappelijke organisaties. Het moedigt best practices aan en bevordert de bijdrage aan de duurzaamheid van het open source ecosysteem. Bekijk de [OSPO Alliance](https://ospo-alliance.org) website voor een snel overzicht van aanvullende raamwerken van best practices op het gebied van IT-beheer.

De website [OSPO Alliance](https://ospo-alliance.org) is ook de plek waar we feedback verzamelen over het initiatief en de inhoud ervan (bijv. activiteiten, kennis) van de gemeenschap als geheel.

## Vertalingen

Er is een doorlopend project om het GGI-handboek in verschillende talen te vertalen. Aangezien de voortgang snel evolueert, raden we aan om onze officiële website te bezoeken voor een volledige lijst met beschikbare vertalingen.

> Zie <https://hosted.weblate.org/projects/ospo-zone-ggi/#languages>

Het GGI-handboek is vertaald met behulp van [Weblate](https://hosted.weblate.org/), een open source project en platform dat gratis hosting biedt voor open source projecten. We willen hen van harte bedanken, evenals al onze vertaalmedewerkers. Je bent geweldig.

## Bijdragers

De volgende geweldige mensen hebben bijgedragen aan de Good Governance Initiative:

* Frédéric Aatz (Microsoft France)
* Boris Baldassari (Castalia Solutions, Eclipse Foundation)
* Philippe Bareille (Ville de Paris)
* Gaël Blondelle (Eclipse Foundation)
* Vicky Brasseur (Wipro)
* Philippe Carré (Nokia)
* Pierre-Yves Gibello (OW2)
* Michael Jaeger (Siemens)
* Sébastien Lejeune (Thales)
* Max Mehl (Free Software Foundation Europe)
* Hervé Pacault (Orange)
* Stefano Pampaloni (RIOS)
* Christian Paterson (OpenUp)
* Simon Phipps (Meshed Insights)
* Silvério Santos (Orange Business Services)
* Cédric Thomas (OW2)
* Nicolas Toussaint (Orange Business Services)

## Licentie

Dit werk is gelicentieerd onder een [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) licentie (CC-BY 4.0). Van de Creative Commons website:

> Je bent vrij om:
>
> * het werk te delen — te kopiëren, te verspreiden en door te geven via elk medium of bestandsformaat
> * het werk te bewerken - te remixen, te veranderen en afgeleide werken te maken
>
> voor alle doeleinden, inclusief commerciële doeleinden.
>
> Zolang u de juiste vermelding geeft, een link naar de licentie geeft, en aangeeft of er wijzigingen zijn aangebracht. U mag dit op elke redelijke manier doen, maar niet op een manier die suggereert dat de licentiegever u of uw gebruik goedkeurt.

Alle content is Copyright: 2020-2022 OW2 & The Good Governance Initiative participants.
