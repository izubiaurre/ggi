## Bijdragen aan open source projecten

Activity ID: [GGI-A-26](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_26.md).

### Beschrijving

Bijdragen aan open source projecten die vrij gebruikt worden is een van de belangrijkste principes van goed bestuur. Het punt is om te vermijden een simpele passieve consument te zijn en terug te geven aan de projecten. Wanneer mensen een functie toevoegen of een bug repareren voor hun eigen doel, moeten ze het generiek genoeg maken om bij te dragen aan het project. Ontwikkelaars moeten de tijd krijgen om bij te dragen.

Deze activiteit heeft betrekking op het volgende toepassingsgebied:

- Werken met upstream open source projecten.
- Bugs en feature requests rapporteren.
- Code en bugfixes bijdragen.
- Deelnemen aan mailinglijsten van de open source gemeenschap.
- Ervaring delen.

### Beoordeling van kansen

De belangrijkste voordelen van deze activiteit zijn:

- Het vergroot de algemene kennis van en betrokkenheid bij open source binnen het bedrijf, omdat mensen gaan bijdragen en betrokken raken bij open source projecten. Ze krijgen een gevoel van openbaar nut en verbeteren hun persoonlijke reputatie.
- Het bedrijf vergroot zijn zichtbaarheid en reputatie naarmate de bijdragen zich een weg banen door het bijgedragen project. Dit laat zien dat het bedrijf daadwerkelijk betrokken is bij open source, een bijdrage levert en eerlijkheid en transparantie bevordert.

### Voortgangsbeoordeling

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] Er is een duidelijk en officieel pad voor mensen die een bijdrage willen leveren.
- [ ] Ontwikkelaars worden aangemoedigd om bij te dragen aan open source projecten die ze gebruiken.
- [ ] Er bestaat een proces om de compliance en de zekerheid van de bijdragen van de ontwikkelaars te waarborgen.
- KPI: Volume van externe bijdragen (code, mailinglijsten, issues...) per individu, team of entiteit.

### Hulpmiddelen

Het kan nuttig zijn de bijdragen te volgen, zowel om bij te houden wat er wordt bijgedragen als om te kunnen communiceren over de inspanningen van het bedrijf. Hiervoor kunnen dashboards en software voor het bijhouden van activiteiten worden gebruikt. Controleer:

- Bitergia's [GrimoireLab](https://chaoss.github.io/grimoirelab/)
- [ScanCode](https://scancode-toolkit.readthedocs.io)

### Aanbevelingen

Moedig mensen binnen de entiteit aan om bij te dragen aan externe projecten door:

- Geef ze tijd om generieke, goed geteste bugfixes en functies te schrijven en deze terug te dragen aan de gemeenschap.
- Training geven aan mensen over het bijdragen aan open source gemeenschappen. Dit gaat zowel over technische vaardigheden (verbetering van de kennis van uw team) als over de gemeenschap (behoren tot de open-sourcegemeenschappen, gedragscode, enz.).
- Zorg voor opleiding over juridische, intellectueel eigendom en technische kwesties, en stel een contactpersoon binnen het bedrijf in om te helpen met deze onderwerpen als mensen twijfels hebben.
- Prikkels geven voor gepubliceerd werk.
- Merk op dat de bijdragen van het bedrijf/de entiteit de kwaliteit en de betrokkenheid van de code weerspiegelen, dus zorg ervoor dat uw ontwikkelingsteam code levert die goed genoeg is.

### Middelen

- Het [CHAOSS](https://chaoss.community/) initiatief van de Linux Foundation heeft enkele hulpmiddelen en aanwijzingen over hoe je bijdragen in ontwikkeling kunt volgen.
