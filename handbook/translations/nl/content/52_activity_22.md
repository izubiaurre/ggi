## Beheer van kwetsbaarheden van software

Activity ID: [GGI-A-22](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_22.md).

### Beschrijving

Iemands code is zo veilig als het minst veilige deel. Recente gevallen (bijv. Heartbleed[^heartbleed], equifax[^equifax]) hebben het belang aangetoond van het controleren van kwetsbaarheden in delen van de code die niet rechtstreeks door de entiteit zijn ontwikkeld. De gevolgen van blootstellingen variëren van datalekken (met enorme impact op de reputatie) tot ransomware aanvallen en bedrijfsbedreigende onbeschikbaarheid van diensten.

Het is bekend dat open source software een beter beheer van kwetsbaarheden biedt dan propriëtaire software, vooral omdat:

- Meer ogen zijn op zoek naar het vinden en oplossen van problemen met open code en processen.
- Open source projecten lossen kwetsbaarheden op en brengen veel sneller patches en nieuwe versies uit.

Een [onderzoek door WhiteSource](https://resources.whitesourcesoftware.com/blog-whitesource/3-reasons-why-open-source-is-safer-than-commercial-software) over eigen software toonde bijvoorbeeld aan dat bij 95 % van de gevonden kwetsbaarheden in hun open source componenten, er al een oplossing vrijgegeven was op het moment van de analyse. Het probleem is daarom om **kwetsbaarheden beter te beheren, zowel in de codebase als in de afhankelijkheden**, ongeacht of ze gesloten of open source zijn.

Om deze risico's te beperken, moet men een beoordelingsprogramma van zijn software opzetten en een proces voor het controleren van kwetsbaarheden welke regelmatig wordt uitgevoerd. Implementeer tools die getroffen teams waarschuwen, bekende kwetsbaarheden beheren en bedreigingen door softwareafhankelijkheden voorkomen.

### Beoordeling van kansen

Elk bedrijf dat software gebruikt, moet zijn kwetsbaarheden bekijken in:

- zijn infrastructuur (bijv. cloudinfrastructuur, netwerkinfrastructuur, datastores),
- zijn bedrijfstoepassingen (HR, CRM-tools, intern en klantgerelateerd gegevensbeheer),
- zijn intern ontwikkelde code: b.v. de website van het bedrijf, interne ontwikkelingsprojecten, enz.,
- en alle directe en indirecte afhankelijkheden van software en services.

De ROI van kwetsbaarheden is amper bekend totdat er iets ergs gebeurt. Men moet rekening houden met de gevolgen van een grote datalek of onbeschikbaarheid van diensten om de werkelijke kosten van kwetsbaarheden in te schatten.

Ook moet een cultuur van geheimhouding en het verbergen van veiligheidsgerelateerde kwesties binnen het bedrijf, koste wat het kost worden vermeden. In plaats daarvan moet informatie over de staat van kwetsbaarheid worden gedeeld en besproken om de beste antwoorden te vinden van de juiste mensen, van ontwikkelaars tot leidinggevenden op c-niveau.

De voordelen van het voorkomen van cyberaanvallen door softwarekwetsbaarheden zorgvuldig te beheren, zijn talrijk:

- Vermijd reputatierisico's,
- Vermijd verlies in exploitatie (DDoS, Ransomware, de benodigde tijd om een alternatief IT-systeem opnieuw op te bouwen na een aanval),
- Houd u aan de voorschriften voor gegevensbescherming.

Het beheer van OSS software-kwetsbaarheden is slechts een onderdeel van het grotere cyberbeveiligingsproces dat wereldwijd de beveiliging van de systemen en services in de organisatie aanpakt.

### Voortgangsbeoordeling

Er moet een vastgesteld persoon of team zijn om kwetsbaarheden en gebruiksvriendelijke processen te monitoren waar ontwikkelaars op kunnen vertrouwen. Beoordeling van kwetsbaarheden is een standaardonderdeel van het continue integratieproces en mensen kunnen de huidige risicostatus volgen in een speciaal dashboard.

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] Activiteit is gedekt wanneer alle interne software en services worden beoordeeld en gecontroleerd op bekende kwetsbaarheden.
- [ ] Activiteit is gedekt wanneer een speciale tool en proces is geïmplementeerd in de softwareproductieketen om de introductie van problemen in de dagelijkse ontwikkelingsroutines te voorkomen.
- [ ] Een persoon of team is verantwoordelijk voor het beoordelen van CVE/kwetsbaarheidsrisico's tegen blootstelling.
- [ ] Een persoon of team is verantwoordelijk voor het verzenden van CVE/kwetsbaarheden naar betrokken personen (SysOps, DevOps, ontwikkelaars, enz.).

### Hulpmiddelen

- GitHub gereedschap
   - GitHub biedt richtlijnen en hulpmiddelen voor het beveiligen van code die op het platform wordt gehost. Zie [GitHub docs](https://docs.github.com/en/github/administering-a-repository/about-securing-your-repository) voor meer informatie.
   - GitHub biedt [Dependabot](https://docs.github.com/en/github/managing-security-vulnerabilities/about-alerts-for-vulnerable-dependencies) om kwetsbaarheden in afhankelijkheden automatisch te identificeren.
- [Eclipse Steady](https://eclipse.github.io/steady/) is een gratis, open source tool die Java- en Python-projecten analyseert op kwetsbaarheden en ontwikkelaars helpt deze te verminderen.
- [OWASP dependency-check](https://owasp.org/www-project-dependency-check/): een open source kwetsbaarheidsscanner.
- [OSS Review Toolkit](https://github.com/oss-review-toolkit/ort): een open source orchestrator die beveiligingsadviezen kan verzamelen voor gebruikte afhankelijkheden van geconfigureerde gegevensservices voor kwetsbaarheden.

### Middelen

- De [MITRE's kwetsbaarheidsdatabase](https://cve.mitre.org/) van CVE's. Zie ook [NIST's beveiligingsdatabase](https://nvd.nist.gov/) van NVD's en satellietbronnen zoals [CVE Details](https://www.cvedetails.com/).
- Check ook dit nieuwe initiatief van Google: the [open source Vulnerabilities](https://osv.dev/).
- De OWASP-werkgroep publiceert een lijst met kwetsbaarheidsscanners [op hun website](https://owasp.org/www-community/Vulnerability_Scanning_Tools), zowel uit de commerciële wereld als uit de wereld van open source.
- J. Williams and A. Dabirsiaghi. The unfortunate reality of insecure libraries, 2012.
- [Detection, assessment and mitigation of vulnerabilities in open source dependencies](https://link.springer.com/article/10.1007/s10664-020-09830-x), Serena Elisa Ponta, Henrik Plate & Antonino Sabetta, Empirical Software Engineering volume 25, pagina's 3175–3215(2020).
- [A Manually-Curated Dataset of Fixes to Vulnerabilities of open source Software](https://arxiv.org/abs/1902.02595), Serena E. Ponta, Henrik Plate, Antonino Sabetta, Michele Bezzi, Cédric Dangremont. Er is ook een [toolkit in development to implement the aforementioned dataset](https://sap.github.io/project-kb/).

[^heartbleed]: https://nl.wikipedia.org/wiki/Heartbleed
[^equifax]: https://arstechnica.com/information-technology/2017/09/massive-equifax-breach-caused-by-failure-to-patch-two-month-old-bug/
