## Upstream-first

Activity ID: [GGI-A-39](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_39.md).

### Beschrijving

Deze activiteit houdt zich bezig met het ontwikkelen van bewustzijn met betrekking tot de voordelen van bijdragen en het afdwingen van het upstream-first principe.

Met de upstream-first benadering moet alle ontwikkeling op een open source project worden gedaan met het niveau van kwaliteit en openheid dat vereist is om te worden voorgelegd aan de kernontwikkelaars van een project, en door hen te worden gepubliceerd.

### Beoordeling van kansen

Het schrijven van code met upstream-first in het achterhoofd resulteert in:

- betere kwaliteit code,
- code die klaar is om "upstream" te worden ingediend,
- code die is samengevoegd in de kernsoftware,
- code die compatibel zal zijn met toekomstige versies,
- erkenning door de projectgemeenschap en een betere en meer winstgevende samenwerking.

> Upstream-first is meer dan alleen "aardig zijn". Het betekent dat je inspraak hebt in het project. Het betekent voorspelbaarheid. Het betekent dat je de controle hebt. Het betekent dat je handelt in plaats van reageert. Het betekent dat je open source begrijpt. ([Maximilian Michels](https://maximilianmichels.com/2021/upstream-first/))

### Voortgangsbeoordeling

De volgende **verificatiepunten** laten de voortgang van deze activiteit zien: Upstream-first geïmplementeerd?

- [ ] Aanzienlijke toename van het aantal pull/merge verzoeken dat wordt ingediend bij projecten van derden.
- [ ] Er is een lijst opgesteld van projecten van derden waarvoor upstream-first moet worden toegepast.

### Aanbevelingen

- Identificeer ontwikkelaars met de meeste ervaring in interactie met "upstream" ontwikkelaars.
- Faciliteer de interactie tussen ontwikkelaars en kernontwikkelaars (evenementen, hackathons, enz.)

### Middelen

- Een duidelijke uitleg van het upstream-first principe en waarom het past in het Cultuurdoel: <https://maximilianmichels.com/2021/upstream-first/>.

> Upstream-first betekent dat wanneer u een probleem oplost in uw kopie van de upstream-code waarvan anderen kunnen profiteren, u deze wijzigingen weer upstream bijdraagt, d.w.z. u stuurt een patch of opent een pull-verzoek naar de upstream-repository.

- [Wat is upstream en downstream in softwareontwikkeling?](https://reflectoring.io/upstream-downstream/) Een kristalheldere uitleg.
- Uitgelegd vanuit de ontwerpdocumenten van Chromium OS: [Upstream First](https://www.chromium.org/chromium-os/chromiumos-design-docs/upstream-first).
- Red Hat upstream en de voordelen van [upstream first](https://www.redhat.com/en/blog/what-open-source-upstream).
