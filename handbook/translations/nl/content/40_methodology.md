# Methodologie

Het implementeren van de OSS Good Governance-methodiek is uiteindelijk een consequent en impactvol initiatief. Er zijn verschillende categorieën mensen, diensten en processen bij betrokken, van de dagelijkse praktijk tot HR-management en van ontwikkelaars tot C-level executives. Er bestaat echt geen wondermiddel om het goed bestuur van open source te implementeren. Verschillende soorten organisaties en bedrijfsculturen en -situaties vragen om verschillende benaderingen van open source governance. Voor elke organisatie zullen er verschillende beperkingen en verwachtingen zijn, wat leidt tot verschillende paden en manieren om het programma te beheren.

Met dit in gedachten biedt de Good Governance Initiative een generieke blauwdruk van activiteiten die kunnen worden afgestemd op het eigen domein, de eigen cultuur en de eigen vereisten van een organisatie. Hoewel de blauwdruk pretendeert allesomvattend te zijn, kan de methodologie stapsgewijs worden toegepast. Men kan het programma opstarten door eenvoudigweg de meest relevante doelstellingen en activiteiten in de eigen specifieke context te selecteren. Het idee is om een eerste ontwerp-roadmap op te stellen om het lokale initiatief te helpen opzetten.

Naast dit kader raden wij ook ten zeerste aan in contact te komen met gelijken via een gevestigd netwerk zoals het Europese [OSPO Alliance](https://ospo-alliance.org) initiatief, of andere gelijkgestemde initiatieven van de TODO-groep of OSPO++. Wat belangrijk is de mogelijkheid om kennis uit te wisselen met mensen die een soortgelijk initiatief uitvoeren, en de problemen en oplossingen die zich voordoen te delen.

## Voorbereidingen

Gezien de ambitie van de methode voor goed bestuur en de potentieel brede impact ervan, is het belangrijk om met verschillende mensen binnen een organisatie te communiceren. Het zou goed zijn hen in te werken om een eerste reeks realistische verwachtingen en vereisten vast te stellen om een goede start te maken en belangstelling en steun aan te trekken. Een goed idee is om de aangepaste activiteitenscorekaarten te publiceren op het samenwerkingsplatform van de organisatie, zodat ze kunnen worden gebruikt om met belanghebbenden te communiceren. Enkele tips:

* Identificeer de belangrijkste belanghebbenden, zorg dat zij het eens worden over een reeks primaire doelstellingen. Betrek hen bij het succes van het initiatief als onderdeel van hun eigen agenda.
* Zorg voor initiële instemming, maak afspraken over de stappen en het tempo, en stel regelmatige controles in om hen te informeren over de voortgang.
* Zorg ervoor dat zij de voordelen begrijpen van wat kan worden bereikt en wat het inhoudt: de verwachte verbetering moet duidelijk zijn en de resultaten moeten zichtbaar zijn.
* Opstellen van een eerste diagnose of stand van zaken van open source in de kandidaat-organisatie. Resultaat: een document dat beschrijft wat dit programma zal bereiken, waar de organisatie staat en waar het naartoe wil.

## Werkstroom

Als moderne software beoefenaars houden wij van agile-achtige methoden die kleine en veilige incrementen definiëren.

In de context van een levend OSPO-programma is dit zeer relevant, aangezien veel nevenaspecten in de loop van de tijd zullen veranderen, van de strategie van de organisatie en de reactie op open source tot de beschikbaarheid en de betrokkenheid van de mensen. Periodieke herbeoordeling en iteratie maken het ook mogelijk zich aan te passen aan de lopende programma-acceptatie, de huidige trends en kansen beter te volgen, en kleine incrementele voordelen voor de belanghebbenden en de organisatie als geheel.

Idealiter zou de methodologie als volgt in vijf fasen kunnen worden uitgevoerd:

1. **Ontdekking** Inzicht in sleutelbegrippen, eigenaarschap van de methodologie, afstemming van doelstellingen en verwachtingen.
1. **Maatwerk** Aanpassen van de beschrijving van de activiteit en de beoordeling van de kansen aan de specifieke kenmerken van de organisatie.
1. **Prioritering** Het identificeren van doelstellingen en belangrijke resultaten, taken en instrumenten, het plannen van mijlpalen en het opstellen van een tijdschema.
1. **Activering** Activiteitenrapport afronden, budget, opdrachten, taken documenteren in issue manager.
1. **Iteratie** Resultaten beoordelen en scoren, problemen signaleren, verbeteren, aanpassen. Iteratief per kwartaal of semester.

Voorbereiding voor de eerste programma iteratie:

* Een eerste reeks taken vaststellen waaraan moet worden gewerkt, en deze prioriteren volgens de behoeften (hiaten naar de gewenste toestand) en het tijdschema. Resultaat: een lijst van taken waaraan tijdens de iteratie moet worden gewerkt.
* Definieer een reeks eisen en verbeteringsgebieden, deel deze mee aan de belanghebbenden en eindgebruikers, en verkrijg hun goedkeuring of betrokkenheid.
* Vul de scorekaarten in voor voortgangsbewaking. Een model scorekaart kan worden gedownload van de [GGI repository](https://gitlab.ow2.org/ggi/ggi/-/tree/main/resources/).

Doe aan het eind van elke iteratie een terugblik en bereid u voor op de volgende iteratie:

* Communiceer over de laatste verbeteringen.
* Beoordeel waar u staat, of de beoogde taken zijn voltooid en verfijn het stappenplan dienovereenkomstig.
* Controleer de resterende pijnpunten of problemen, vraag zo nodig ondersteuning van andere actoren of diensten.
* Taken herprioriteren volgens de bijgewerkte context.
* Definieer een nieuwe subset van uit te voeren taken.

## Handmatige opzet: gebruik van aangepaste activiteitenscorekaarten

Een aangepast activiteitenrapport is een formulier dat een basisactiviteit beschrijft, aangepast aan de specifieke kenmerken van een organisatie. Samen vormen deze scorekaarten de routekaart voor het beheer van open source software.

`Uit de eerste ervaringen met de methodologie blijkt dat het tot een uur duurt om een basisactiviteit aan te passen aan de specifieke aangepaste scorekaart van een organisatie.`

De aangepaste activiteiten scorekaart bevat de volgende onderdelen:

* **De titel bepalen** Neem eerst een paar minuten de tijd om een begrip te ontwikkelen van waar de activiteit over zou kunnen gaan en de relevantie ervan, hoe het kan passen in uw algehele OSS-management reis.
* **Aangepaste beschrijving** Pas de activiteit aan de specifieke kenmerken van de organisatie en scope aan. Bepaal de reikwijdte van de activiteit, de specifieke use case die u gaat behandelen.
* **Beoordeling van kansen** Leg uit waarom het relevant is om deze activiteit te ondernemen, in welke behoeften deze voorziet. Wat zijn onze pijnpunten? Wat zijn de kansen om vooruitgang te boeken? Wat kan er gewonnen worden?
* **Doelstellingen** Bepaal een paar cruciale doelstellingen voor de activiteit. Op te lossen pijnpunten, voortgangsmogelijkheden, wensen, identificeer kerntaken, en wat we in deze iteratie willen bereiken.
* **Instrumenten** Technologieën, instrumenten en producten die in de activiteit worden gebruikt.
* **Operationele nota's** Aanwijzingen over aanpak, methode, strategie om vooruitgang te boeken bij deze activiteit.
* **Belangrijkste resultaten** Definieer meetbare, controleerbare verwachte resultaten. Kies resultaten die de vooruitgang ten opzichte van de doelstellingen aangeven, geef hier KPI's aan.
* **Voortgang en score** Voorgang is in %, het voltooiingspercentage van het resultaat; score is de persoonlijke succesbeoordeling.
* **Persoonlijke beoordeling** Voor elk resultaat kunt u een korte toelichting toevoegen en uw persoonlijke tevredenheid uitgedrukt in de score toelichten.
* **Tijdslijn** Vermeld start- en einddata, fasering van taken, kritische stappen, en mijlpalen.
* **Inspanningen** Evalueer de gevraagde tijd en materiële middelen, intern en door derden. Welke inspanningen worden verwacht? Hoeveel gaat het kosten? Welke middelen hebben we nodig?
* **Toegewezen** Zeg wie deelneemt. Wijs taken of activiteitenleiding en verantwoordelijkheden toe.
* **Kwesties** Identificeer belangrijke kwesties, voorziene moeilijkheden, risico's, wegversperringen, onzekerheden, aandachtspunten, kritische afhankelijkheden.
* **Status** Schrijf hier een beoordeling van hoe het met de activiteit gaat: Gezond? Vertraagd? Enz.
* **Algemene voortgangsbeoordeling** Uw eigen, managementgerichte, voortgangsevaluatie op hoog niveau.

## Automatische installatie: gebruik van de GGI Deployment functie

Vanaf versie 1.1 van het handboek, stelt de GGI [My GGI Board](https://gitlab.ow2.org/ggi/my-ggi-board) een geautomatiseerde tooling voor, om je eigen instantie van de GGI uit te rollen als een GitLab project. Het installatieproces duurt minder dan 10 minuten, is volledig gedocumenteerd, en biedt een eenvoudige en betrouwbare manier om de activiteiten aan te passen, de uitvoering te volgen terwijl u vooruitgang boekt, en de resultaten te communiceren aan uw stakeholders. Een live voorbeeld van de implementatie is te zien in het [initiatief GitLab](https://gitlab.ow2.org/ggi/my-ggi-board-test), met de automatisch gegenereerde website beschikbaar op [zijn GitLab pagina's](https://ggi.ow2.io/my-ggi-board-test/).

![GGI deploy activiteiten](resources/images/ggi_deploy_activities.png)

Hier is een standaard workflow om de uitrolfunctie te gebruiken:

1. Fork het My GGI Board naar je eigen GitLab instance of project, en stel het in volgens de instructies in de README van het project: <https://gitlab.ow2.org/ggi/my-ggi-board>. Dit zal:

- Creëert alle activiteiten als issues in het project.
- Maakt een mooi bord om de activiteiten te visualiseren en te beheren.
- Maakt een statische website, gepubliceerd op je GitLab instance pagina's, met de informatie uit de activiteiten.
- Werkt de beschrijving van het project bij met de juiste links naar de activiteitenraad en uw statische website.

1. Van daaruit kunt u de verschillende activiteiten bekijken en de scorekaart invullen.

- Het scorecard-gedeelte is het elektronische (en vereenvoudigde) equivalent van de bovengenoemde ODT-scorecards. Ze worden gebruikt om de activiteit aan te passen aan uw context, door de lokale middelen, risico's en mogelijkheden op te sommen, en aangepaste doelstellingen te definiëren die nodig zijn om de activiteit te voltooien.
- Als een activiteit niet van toepassing is op uw context, markeer deze dan gewoon als 'Niet geselecteerd', of sluit hem af.
- Dit is een vrij tijdrovend proces, maar zeer noodzakelijk omdat het u zal helpen, stap voor stap, uw eigen routekaart en plan te bepalen.

1. Wanneer de activiteiten zijn gedefinieerd, kunt u beginnen met de uitvoering van uw eigen OSPO. Selecteer een paar activiteiten die je relevant vindt om mee te beginnen, en verander hun voortgangslabel van 'Niet gestart' in 'In uitvoering'. Je kunt GitLab functies gebruiken om je te helpen het werk te organiseren (commentaar, opdrachtnemers, enz.) of een andere tool. Het is gemakkelijk om aan de activiteiten te linken, en er zijn veel goede integraties beschikbaar.
1. Beoordeel en herzie regelmatig (wekelijks, maandelijks, afhankelijk van uw tijdschema) de huidige activiteiten en verander het label van "In uitvoering" in "Gereed" wanneer ze voltooid zijn. Selecteer een paar andere en begin opnieuw bij stap 3 totdat ze allemaal voltooid zijn.

De website biedt een snel overzicht van de huidige en vroegere activiteiten, en haalt het scorecard-gedeelte van de kwesties eruit om alleen de lokaal relevante informatie weer te geven. Wanneer er veranderingen plaatsvinden in de issues (activiteiten) worden deze automatisch bijgewerkt in de gegenereerde website. Merk op dat de CI pipelines voor het automatisch genereren van de website automatisch 's nachts worden uitgevoerd, maar je kunt ze eenvoudig starten vanuit de CI/CD sectie van het GitLab project. De volgende afbeelding toont de automatisch gegenereerde website interface.

![GGI deploy website](resources/images/ggi_deploy_website.png)

Je kunt vragen stellen of ondersteuning krijgen voor de deployment functie op onze GitLab homepage, en we verwelkomen feedback.

> GGI Deploy homepage: <https://gitlab.ow2.org/ggi/my-ggi-board>

## Geniet

Communiceer over uw succes en geniet van de gemoedsrust van een state of the art open source strategie!

De OSS Good Governance-methodiek is een methode om een continu verbeteringsprogramma te implementeren, en als zodanig eindigt het nooit. Toch is het belangrijk om tussenstappen te benadrukken en de veranderingen die het oplevert te waarderen, om de vooruitgang zichtbaar te maken en de resultaten te delen.

* Communiceer met belanghebbenden en eindgebruikers om hen te laten weten wat de voordelen van het initiatief zijn.
* De duurzaamheid van het programma bevorderen. Ervoor zorgen dat de best practices en de uit het programma getrokken lessen steeds worden toegepast en bijgewerkt.
* Deel uw ervaring met uw collega's: geef feedback aan de GGI-werkgroep en binnen uw OSPO-gemeenschap van goedkeuring, en deel uw aanpak.
