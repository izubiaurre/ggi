## Open source maakt digitale transformatie mogelijk

Activity ID: [GGI-A-37](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_37.md).

### Beschrijving

> "Digitale transformatie is de toepassing van digitale technologie om diensten of bedrijven te transformeren, door niet-digitale of handmatige processen te vervangen door digitale processen of door oudere digitale technologie te vervangen door nieuwere digitale technologie." (Wikipedia)

Wanneer de meest geavanceerde organisaties in Digitale Transformatie gezamenlijk veranderingen aansturen via hun Business, IT en Finance om digitaal in de weg te verankeren, heroverwegen ze:

- Businessmodel: waardeketen met ecosystemen, as a service, SaaS.
- Financiën: opex/capex, mensen, uitbesteding.
- IT: innovatie, modernisering van legacy/assets.

Open source is de kern van digitale transformatie:

- Technologieën, Agile praktijken, product management.
- Mensen: samenwerking, open communicatie, ontwikkelings-/beslissingscyclus.
- Bedrijfsmodellen: try & buy, open innovatie.

In termen van concurrentievermogen zijn de meest zichtbare processen waarschijnlijk de processen die rechtstreeks van invloed zijn op de klantervaring. En we moeten erkennen dat de grote spelers, maar ook startende bedrijven, door een totaal ongekende klantervaring te bieden, de verwachtingen van de klant drastisch hebben veranderd.

Klantervaring en alle andere processen binnen een bedrijf zijn volledig afhankelijk van IT. Elk bedrijf moet zijn IT transformeren, daar gaat de digitale transformatie over. Bedrijven die dat nog niet hebben gedaan, moeten hun digitale transformatie nu zo snel mogelijk realiseren, anders bestaat het risico dat ze uit de markt worden geveegd. Digitale transformatie is een voorwaarde om te overleven. Omdat de inzet zo hoog is, kan een bedrijf de digitale transformatie niet volledig overlaten aan een leverancier. Elk bedrijf moet de handen uit de mouwen steken met zijn IT, wat betekent dat elk bedrijf de handen uit de mouwen moet steken met open source software, want zonder open source software bestaat er geen IT.

Verwachte voordelen van de digitale transformatie zijn onder meer:

- Vereenvoudig, automatiseer kernprocessen, maak ze real-time.
- Snelle reacties op veranderingen in de concurrentie mogelijk maken.
- Profiteer van kunstmatige intelligentie en big data.

### Beoordeling van kansen

Digitale transformatie zou kunnen worden beheerd door:

- Segmenten van de IT: Productie IT, Business Support IT (CRM, facturatie, inkoop...), Support IT (HR, Financiën, boekhouding...), Big Data.
- Type technologie of proces dat de IT ondersteunt: Infrastructuur (cloud), Kunstmatige Intelligentie, Processen (Make-or-Buy, DevSecOps, SaaS).

Het injecteren van open source in een bepaald segment of technologie van uw IT laat zien dat u dit segment of deze technologie in handen wilt krijgen, omdat u van mening bent dat dit specifieke segment of deze technologie van uw IT belangrijk is voor het concurrentievermogen van uw bedrijf. Het is belangrijk om de positie van uw bedrijf te beoordelen, niet alleen ten opzichte van uw concurrenten, maar ook ten opzichte van andere industrieën en belangrijke spelers in termen van klantervaring en marktoplossingen.

### Voortgangsbeoordeling

1. Niveau 1: beoordeling van de situatie

- Ik heb geïdentificeerd:
   - de IT-segmenten die belangrijk zijn voor het concurrentievermogen van mijn bedrijf, en
   - de open source technologieën die nodig zijn om toepassingen in deze segmenten te ontwikkelen.
- En ik heb dus besloten:
   - over welke segmenten ik intern de ontwikkeling van projecten wil beheren, en
   - over welke open source technologieën ik in-house expertise moet opbouwen.

1. Niveau 2: Betrokkenheid

- Voor enkele geselecteerde open source technologieën die binnen het bedrijf worden gebruikt, zijn verschillende ontwikkelaars opgeleid en erkend als waardevolle bijdragers door de open source gemeenschap.

In enkele geselecteerde segmenten zijn projecten op basis van open source technologieën gelanceerd.

1. Niveau 3: Generalisatie

- Voor alle projecten wordt in de beginfase van het project systematisch een open source alternatief onderzocht. Om het voor het projectteam gemakkelijker te maken een dergelijk open source alternatief te bestuderen, wordt een centraal budget en een centraal team van architecten, ondergebracht bij de IT-afdeling, ingezet om de projecten bij te staan.

**KPI's**:

- KPI 1. Verhouding waarvoor een open source alternatief is onderzocht: (Aantal projecten / Totaal aantal projecten).
- KPI 2. Verhouding waarvoor het open source alternatief is gekozen: (Aantal projecten / Totaal aantal projecten).

### Aanbevelingen

Afgezien van de headline is digitale transformatie een mentaliteit die een aantal fundamentele veranderingen met zich meebrengt, en die moet ook (of zelfs vooral) komen van de bovenste lagen van de organisatie. Het management moet initiatieven en nieuwe ideeën bevorderen, risico's beheren en eventueel bestaande procedures aanpassen aan de nieuwe concepten.

Passie is een enorme succesfactor. Een van de middelen die de belangrijkste spelers op dit gebied hebben ontwikkeld, is het opzetten van open ruimtes voor nieuwe ideeën, waar mensen hun ideeën over digitale transformatie kunnen indienen en er vrijelijk aan kunnen werken. Het management moet dergelijke initiatieven aanmoedigen.

### Middelen

- [Eclipse Foundation: Enabling Digital Transformation in Europe Through Global Open Source Collaboration](https://outreach.eclipse.foundation/hubfs/EuropeanOpenSourceWhitePaper-June2021.pdf).
- [Europa: Open source software strategie](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).
- [Europa: Open source software strategie 2020-2023](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
