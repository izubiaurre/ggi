## Publiekelijk bevestigen van het gebruik van open source

Activity ID: [GGI-A-31](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_31.md).

### Beschrijving

Deze activiteit gaat over de erkenning van het gebruik van OSS in een informatiesysteem, in toepassingen en in nieuwe producten.

- Succesverhalen vertellen.
- Presenteren op evenementen.
- Financiering van deelname aan evenementen.

### Beoordeling van kansen

Het is nu algemeen aanvaard dat de meeste informatiesystemen op OSS draaien, en dat nieuwe toepassingen voor het grootste deel worden gemaakt door hergebruik van OSS.

Het belangrijkste voordeel van deze activiteit is het creëren van een gelijk speelveld tussen OSS en propriëtaire software, om ervoor te zorgen dat OSS evenveel aandacht krijgt en even professioneel wordt beheerd als propriëtaire software.

Een bijkomend voordeel is dat het OSS-ecosysteem hierdoor veel beter zichtbaar wordt en, aangezien OSS-gebruikers worden geïdentificeerd als "innovators", vergroot het ook de aantrekkelijkheid van de organisatie.

### Voortgangsbeoordeling

Uit de volgende **verificatiepunten** blijkt dat op dit gebied vooruitgang is geboekt:

- [ ] Commerciële open source leveranciers krijgen toestemming om de naam van de organisatie als klantreferentie te gebruiken.
- [ ] Bijdragers mogen dit doen en zich uitdrukken onder de naam van de organisatie.
- [ ] Het gebruik van OSS wordt openlijk vermeld in het jaarverslag van de IT-afdeling.
- [ ] Er is geen belemmering voor de organisatie om hun gebruik van OSS toe te lichten in de media (interviews, OSS en branche-evenementen, enz.).

### Aanbevelingen

- Het doel van deze activiteit is niet dat de organisatie een activistisch OSS-orgaan wordt.

### Middelen

- Voorbeeld van [CERN](https://superuser.openstack.org/articles/cern-openstack-update/) die publiekelijk hun gebruik van OpenStack bevestigen
