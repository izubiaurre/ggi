# Conclusie

Zoals we eerder hebben gezegd, is het goed besturen van open source goed geen bestemming; het is een reis. We moeten ons bekommeren om onze gemeenschappelijke bezittingen, om de gemeenschappen en het ecosysteem die ervoor zorgen dat ze streven, omdat ons eigen gemeenschappelijke en dus individuele succes ervan afhangt.

**Wij**, als software beoefenaars en open source enthousiastelingen, zetten ons in om het Good Governance Initiative handboek voor goed bestuur van open source te blijven verbeteren en te werken aan de verspreiding en het bereik ervan. Wij zijn ervan overtuigd dat organisaties, individuen en gemeenschappen hand in hand moeten werken om een betere en grotere reeks commons op te bouwen, die voor iedereen beschikbaar en nuttig zijn.

**U** bent welkom bij de OSPO Alliance, draag bij aan ons werk, verspreid het woord, en wees de ambassadeur van een beter open source bewustzijn en bestuur binnen uw eigen ecosysteem. Er zijn heel wat hulpmiddelen beschikbaar, van blogposts en onderzoeksartikelen tot conferenties en online opleidingen. Wij bieden ook een reeks nuttig materiaal aan op [onze website](https://ospo-alliance.org), en wij zijn graag bereid om zoveel mogelijk te helpen.

**Laten we samen de toekomst van de Good Governance Initiative bepalen en opbouwen!**

## Neem contact op met

De beste manier om in contact te komen met de OSPO Alliance is door een bericht te plaatsen op onze openbare mailinglijst op <https://accounts.eclipse.org/mailing-list/ospo.zone>. U kunt ook met ons komen discussiëren op de gebruikelijke open source evenementen, deelnemen aan onze maandelijkse OSPO OnRamp webinars, of contact opnemen met een lid -- zij zullen u vriendelijk doorverwijzen naar de juiste persoon.

## Bijlage: Sjabloon voor een aangepaste activiteitenscorekaart

De laatste versie van de aangepaste activiteitenscorekaart is beschikbaar in de sectie `resources` van de [Good Governance Initiative GitLab](https://gitlab.ow2.org/ggi/ggi) op OW2.
