## Beheer van open source vaardigheden en middelen

Activiteit ID: [GGI-A-42](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_42.md).

### Beschrijving

Deze activiteit is gericht op vaardigheden en middelen rondom **softwareontwikkeling**. De actitiveit omvat de technologieën en specifieke ontwikkelingsvaardigheden van de ontwikkelaars, alsmede de algemene ontwikkelingsprocessen, -methoden en -instrumenten.

Er is een enorme hoeveelheid documentatie, forums en discussies die voortkomen uit het ecosysteem, en openbare bronnen beschikbaar voor open source technologieën. Om ten volle te profiteren van hun open source-aanpak moet men een routekaart opstellen van de huidige activa en de gewenste doelstellingen om een consistent programma op te zetten voor ontwikkelingsvaardigheden, -methoden en -tools binnen de teams.

#### Toepassingsgebieden

Men moet vaststellen in welke domeinen het programma zal worden toegepast, en hoe het de kwaliteit en de efficiëntie van de code en de praktijken zal verbeteren. Het programma zal bijvoorbeeld niet dezelfde voordelen hebben als er slechts één ontwikkelaar aan open source componenten werkt, of als de hele ontwikkelingslevenscyclus wordt geoptimaliseerd om open source best practices op te nemen.

Men moet het toepassingsgebied voor open source ontwikkeling bepalen: technische componenten, toepassingen, modernisering of nieuwe ontwikkeling. Voorbeelden van ontwikkelingspraktijken die baat kunnen hebben bij open source zijn:

- Cloud administratie.
- Cloud-native toepassingen, hoe te innoveren met deze technologieën.
- DevOps, voortdurende integratie / voortdurende levering.

#### Categorieën

- Vaardigheden en middelen die nodig zijn om open source software te ontwikkelen: intellectueel eigendom, licenties, praktijken.
- Vaardigheden en middelen die nodig zijn om software te ontwikkelen met behulp van open source componenten, -talen en -technologieën.
- Vaardigheden en middelen die nodig zijn om open source methoden en -processen te gebruiken.

### Beoordeling van kansen

Open source tools worden steeds populairder onder ontwikkelaars. Deze activiteit richt zich op de noodzaak om de verspreiding van heterogene tools binnen een ontwikkelteam te voorkomen. Het helpt bij het bepalen van een beleid op dit gebied. Het helpt training en ervaringsopbouw te optimaliseren. Een inventarisatie van vaardigheden wordt gebruikt voor werving, training en opvolgingsplanning voor het geval een sleutelmedewerker het bedrijf verlaat.

We zouden een methodologie nodig hebben om de vaardigheden op het gebied van open source softwareontwikkeling in kaart te brengen.

### Voortgangsbeoordeling

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] Er is een beschrijving van de open source productieketen (de "software supply chain"),
- [ ] Er is een plan (of een verlanglijstje) voor de rationalisatie van de ontwikkelingsmiddelen,
- [ ] Er is een inventaris van vaardigheden waarin de vaardigheden, opleiding en ervaring van de huidige ontwikkelaars worden samengevat,
- [ ] Er is een verlanglijst voor training en een programma om hiaten in vaardigheden aan te pakken,
- [ ] Er is een lijst met ontbrekende best practices voor open source ontwikkeling en een plan om die toe te passen.

### Aanbevelingen

- Begin eenvoudig, laat de analyse en het stappenplan gestaag groeien.
- Leg bij de aanwerving sterk de nadruk op open source vaardigheden en ervaring. Het is altijd gemakkelijker wanneer mensen al een open source DNA hebben dan mensen op te leiden en te coachen.
- Bekijk opleidingsprogramma's van softwareleveranciers en open source opleiders.

### Middelen

Meer informatie:

- Een inleiding tot [what is a Skills Inventory?](https://managementisajourney.com/management-toolbox-better-decision-making-with-a-skills-inventory) van Robert Tanner.
- Een artikel over open source vaardigheden: [5 Open Source Skills to Up Your Game and Your Resume](https://sourceforge.net/blog/5-open-source-skills-game-resume/)

Deze activiteit kan technische middelen en vaardigheden omvatten zoals:

- **Populaire talen** (zoals Java, PHP, Perl, Python).
- **Open source frameworks** (Spring, AngularJS, Symfony) en test hulpmiddelen.
- Agile, DevOps en open source **ontwikkelingsmethoden en best practices**.

Aanverwante activiteiten:

- [GGI-A-28 HR perspectief](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_28.md)
