## Bevorderen van best practices voor open source-ontwikkeling

Activity ID: [GGI-A-25](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_25.md).

### Beschrijving

Deze activiteit betreft het definiëren, actief bevorderen en implementeren van best practices op het gebied van open source binnen de ontwikkelingsteams.

Als uitgangspunt kunnen de volgende onderwerpen in aanmerking worden genomen:

- Documentatie voor gebruikers en ontwikkelaars.
- Correcte organisatie van het project op een publiek toegankelijke repository.
- Bevorderen en uitvoeren van gecontroleerd hergebruik.
- Het verstrekken van een volledige en actuele productdocumentatie.
- Configuratiebeheer: git workflows, samenwerkingspatronen.
- Releasebeheer: vroeg en vaak uitbrengen, stabiele versus ontwikkelingsversies, enz.

OSS-projecten hebben een speciale, [bazaar-achtige](http://www.catb.org/~esr/writings/cathedral-bazaar/) modus operandi. Om deze samenwerking en mentaliteit mogelijk te maken en te bevorderen, worden enkele werkwijzen aanbevolen die gezamenlijke en gedecentraliseerde ontwikkeling en bijdragen van externe ontwikkelaars vergemakkelijken…

#### Gemeenschappelijke documenten

Zorg ervoor dat alle projecten binnen het bedrijf de volgende documenten gebruiken:

- README -- snelle beschrijving van het project, hoe ermee om te gaan, links naar bronnen.
- Bijdragen -- introductie voor mensen die bereid zijn bij te dragen.
- Gedragscode -- Wat is aanvaardbaar -- of niet -- als gedrag binnen de gemeenschap.
- LICENSE -- de standaard licentie van het archief.

#### REUSE: Beste praktijken voor REUSE

[REUSE](https://reuse.software) is een initiatief van de [Free Software Foundation Europe](https://fsfe.org/) om het hergebruik van software te verbeteren en de compliance van OSS en licenties te stroomlijnen.

### Beoordeling van kansen

Hoewel het sterk afhangt van de gemeenschappelijke OSS kennis van het team, is het altijd nuttig om mensen op te leiden en processen te creëren die deze praktijken afdwingen. Het is nog belangrijker wanneer:

- potentiële gebruikers en medewerkers zijn niet bekend,
- ontwikkelaars niet gewend zijn aan open source ontwikkeling.

### Voortgangsbeoordeling

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] Project stelt een lijst op van open source best practices om aan te voldoen.
- [ ] Project bewaakt de afstemming met best practices.
- [ ] Het ontwikkelingsteam heeft bewustzijn opgebouwd over het naleven van best practices voor OSS.
- [ ] Nieuwe best practices worden regelmatig geëvalueerd en er wordt naar gestreefd deze te implementeren.

### Hulpmiddelen

- De [REUSE helper tool](https://github.com/fsfe/reuse-tool) helpt bij het conform maken van een repository met de [REUSE](https://reuse.software) best practices. Het kan in veel ontwikkelingsprocessen worden opgenomen om de huidige status te bevestigen.
- [ScanCode](https://scancode-toolkit.readthedocs.io) heeft de mogelijkheid om alle gemeenschaps- en juridische documenten in het archief op te sommen: zie [functiebeschrijving](https://scancode-toolkit.readthedocs.io/en/latest/cli-reference/scan-options-pre.html#classify).
- GitHub heeft een leuke functie om te [controleren op ontbrekende gemeenschapsdocumenten](https://docs.github.com/articles/viewing-your-community-profile). Het is te vinden in de Repository pagina > "Insights" > "Community".

### Aanbevelingen

- De lijst met best practices hangt af van de context en het domein van het programma en moet regelmatig opnieuw worden geëvalueerd om continu te verbeteren. De best practices moeten worden gecontroleerd en regelmatig worden beoordeeld om de voortgang te monitoren.
- Train mensen over OSS hergebruik (als consumenten) en ecosystemen (als bijdragers).
- Implementeer REUSE.software zoals in activiteit #14.
- Zet een proces op om de juridische risico's in verband met hergebruik en bijdragen te beheren.
- Moedig ensen expliciet aan om bij te dragen aan externe projecten.
- Zorg voor een sjabloon of officiële richtlijnen voor de projectstructuur.
- Stel geautomatiseerde controles in om ervoor te zorgen dat alle projecten aan de richtlijnen voldoen.

### Middelen

- [OW2's lijst van open source best practices](https://www.ow2.org/view/MRL/Full_List_of_Best_Practices) van de Market Readiness Levels beoordelingsmethodologie.
- [De officiële website van REUSE](https://reuse.software) met specificaties, handleiding en FAQ.
- [GitHub's gemeenschapsrichtlijnen](https://opensource.guide/).
- Een voorbeeld van [configuration management best practices using GitHub](https://dev.to/datreeio/top-10-github-best-practices-3kl2).
