## HR-perspectief

Activity ID: [GGI-A-28](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_28.md).

### Beschrijving

De overstap naar een open-source cultuur heeft grote gevolgen voor de HR:

- **Nieuwe processen en contracten**: Contracten moeten worden aangepast om externe bijdragen mogelijk te maken en te bevorderen. Dit omvat IP- en licentiekwesties voor werk dat binnen het bedrijf wordt gedaan, maar ook de mogelijkheid voor de werknemer of contractant om zijn eigen projecten te hebben.
- **Verschillende soorten mensen**: Mensen die werken met open source hebben vaak andere incentives en mindsets dan mensen die puur propriëtair, corporate werken. Processen en denkwijzen moeten worden aangepast aan dit op de reputatie van de gemeenschap gerichte paradigma om nieuwe soorten talent aan te trekken en te behouden.
- **Carrièreontwikkeling**: de noodzaak om een carrièrepad aan te bieden dat werknemers koestert en waardeert voor hun technische, zachte vaardigheden, en de competenties die uw organisatie verwacht (samenwerking om de inspanningen van de gemeenschap te stimuleren, communicatie om als woordvoerder van uw bedrijf op te treden, enz.) HR heeft hoe dan ook een sleutelrol in het mogelijk maken van open source als cultureel doel.

**Workforce** Voor een ontwikkelaar die lange tijd met dezelfde propriëtaire oplossing heeft gewerkt, kan de overstap naar open source een hele verandering lijken en aanpassing vergen. Maar voor de meeste ontwikkelaars brengt open source software alleen maar voordelen met zich mee.

Ontwikkelaars die vandaag de dag van school of universiteit komen, werken allemaal met open source. Binnen een bedrijf gebruikt de grote meerderheid van de ontwikkelaars open source talen en importeert dagelijks open source bibliotheken of snippets. Het is inderdaad veel gemakkelijker om regels open source code in een programma te plakken dan het interne sourcing proces in gang te zetten, dat via meerdere validaties via de managementlijn escaleert.

Open source maakt het werk van de ontwikkelaar interessanter, want met open source is een ontwikkelaar altijd op zoek naar wat zijn collega's buiten het bedrijf hebben uitgevonden, en blijft daardoor op het scherpst van de snede van de technologie.

Voor een organisatie moet er een HR-strategie zijn om 1/ het bestaande personeelsbestand op te leiden of bij te scholen 2/ het bedrijf te positioneren bij het aantrekken van nieuwe talenten, dus wat is de aantrekkelijkheid van het bedrijf als het gaat om open source.

> Het is prachtig om mensen te krijgen met een goede FLOSS-mentaliteit, die de code al begrijpen en goed met anderen kunnen samenwerken. Het alternatief van evangeliseren / opleiden / stage lopen is de moeite waard, maar duurder en tijdrovender.
>
> &mdash; <cite>CEO van OSS software leverancier</cite>

Dit illustreert dat het inhuren van mensen met open source DNA een versnelling is om te overwegen binnen de HR-strategie.

#### Processen

- functiebeschrijvingen opstellen of herzien (technische vaardigheden, zachte vaardigheden, competenties en ervaringen)
- Opleidingsprogramma's: zelfopleiding, formele opleiding, management coaching, peer mapping, gemeenschappen
- Loopbaantraject vaststellen of herzien: competenties, belangrijkste resultaten/effecten en loopbaanstappen

### Beoordeling van kansen

1. Kaderontwikkelingspraktijken: het probleem is waarschijnlijk niet zozeer om ontwikkelaars aan te sporen meer open source te gebruiken, maar eerder om ervoor te zorgen dat zij die veilig gebruiken, in overeenstemming met de licentievoorwaarden van elke open source-technologie, en zonder de traditionele veiligheidscontroles los te laten (open source coderegels zouden kwaadaardige codes kunnen bevatten),
1. Samenwerkingspraktijken herzien: met ontwikkelingspraktijken bestaat de mogelijkheid om de flexibiliteit en samenwerking uit te breiden naar andere bedrijfsonderdelen binnen uw organisatie. Inner sourcing wordt vaak gebruikt om dit gedrag te bevorderen, hoewel dit misschien de helft is van de weg naar een open source-cultuur,
1. Organisatiecultuur: uiteindelijk gaat het om de cultuur binnen uw organisatie: open source kan het vlaggenschip zijn voor waarden als openheid, samenwerking, ethiek, duurzaamheid.

### Voortgangsbeoordeling

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] Training is beschikbaar voor het presenteren van zowel de voordelen als de beperkingen (naleving van licentievoorwaarden voor intellectueel eigendom) met betrekking tot open source.
- [ ] Elke ontwikkelaar, elke architect, elke projectleider (of Product Owner/Business Owner) begrijpt de voordelen en de beperkingen (naleving van licentievoorwaarden voor intellectueel eigendom) van open source.
- [ ] Ontwikkelaars worden aangemoedigd om bij te dragen aan open source-gemeenschappen en er verantwoordelijkheid voor te nemen, en kunnen daarvoor een adequate opleiding krijgen.
- [ ] Vaardigheden en competenties komen tot uiting in functiebeschrijvingen en loopbaanstappen van de organisatie.
- [ ] De ervaring die ontwikkelaars hebben opgedaan in open source (bijdragen aan open source gemeenschappen, deelname aan het interne compliance proces, externe woordvoerders van het bedrijf, ...) wordt meegenomen in het HR-evaluatieproces.

### Hulpmiddelen

- Vaardigheden matrix.
- Openbare opleidingsprogramma's (bijvoorbeeld open source school).
- Sourcing: GitHub, GitLab, LinkedIn, Meetups, Epitech, Epita…
- Contract sjablonen (Loyaliteits clausule).
- Functiebeschrijvingen (sjablonen) & loopbaanstappen (sjablonen).

### Aanbevelingen

Meestal kennen ontwikkelaars tegenwoordig al enkele open source principes en zijn ze bereid om met en aan open source software te werken. Er zijn echter nog enkele acties die het management zou moeten ondernemen:

- Voorkeur voor OSS ervaring bij het werven, ook al heeft de baan waarvoor de ontwikkelaar wordt aangenomen alleen betrekking op propriëtaire technologie. Met de digitale transformatie is de kans groot dat de ontwikkelaar ooit aan open source zal moeten werken.
- OSS-trainingsprogramma: Elke ontwikkelaar, elke architect, elke projectleider (of Product Owner/Business Owner), zou toegang moeten hebben tot trainingsmiddelen (video's of face-to-face training) die de voordelen van open source presenteren en ook de beperkingen in termen van intellectueel eigendom en licentie compliance.
- Er moet opleiding beschikbaar komen voor ontwikkelaars die willen bijdragen aan open source-gemeenschappen en deel willen uitmaken van de bestuursorganen van deze gemeenschappen (Linux certificeringen).
- Erkenning in de HR persoonlijke beoordelingsprocessen van de bijdrage van de werknemer (ontwikkelaar of architect) aan open source gerelateerde onderwerpen zoals bijdragen aan open source gemeenschappen en naleving van licentievoorwaarden voor intellectueel eigendom. De meeste onderwerpen zijn gemeenschappelijk en passen in technische carrièrepaden, terwijl sommige specifiek kunnen of moeten zijn.
- Best bewaarde geheim en bedrijfshouding: de communicatie aspecten moeten aan bod komen (hoe belangrijk is dit voor uw organisatie dat het in uw jaarverslag kan worden opgenomen), hoe beïnvloedt het uw communicatiehouding (een open source-bijdrager kan een woordvoerder zijn voor uw bedrijf, inclusief perscontacten).

### Middelen

- Wat betreft de mogelijkheid van mensen om tijdens evenementen buiten het bedrijf te spreken, zie activiteit 31: "(Engagement doel) Het gebruik van open source publiekelijk bevestigen".
