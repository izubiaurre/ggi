## Uitvoeren van code-reviews

Activity ID: [GGI-A-44](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_44.md).

### Beschrijving

Codebeoordeling is een routinetaak waarbij handmatige en/of geautomatiseerde review van de broncode van een applicatie nodig is voordat een product wordt vrijgegeven of een project aan de klant wordt geleverd. In het geval van open source software is code review meer dan alleen het (opportunistisch) opsporen van fouten; het is een geïntegreerde benadering van gezamenlijke ontwikkeling die op teamniveau wordt uitgevoerd.

Codebeoordelingen moeten zowel gelden voor code die intern is ontwikkeld als voor code die is hergebruikt uit externe bronnen, aangezien dit het algemene vertrouwen in code verbetert en eigenaarschap versterkt. Het is ook een uitstekende manier om de wereldwijde vaardigheden en kennis binnen het team te verbeteren en de teamsamenwerking te bevorderen.

### Beoordeling van kansen

Codebeoordelingen zijn waardevol wanneer de organisatie software ontwikkelt of externe software hergebruikt. Hoewel het een standaardstap is in het software engineeringproces, bieden codebeoordelingen in de context van open source specifieke voordelen, zoals:

- Controleer bij het publiceren van interne broncode of de juiste kwaliteitsrichtlijnen worden gerespecteerd.
- Controleer bij het bijdragen aan een bestaand open source project of de richtlijnen van het beoogde project worden gerespecteerd.
- De openbaar beschikbare documentatie wordt dienovereenkomstig bijgewerkt.

Het is ook een uitstekende gelegenheid om enkele van de beleidsregels van uw bedrijf op het gebied van compliance te delen en af te dwingen, zoals:

- Verwijder nooit bestaande licentieheaders of copyrights in hergebruikte open sourcecode.
- Kopieer en plak de broncode van Stack Overflow niet zonder voorafgaande toestemming van het juridische team.
- Voeg indien nodig de juiste copyrightregel toe.

Codebeoordelingen zullen vertrouwen in code brengen. Als mensen niet zeker zijn van de kwaliteit of potentiële risico's van het gebruik van een softwareproduct, moeten ze peer- en code-reviews uitvoeren.

### Voortgangsbeoordeling

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] Open source codebeoordeling wordt erkend als een noodzakelijke stap.
- [ ] Open source codebeoordelingen zijn regelmatig of op kritieke momenten gepland.
- [ ] Een proces voor het uitvoeren van open source codebeoordelingen is collectief gedefinieerd en geaccepteerd.
- [ ] Open source codebeoordelingen zijn een standaard onderdeel van het ontwikkelingsproces.

### Aanbevelingen

- Codebeoordeling is een collectieve taak die beter werkt in een goede samenwerkingsomgeving.
- Schroom niet om gebruik te maken van bestaande tools en patronen uit de open source wereld, waar codebeoordelingen al jaren (decennia) standaard zijn.

### Middelen

- [What is Code Review?](https://openpracticelibrary.com/practice/code-review/): een didactische lezing over codebeoordeling gevonden in de Open Practice Library van Red Hat.
- [Best Practices for Code Reviews](https://www.perforce.com/blog/qac/9-best-practices-for-code-review): nog een interessant perspectief op wat codebeoordeling inhoudt.
