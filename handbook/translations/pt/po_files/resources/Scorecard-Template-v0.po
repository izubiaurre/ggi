# ssantos <ssantos@web.de>, 2022, 2023.
# Gerardo Lisboa <gvlx@sapo.pt>, 2023.
# Tiago Lucas <tiago.slucas@ua.pt>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-05 16:50+0200\n"
"PO-Revision-Date: 2023-06-17 11:08+0000\n"
"Last-Translator: Tiago Lucas <tiago.slucas@ua.pt>\n"
"Language-Team: Portuguese <https://hosted.weblate.org/projects/ospo-zone-ggi/scorecard-template/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.18.1\n"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[0]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "Goal/Activity"
msgstr "Objetivo/Atividade"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[0]/table:table-cell[0]/text:p[0]/text:span[2]
msgid "Culture 1"
msgstr "Cultura 1"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[0]/table:table-cell[1]/text:p[0]
msgid "Promote open source development best practices "
msgstr "Promover as práticas recomendadas de desenvolvimento de código aberto "

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[0]/table:table-cell[2]/text:p[0]/text:span[0]
msgid "Last update "
msgstr "Última atualização "

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "Customized Description"
msgstr "Descrição personalizada"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[0]/text:p[0]/text:span[1]
msgid "Scope of what has to be done"
msgstr "Âmbito do que tem que ser feito"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[0]/text:p[1]
msgid "Brief essential description..."
msgstr "Breve descrição essencial..."

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Brief highlights.."
msgstr "Breves destaques.."

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[1]/text:p[0]/text:span[0]
msgid "Opportunity Assessment"
msgstr "Avaliação de oportunidades"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[1]/text:p[0]/text:span[1]
msgid "Why is this activity relevant"
msgstr "Por que essa atividade é relevante"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[1]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Key pain points..."
msgstr "Pontos principais de dor..."

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[1]/text:list[0]/text:list-item[1]/text:p[0]
msgid "Key progress opportunities..."
msgstr "Oportunidades principais de progresso..."

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "Objectives "
msgstr "Objetivos "

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[0]/text:p[0]/text:span[1]
msgid "What we aim to achieve in this iteration"
msgstr "O que pretendemos alcançar nesta iteração"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Objective 1..."
msgstr "Objetivo 1..."

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[0]/text:list[0]/text:list-item[1]/text:p[0]
msgid "Objective 2..."
msgstr "Objetivo 2..."

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[1]/text:p[0]/text:span[0]
msgid "Tools"
msgstr "Ferramentas"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[1]/text:p[0]/text:span[2]
msgid "Technologies, "
msgstr "Tecnologias, "

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[1]/text:p[0]/text:span[3]
msgid "tools and products used in the Activity"
msgstr "ferramentas e produtos utilizados na Atividade"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[1]/text:list[0]/text:list-item[0]/text:p[0]/text:a[0]/text:span[0]
msgid "Resources"
msgstr "Recursos"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[1]/text:list[0]/text:list-item[0]/text:p[0]/text:span[0]
msgid "..."
msgstr "..."

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[2]/text:p[0]/text:span[0]
msgid "Operational Notes"
msgstr "Notas operacionais"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[2]/text:p[0]/text:span[3]
msgid "Approach, method to progress in the Activity"
msgstr "Abordagem, método para progredir na Atividade"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[2]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Start with..."
msgstr "Começar com..."

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[3]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "Key Result "
msgstr "Resultado-chave "

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[3]/table:table-cell[0]/text:p[0]/text:span[1]
msgid "How we will measure success in this iteration"
msgstr "Como mediremos o sucesso nesta iteração"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[3]/table:table-cell[1]/text:p[0]
msgid "Progress"
msgstr "Progresso"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[3]/table:table-cell[2]/text:p[0]
msgid "Score"
msgstr "Pontuação"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[3]/table:table-cell[3]/text:p[0]
msgid "Personal Assessment"
msgstr "Avaliação pessoal"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Key result 1 (minimum one key result)"
msgstr "Resultado-chave 1 (mínimo de um resultado-chave)"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[1]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[1]/text:p[0]office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[1]/text:p[0]office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[1]/text:p[0]"
msgid "xx%"
msgstr "xx%"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[2]/text:p[0]
msgid ".9"
msgstr "0,9"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[3]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[3]/text:p[0]office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[3]/text:p[0]office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[3]/text:p[0]"
msgid "Personal comment"
msgstr "Comentário pessoal"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Key result 2"
msgstr "Resultado-chave 2"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[1]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[1]/text:p[0]"
msgid "xx%"
msgstr "xx%"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[2]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[2]/text:p[0]"
msgid ".5"
msgstr "0,5"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[3]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[3]/text:p[0]"
msgid "Personal comment"
msgstr "Comentário pessoal"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Key result 3"
msgstr "Resultado-chave 3"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[1]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[1]/text:p[0]"
msgid "xx%"
msgstr "xx%"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[2]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[2]/text:p[0]"
msgid ".5"
msgstr "0,5"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[3]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[3]/text:p[0]"
msgid "Personal comment"
msgstr "Comentário pessoal"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[7]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Key result 4 (maximum four key results)"
msgstr "Resultado-chave 4 (máximo de quatro resultados-chave)"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[7]/table:table-cell[1]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[7]/table:table-cell[1]/text:p[0]"
msgid "xx%"
msgstr "xx%"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[7]/table:table-cell[2]/text:p[0]
msgid ".0"
msgstr "0,0"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[7]/table:table-cell[3]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[7]/table:table-cell[3]/text:p[0]"
msgid "Personal comment"
msgstr "Comentário pessoal"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[8]/table:table-cell[2]/text:p[0]
msgid ".475"
msgstr "0,475"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "Timeline"
msgstr "Linha do tempo"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[0]/text:p[0]/text:span[3]
msgid "Start-End dates, Milestones"
msgstr "Datas de início e fim, marcos"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Date indication here"
msgstr "Indicação da data aqui"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[1]/text:p[0]/text:span[0]
msgid "Efforts"
msgstr "Esforços"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[1]/text:p[0]/text:span[2]
msgid "Time and material budget"
msgstr "Orçamento de tempo e material"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[1]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Time allocation over the next three months"
msgstr "Alocação de tempo nos próximos três meses"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[1]/text:list[0]/text:list-item[1]/text:p[0]
msgid "Budget allowance"
msgstr "Subsídio orçamental"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[2]/text:p[0]/text:span[0]
msgid "Assignees"
msgstr "Consignatários"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[2]/text:p[0]/text:span[2]
msgid "Who participates? Leads?"
msgstr "Quem participa? Líderes?"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[2]/text:list[0]/text:list-item[0]/text:p[0]
msgid "XX to prepare internal presentation"
msgstr "XX para preparar a apresentação interna"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "Issues"
msgstr "Problemas"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[0]/text:p[0]/text:span[2]
msgid "Difficulties, uncertainties, roadblocks, points of attention, dependencies"
msgstr "Dificuldades, incertezas, bloqueios, pontos de atenção, dependências"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]/text:span[0]
msgid "Concern 1..."
msgstr "Preocupação 1..."

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[0]/text:list[0]/text:list-item[1]/text:p[0]
msgid "Concern 2..."
msgstr "Preocupação 2..."

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[1]/text:p[0]/text:span[0]
msgid "Status"
msgstr "Estado"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[1]/text:p[0]/text:span[1]
msgid "How the Activity is doing"
msgstr "Como a Atividade está indo"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[1]/text:list[0]/text:list-header[0]/text:p[0]
msgid "Personal comment on the health of the Activity"
msgstr "Comentário pessoal sobre a saúde da Atividade"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[11]/table:table-cell[0]/text:p[0]
msgid "Overall Progress Rating"
msgstr "avaliação global do progresso"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[11]/table:table-cell[1]/text:p[0]
msgid "XX%"
msgstr "XX%"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[12]/table:table-cell[0]/text:p[0]
msgid "Notes"
msgstr "Anotações"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[1]/table:table-row[0]/table:table-cell[0]/text:p[0]
msgid "Insights from the GitLab Activity Forum"
msgstr "Perceções do fórum de atividades do GitLab"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[1]/table:table-row[1]/table:table-cell[0]/text:p[0]
msgid "https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/25"
msgstr "https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/25"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[1]/table:table-row[1]/table:table-cell[0]/text:p[2]
msgid "Copy/paste here the content of the Activity description from https://gitlab.ow2.org/ggi/ggi-castalia/"
msgstr "Copie/cole aqui o conteúdo da descrição da Atividade de https://gitlab.ow2.org/ggi/ggi-castalia/"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[1]/table:table-row[1]/table:table-cell[0]/text:p[4]
msgid "This will serve as a reference to help develop the Customized Activity Scorecard"
msgstr "Servirá como referência para ajudar a desenvolver o Quadro Personalizado de Pontuação de Atividades"

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:header[0]/table:table[0]/table:table-row[0]/table:table-cell[0]/text:p[0]
msgid "OW2 OSS Good Governance initiative"
msgstr "Good Governance Initiative de OSS da OW2"

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:header[0]/table:table[0]/table:table-row[0]/table:table-cell[1]/text:p[0]
msgid "THE GOOD EXAMPLE COMPANY"
msgstr "A EMPRESA EXEMPLO BOA"

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:header[0]/table:table[0]/table:table-row[0]/table:table-cell[2]/text:p[0]
msgid "Customised Activity Scorecard"
msgstr "Quadro de pontuação de atividades personalizado"

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:footer[0]/table:table[0]/table:table-row[0]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "File: "
msgstr "Ficheiro: "

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:footer[0]/table:table[0]/table:table-row[0]/table:table-cell[1]/text:p[0]/text:span[0]
msgid "Last revision:"
msgstr "Última revisão:"

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:footer[0]/table:table[0]/table:table-row[0]/table:table-cell[2]/text:p[0]/text:span[0]
msgid "By:"
msgstr "Por:"

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:footer[0]/table:table[0]/table:table-row[0]/table:table-cell[3]/text:p[0]/text:span[0]
msgid "Page: "
msgstr "Página: "

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:footer[0]/table:table[0]/table:table-row[0]/table:table-cell[3]/text:p[0]/text:span[1]
msgid "/"
msgstr "/"
