#
msgid ""
msgstr ""
"PO-Revision-Date: 2022-09-28 18:44+0000\n"
"Last-Translator: ssantos <ssantos@web.de>\n"
"Language-Team: Portuguese <https://hosted.weblate.org/projects/ospo-zone-ggi/51_0_usage/pt/>\n"
"Language: pt\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.14.1\n"

#. Top level goal headline
#: ../content/51_0_usage.md:block 1 (header)
msgid "Usage goal activities"
msgstr "Atividades de objetivos de utilização"
