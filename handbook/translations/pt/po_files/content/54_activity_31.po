# ssantos <ssantos@web.de>, 2022.
msgid ""
msgstr ""
"PO-Revision-Date: 2022-12-23 08:46+0000\n"
"Last-Translator: ssantos <ssantos@web.de>\n"
"Language-Team: Portuguese <https://hosted.weblate.org/projects/ospo-zone-ggi/54_activity_31/pt/>\n"
"Language: pt\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"

#. Activity's title
#: ../content/54_activity_31.md:block 1 (header)
msgid "Publicly assert use of open source"
msgstr "Afirmar publicamente a utilização de código aberto"

#. Just translate "Activity ID:" and leave the activty's code as it is
#: ../content/54_activity_31.md:block 2 (paragraph)
msgid "Activity ID: [GGI-A-31](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_31.md)."
msgstr "ID da atividade: [GGI-A-31](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_31.md)."

#: ../content/54_activity_31.md:block 3 (header)
msgid "Description"
msgstr "Descrição"

#. Description
#: ../content/54_activity_31.md:block 4 (paragraph)
msgid "This Activity is about acknowledging the use of OSS in an information system, in applications and in new products."
msgstr "Esta atividade consiste em reconhecer a utilização do OSS num sistema de informação, em aplicações e em produtos novos."

#. Acknowledging the use of OSS
#: ../content/54_activity_31.md:block 5 (unordered list)
msgid "Providing success stories."
msgstr "Fornecer histórias de sucesso."

#. Acknowledging the use of OSS
#: ../content/54_activity_31.md:block 5 (unordered list)
msgid "Presenting at events."
msgstr "Apresentar em eventos."

#. Acknowledging the use of OSS
#: ../content/54_activity_31.md:block 5 (unordered list)
msgid "Funding participation to events."
msgstr "Financiamento da participação em eventos."

#: ../content/54_activity_31.md:block 6 (header)
msgid "Opportunity Assessment"
msgstr "Avaliação de oportunidades"

#. Opportunity Assessment
#: ../content/54_activity_31.md:block 7 (paragraph)
msgid "It is now generally accepted that most information systems run on OSS, and that new applications are for the most part made by reusing OSS."
msgstr "É agora geralmente aceite que a maioria dos sistemas de informação funcionam no OSS, e que as novas aplicações são, na maioria, feitas através da reutilização do OSS."

#. Opportunity Assessment
#: ../content/54_activity_31.md:block 8 (paragraph)
msgid "The main benefit of this activity is to create a level playing field between OSS and proprietary software, to make sure OSS is paid equal attention to and managed just as professionally as proprietary software."
msgstr "O benefício principal desta atividade é de criar condições equitativas entre o OSS e o software proprietário, para garantir que o OSS é alvo de igual atenção e gerido de forma tão profissional como o software proprietário."

#. Opportunity Assessment
#: ../content/54_activity_31.md:block 9 (paragraph)
msgid "A side benefit is that it greatly helps raise the profile of the OSS ecosystem and, since OSS users are identified as \"innovators\" it also enhances the attractiveness of the organisation."
msgstr "Um benefício secundário é que ajuda muito a elevar o perfil do ecossistema OSS e, uma vez que os utilizadores do OSS são identificados como \"inovadores\", também aumenta a atratividade da organização."

#: ../content/54_activity_31.md:block 10 (header)
msgid "Progress Assessment"
msgstr "Avaliação do progresso"

#. Progress Assessment
#: ../content/54_activity_31.md:block 11 (paragraph)
msgid "The following **verification points** demonstrate progress in this activity:"
msgstr "Os seguintes **pontos de verificação** demonstram o progresso nesta atividade:"

#. Progress assessment verification point
#: ../content/54_activity_31.md:block 12 (unordered list)
msgid "Commercial open source vendors are granted authorization to use the organisation's name as customer reference."
msgstr "Os vendedores comerciais de código aberto recebem autorização para utilizar o nome da organização como cliente de referência."

#. Progress assessment verification point
#: ../content/54_activity_31.md:block 12 (unordered list)
msgid "Contributors are allowed to do so and express themselves under the organisation's name."
msgstr "Contribuidores podem fazê-lo e expressar-se sob o nome da organização."

#. Progress assessment verification point
#: ../content/54_activity_31.md:block 12 (unordered list)
msgid "Use of OSS is openly mentioned in the IT department annual report."
msgstr "A utilização do OSS é abertamente mencionada no relatório anual do departamento de TI."

#. Progress assessment verification point
#: ../content/54_activity_31.md:block 12 (unordered list)
msgid "There is no obstacle to the organisation explaining their use of OSS in the media (interviews, OSS and industry events, etc.)."
msgstr "Não há nenhum obstáculo para a organização explicar o seu uso de OSS na média (entrevistas, OSS e eventos do setor, etc.)."

#: ../content/54_activity_31.md:block 13 (header)
msgid "Recommendations"
msgstr "Recomendações"

#. Recommendation
#: ../content/54_activity_31.md:block 14 (unordered list)
msgid "The objective of this activity is not for the organisation to become an OSS activism body, but to make sure there is no obstacle to the public recognising its use of OSS."
msgstr "O objetivo desta atividade não é que a organização se torne um organismo de ativismo do OSS, mas que se certifique de que não existe nenhum obstáculo para que o público reconheça a utilização dela do OSS."

#: ../content/54_activity_31.md:block 15 (header)
msgid "Resources"
msgstr "Recursos"

#. Resource
#: ../content/54_activity_31.md:block 16 (unordered list)
msgid "Example of [CERN](https://superuser.openstack.org/articles/cern-openstack-update/) publicly asserting their use of OpenStack"
msgstr "Exemplo de [CERN](https://superuser.openstack.org/articles/cern-openstack-update/) que afirma publicamente a utilização do OpenStack deles"

#~ msgid "Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/31>."
#~ msgstr "Ligação ao problema no GitLab: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/31>."

#~ msgid "Commercial open source vendors are granted authorization to use organisation's name as customer reference."
#~ msgstr "Os vendedores comerciais de código aberto recebem autorização para utilizar o nome da organização como cliente de referência."

#~ msgid "There is no obstacle to the organisation explaining their use of OSS in the media (interviews, OSS and industry event, etc.)."
#~ msgstr "Não há nenhum obstáculo para a organização explicar o seu uso de OSS na média (entrevistas, OSS e eventos do setor, etc.)."
