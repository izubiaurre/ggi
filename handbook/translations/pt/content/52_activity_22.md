## Gerir vulnerabilidades de software

Activity ID: [GGI-A-22](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_22.md).

### Descrição

O código de uma pessoa é tão seguro quanto a sua parte menos segura. Casos recentes (por exemplo, heartbleed[^heartbleed], equifax[^equifax]) demonstraram a importância de verificar as vulnerabilidades em partes do código que não são diretamente desenvolvidas pela entidade. As consequências da exposição vão desde perdas de dados (com tremendo impacto na reputação) a pedidos de resgate e indisponibilidade de serviços, ameaçando as empresas.

O software de código aberto é conhecido por ter uma melhor gestão de vulnerabilidades do que o software proprietário, principalmente porque:

- Mais olhos procuram e corrigem problemas em códigos e processos abertos.
- Projetos de código aberto corrigem vulnerabilidades e lançam patches e novas versões muito mais rapidamente.

Por exemplo, um [estudo da WhiteSource](https://resources.whitesourcesoftware.com/blog-whitesource/3-reasons-why-open-source-is-safer-than-commercial-software) sobre software proprietário mostrou que 95% das vulnerabilidades encontradas nos seus componentes de código aberto já tinham lançado uma correção na altura da análise. A questão, portanto, é de **gerir melhor as vulnerabilidades tanto no código desenvolvido como nas dependências do mesmo**, independentemente de serem de código fechado ou de código aberto.

Para mitigar estes riscos, é necessário criar um programa de avaliação dos seus ativos de software e um processo de verificação de vulnerabilidade executado regularmente. Implementar ferramentas que alertem as equipas afetadas, gerir as vulnerabilidades conhecidas e prevenir ameaças de dependências de software.

### Avaliação de oportunidades

Qualquer empresa que usa software deve observar as vulnerabilidades dele em:

- a sua infraestrutura (por exemplo, infraestrutura cloud, infraestrutura de rede, armazéns de dados),
- as suas aplicações comerciais (RH, ferramentas de CRM, gestão de dados internos e relacionados com os clientes),
- o seu código interno: por exemplo, o sítio web da empresa, projetos de desenvolvimento interno, etc.,
- e todas as dependências diretas e indiretas de software e serviços.

O RSI das vulnerabilidades é pouco conhecido até que algo de mau aconteça. Há que considerar as consequências de uma grande perda de dados ou indisponibilidade de serviços para estimar o verdadeiro custo das vulnerabilidades.

Da mesma forma, uma cultura de sigilo e ocultação para questões relacionadas com a segurança na empresa deve ser evitada a todo o custo. Em vez disso, a informação sobre o estado de vulnerabilidade deve ser partilhada e discutida para encontrar as melhores respostas por parte das pessoas certas, desde os desenvolvedores até à administração superior.

Os benefícios de prevenir ataques cibernéticos gerindo cuidadosamente as vulnerabilidades de software são bastantes:

- Evitar riscos à reputação,
- Evitar perdas por abuso (DDoS, Ransomware, Tempo para reconstruir um sistema informático alternativo após um ataque),
- Cumprir os regulamentos de proteção de dados.

A gestão de vulnerabilidades de software OSS é apenas uma parte do processo maior de cibersegurança que aborda globalmente a segurança dos sistemas e serviços na organização.

### Avaliação do progresso

Deve haver uma pessoa ou equipa dedicada a monitorizar vulnerabilidades e processos fáceis de usar em que os programadores possam confiar. A avaliação das vulnerabilidades é uma parte padrão do processo de integração contínua e as pessoas conseguem monitorizar o estado atual dos riscos num painel de instrumentos dedicado.

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] A atividade é abrangida quando todo o software e serviços internos são avaliados e monitorizados quanto a vulnerabilidades conhecidas.
- [ ] A atividade é abrangida quando uma ferramenta e um processo dedicados são implementados na cadeia de produção de software para evitar a introdução de problemas nas rotinas diárias de desenvolvimento.
- [ ] Uma pessoa ou equipa é responsável pela avaliação de exposição a CVEs/riscos de vulnerabilidade.
- [ ] Uma pessoa ou equipa é responsável pelo envio de CVE/vulnerabilidade às pessoas interessadas (SysOps, DevOps, programadores, etc.).

### Ferramentas

- Ferramentas do GitHub
   - GitHub fornece diretrizes e ferramentas para assegurar o código alojado na plataforma. Veja [GitHub docs](https://docs.github.com/pt/code-security/getting-started/securing-your-repository) para mais informações.
   - GitHub fornece [Dependabot](https://docs.github.com/pt/code-security/dependabot/dependabot-alerts/about-dependabot-alerts) para identificar vulnerabilidades nas dependências automaticamente.
- [Eclipse Steady](https://eclipse.github.io/steady/) é uma ferramenta gratuita e de código aberto que analisa projetos de Java e Python em busca de vulnerabilidades e ajuda os programadores a atenuá-las.
- [OWASP dependency-check](https://owasp.org/www-project-dependency-check/): um scanner de vulnerabilidades em código aberto.
- [OSS Review Toolkit](https://github.com/oss-review-toolkit/ort): um orquestrador de código aberto capaz de recolher conselhos de segurança para dependências usadas por serviços de dados de vulnerabilidade configurados.

### Recursos

- A [base de dados de vulnerabilidades](https://cve.mitre.org/) dos CVEs. Veja também [base de dados de segurança do NIST](https://nvd.nist.gov/) do NVD, e recursos de satélite como [CVE Details](https://www.cvedetails.com/).
- Consulte também esta nova iniciativa da Google: as [vulnerabilidades do código aberto](https://osv.dev/).
- O grupo de trabalho do OWASP publica uma lista de scanners de vulnerabilidades [no website dele](https://owasp.org/www-community/Vulnerability_Scanning_Tools), tanto do mundo comercial como de código aberto.
- J. Williams and A. Dabirsiaghi. The unfortunate reality of insecure libraries, 2012.
- [Detection, assessment and mitigation of vulnerabilities in open source dependencies](https://link.springer.com/article/10.1007/s10664-020-09830-x), Serena Elisa Ponta, Henrik Plate & Antonino Sabetta, Empirical Software Engineering volume 25, páginas 3175–3215(2020).
- [A Manually-Curated Dataset of Fixes to Vulnerabilities of open source Software](https://arxiv.org/abs/1902.02595), Serena E. Ponta, Henrik Plate, Antonino Sabetta, Michele Bezzi, Cédric Dangremont. Existe também um [conjunto de ferramentas em desenvolvimento para implementar o conjunto de dados em epígrafe](https://sap.github.io/project-kb/).

[^heartbleed]: https://pt.wikipedia.org/wiki/Heartbleed
[^equifax]: https://arstechnica.com/information-technology/2017/09/massive-equifax-breach-caused-by-failure-to-patch-two-month-old-bug/
