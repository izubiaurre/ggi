## Gerir a conformidade legal

Activity ID: [GGI-A-21](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_21.md).

### Descrição

As organizações precisam de implementar um processo de conformidade legal para garantir o seu uso e participação em projetos de código aberto.

A gestão madura e profissional do cumprimento da lei, na organização e em toda a cadeia de abastecimento, está em causa:

- Realizar uma análise exaustiva da propriedade intelectual que inclua a identificação da licença e a verificação da compatibilidade.
- Assegurar que a organização possa utilizar, integrar, modificar e redistribuir componentes de código aberto como parte dos seus produtos ou serviços em segurança.
- Proporcionar aos empregados e contratantes um processo transparente sobre como criar e contribuir ao software de código aberto.

*Análise da Composição do Software (SCA)*: Uma parte significativa das questões legais e de PI resulta do uso de componentes disponibilizados sob licenças que são incompatíveis entre eles ou incompatíveis com a forma como a organização pretende utilizar e redistribuir os componentes. A SCA é o primeiro passo para resolver essas questões, pois "é preciso conhecer o problema para resolvê-lo". O processo consiste em identificar todos os componentes envolvidos num projeto num documento de lista de material, incluindo dependências de construção e testes.

*Verificação de licenças*: Um processo de verificação de licenças utiliza um instrumento para analisar a base de códigos e identificar as licenças e os direitos de autor dentro da mesma automaticamente. Se executado regularmente e idealmente integrado em cadeias de construção e integração contínuas, isto permite detetar problemas de PI antecipadamente.

### Avaliação de oportunidades

Com a crescente utilização do OSS nos sistemas de informação de uma organização, é essencial avaliar e gerir a potencial exposição legal.

No entanto, a verificação de licenças e direitos de autor pode ser complicada e dispendiosa. Os programadores precisam de poder verificar a PI e as questões legais rapidamente. Ter uma equipa e um responsável empresarial dedicado a questões de PI e legais assegura uma gestão proativa e consistente das questões legais, ajuda a assegurar o uso de componentes e contribuições de código aberto e fornece uma visão estratégica clara.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] Existe um processo de verificação de licenças fácil de usar para projetos.
- [ ] Existe um processo de verificação de PI fácil de usar para projetos.
- [ ] Existe uma equipa ou pessoa responsável pelo cumprimento legal na organização.
- [ ] Estão previstas auditorias regulares para avaliar a conformidade legal.

Outras formas de estabelecer pontos de verificação:

- [ ] Existe um processo de verificação de licenças fácil de usar.
- [ ] Existe uma equipa jurídica/PI fácil de usar, como na atividade #13.
- [ ] Todos os projetos fornecem a informação necessária para que as pessoas utilizem e contribuam para o projeto.
- [ ] Existe um contacto na equipa para questões relacionadas com PI e licenciamento.
- [ ] Existe um responsável corporativo dedicado à PI e licenciamento.
- [ ] Existe uma equipa dedicada a questões relacionadas com PI e licenciamento.

### Ferramentas

- [ScanCode](https://scancode-toolkit.readthedocs.io)
- [Fossology](https://www.fossology.org/)
- [SW360](https://www.eclipse.org/sw360/)
- [Fossa](https://github.com/fossas/fossa-cli)
- [OSS Review Toolkit](https://oss-review-toolkit.org)

### Recomendações

- Informe as pessoas sobre os riscos associados ao licenciamento em conflito com objetivos comerciais.
- Proponha uma solução fácil para os projetos criarem a verificação de licenças no seu código.
- Comunique a sua importância e ajude projetos a adicioná-la aos sistemas de IC deles.
- Forneça um modelo ou orientações oficiais para a estrutura do projeto.
- Instaure verificações automatizadas para garantir que todos os projetos cumprem as diretrizes.
- Considere a realização de uma auditoria interna para identificar as licenças da infraestrutura da empresa.
- Forneça formação básica em PI e licenciamento para pelo menos uma pessoa por equipa.
- Forneça formação completa em PI e licenciamento ao oficial.
- Estabelecimento de um processo para levantar questões de PI e licenciamento com o responsável.

Lembre-se que o cumprimento não é apenas legal; é também uma questão de PI. Portanto, eis algumas perguntas para ajudar a compreender as consequências do cumprimento legal:

- Se distribuir um componente de código aberto e não respeitar as condições da licença, infrinjo a licença --> implicações legais.
- Se utilizar um componente de código aberto num projeto que desejo distribuir/publicar, essa licença pode obrigar à visibilidade sobre elementos de código que não quero fazer de código aberto --> Impacto de confidencialidade para a vantagem tática da minha empresa e com terceiros (implicações legais).
- É uma discussão aberta sobre a utilização de uma licença de código aberto para um projeto que pretendo publicar, concedendo direitos de propriedade intelectual relevantes --> implicações de propriedade intelectual.
- Se fizer um projeto de código aberto *antes* de qualquer processo de patentes, *provavelmente* exclui a criação de patentes relativas ao projeto --> implicações de PI.
- Se fizer um projeto open source *após* qualquer processo de patentes, *provavelmente* permite a criação de patentes (defensivas) relativas a esse projeto --> potencial de PI.
- Em projetos complexos que trazem muitos componentes com muitas dependências, a multiplicidade de licenças de código aberto pode exibir incompatibilidades entre licenças --> implicações legais (compare problema #23).

### Recursos

- Existe uma extensa lista de ferramentas na [página de grupo de conformidade OSS existente](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence-Compliance-Tools/).
- [Recommended Open Source Compliance Practices for the enterprise](https://www.ibrahimatlinux.com/wp-content/uploads/2022/01/recommended-oss-compliance-practices.pdf). Um livro de Ibrahim Haddad, da Fundação Linux, sobre práticas de conformidade de código aberto para a empresa. [OpenChain Project](https://www.openchainproject.org/)
