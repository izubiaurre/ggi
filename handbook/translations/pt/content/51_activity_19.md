## Supervisão de código aberto

ID da atividade: [GGI-A-19](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_19.md).

### Descrição

Esta atividade consiste em controlar a utilização de software de código aberto e assegurar que o software de código aberto é gerido proativamente. Isto diz respeito a várias perspetivas, quer para utilizar ferramentas OSS e soluções empresariais, quer para incluir o OSS como componentes em desenvolvimentos próprios ou modificar uma versão de um software adaptando-o às necessidades próprias, etc. Trata-se também de identificar áreas onde o software de código aberto tornou-se uma solução (por vezes oculta) e avaliar a sua adequação.

Poderá ser necessário esclarecer o seguinte:

- A funcionalidade necessária é fornecida?
- Existe uma funcionalidade adicional fornecida que não é necessária, mas aumenta a complexidade nas fases CONSTRUÇÃO e EXECUÇÃO?
- O que exige a licença, quais são as restrições legais?
- Quanto a decisão torna a sua organização independente do fornecedor?
- Existe uma opção de apoio pronta para as suas necessidades comerciais e quanto custa?
- TCO (Custo Total de Propriedade).
- A administração conhece as vantagens de código aberto, por exemplo, para além da "economia de custos de licença"? Estar confortável com o código aberto ajuda a obter o máximo benefício de trabalhar com as comunidades de projeto e fornecedores.
- Veja se faz sentido partilhar os custos de desenvolvimento a dar os próprios desenvolvimentos e todas as implicações, como o cumprimento da licença, à comunidade.
- Verifique a disponibilidade de apoio à comunidade ou suporte profissional.

### Avaliação de oportunidades

A definição de um processo de decisão especificamente dirigido ao código aberto é uma forma de maximizar benefícios.

- Evita o uso descontrolado e os custos ocultos das tecnologias OSS.
- Conduz a decisões estratégicas e organizacionais informadas e conscientes do OSS.

Custos: a atividade pode desafiar e reconsiderar o uso subideal de código aberto como ineficiente, arriscado, etc.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta atividade:

- [ ] O OSS tornou-se uma opção confortável quando escolher OSS não é visto como uma exceção ou uma escolha perigosa.
- [ ] OSS tornou-se a opção "corrente dominante".
- [ ] Os intervenientes principais estão suficientemente convencidos de que a solução de código aberto tem vantagens estratégicas nas quais vale a pena investir.
- [ ] Pode ser demonstrado que o "TCO" da solução baseada no código aberto dá à sua organização um valor mais elevado do que a alternativa.
- [ ] Há uma avaliação de como a independência do fornecedor poupa dinheiro ou potencialmente pode poupar dinheiro no futuro.
- [ ] Há uma avaliação de que a independência da solução reduz os riscos de ser demasiado cara para alterar a solução (nenhuns formatos de dados fechados possíveis).

### Ferramentas

Nesta fase não podemos pensar em nenhuma ferramenta relevante ou preocupada com essa atividade.

### Recomendações

- Gerir proativamente a utilização de código aberto requer níveis básicos de consciencialização e compreensão dos fundamentos do código aberto, porque devem ser considerados em qualquer decisão do OSS.
- Compare a funcionalidade necessária em vez de procurar uma alternativa para uma solução de código fechado conhecida.
- Assegure-se de ter apoio e desenvolvimento futuro.
- Considere os efeitos da licença da solução na sua organização.
- Convença todos os atores-chave sobre o valor das vantagens do código aberto, além da "economia de custos de licença".
- Seja honesto, não exagere o efeito da solução de código aberto.
- No processo de tomar decisões é igualmente importante avaliar soluções de código aberto diferentes de modo a evitar o desapontamento através de expectativas erradas, para esclarecer o que a organização é obrigada a fazer e todas as vantagens que a abertura das soluções traz. Isto deve ser identificado para que a organização possa avaliá-lo para o próprio contexto.

### Recursos

- [Top 5 Benefits of Open Source](https://www.openlogic.com/blog/top-5-benefits-open-source-software): Blog patrocinado, mas ainda interessante, de leitura rápida.
- [Weighing The Hidden Costs Of Open Source](https://www.itjungle.com/2021/02/15/weighing-the-hidden-costs-of-open-source/): uma revisão patrocinada pela IBM sobre os custos de suporte do OSS.
