## Envolver-se com vendedores de código aberto

Activity ID: [GGI-A-33](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_33.md).

### Descrição

Proteja contratos com fornecedores de código aberto que lhe fornecem software crítico para si. As empresas e entidades que produzem software de código aberto precisam de prosperar para fornecer manutenção e desenvolvimento de novas funcionalidades. As competências específicas destas empresas são necessárias para o projeto e a comunidade de utilizadores conta com a continuidade dos negócios e contribuições dos mesmos.

O envolvimento com vendedores de código aberto assume várias formas:

- Subscrição de planos de apoio.
- Contratação de empresas de serviços locais.
- Patrocínio de desenvolvimentos.
- Pagar uma licença comercial.

Esta atividade implica considerar projetos de código aberto como produtos completos pelo qual vale a pena pagar, como qualquer outro produto proprietário - embora normalmente muito menos dispendioso.

### Avaliação de oportunidades

O objetivo desta atividade é de assegurar o apoio profissional do software de código aberto utilizado na organização. Tem várias vantagens:

- Continuidade do serviço através de correções de bugs rápidas.
- Desempenho do serviço através de uma instalação otimizada.
- Clarificação do estado jurídico/comercial do software utilizado.
- Acesso antecipado à informação.
- Previsão de orçamento estável.

O custo é obviamente o dos planos de apoio escolhidos. Outros custos podem ser abandonar a subcontratação a granel a grandes integradores de sistemas em favor de contratos de granulação fina com PMEs especializadas.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta atividade:

- [ ] O código aberto utilizado na organização é secundado por apoio comercial.
- [ ] Planos de suporte para alguns projetos de código aberto foram contratados.
- [ ] O custo dos planos de apoio de código aberto é uma entrada legítima no orçamento de TI.

### Recomendações

- Encontre, sempre que possível, PME especializadas locais.
- Cuidado com os grandes integradores de sistemas que revendem conhecimentos especializados de terceiros (planos de apoio à revenda realmente fornecidos por PMEs especializadas de código aberto).

### Recursos

Algumas ligações que ilustram a realidade comercial do software de código aberto:

- [An investor's view of the community to business evolution of open source projects](https://a16z.com/2019/10/04/commercializing-open-source/).
- [A quick read to understand commercial open source](https://www.webiny.com/blog/what-is-commercial-open-source).
