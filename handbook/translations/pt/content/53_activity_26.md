## Contribuir a projetos de código aberto

Activity ID: [GGI-A-26](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_26.md).

### Descrição

Contribuir para projetos de código aberto que são livremente utilizados é um dos princípios-chave da boa governação. O objetivo é evitar ser um simples consumidor passivo e restituir aos projetos. Quando as pessoas acrescentam uma característica ou corrigem um bug para fins próprios, devem torná-lo suficientemente genérico para contribuir ao projeto. Os programadores devem dispor de tempo para contribuir.

Esta atividade abrange o âmbito seguinte:

- Trabalhar com projetos de código aberto a montante.
- Relatar bugs e pedidos de funcionalidades.
- Contribuir com código e correcções de bugs.
- Participar em listas de correio da comunidade.
- Partilhar experiências.

### Avaliação de oportunidades

Os benefícios principais desta atividade são:

- Aumenta o conhecimento geral e o compromisso com o código aberto na empresa, à medida que as pessoas começam a contribuir e a envolver-se em projetos de código aberto. Têm um sentimento de utilidade pública e melhoram a própria reputação pessoal.
- A empresa aumenta a sua visibilidade e reputação, à medida que as contribuições atravessam o projeto contribuído. Isto mostra que a empresa está realmente envolvida no código aberto, contribui de volta,e promove a equidade e a transparência.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] Existe um caminho claro e oficial para as pessoas dispostas a contribuir.
- [ ] Os promotores são encorajados a contribuir de volta aos projetos de código aberto que utilizam.
- [ ] Existe um processo para assegurar a conformidade legal e a segurança das contribuições dos programadores.
- KPI: Volume de contribuições externas (código, listas de correio, edições...) por indivíduo, equipa ou entidade.

### Ferramentas

Pode ser útil seguir as contribuições, tanto para acompanhar o que é contribuído como para poder comunicar sobre o esforço da empresa. Painéis de controlo e software de localização de actividades podem ser utilizados para este fim. Verifique:

- [GrimoireLab](https://chaoss.github.io/grimoirelab/) do Bitergia
- [ScanCode](https://scancode-toolkit.readthedocs.io)

### Recomendações

Encorajar as pessoas na entidade a contribuir para projetos externos, ao:

- Dar-lhes tempo para escrever correções e características genéricas e bem testadas de bugs e para os contribuir de volta à comunidade.
- Fornecer formação às pessoas sobre como contribuir de volta às comunidades de código aberto. Trata-se tanto de competências técnicas (melhorar os conhecimentos da sua equipa) como da comunidade (pertencentes às comunidades de código aberto, código de conduta, etc.).
- Fornecer formação sobre questões jurídicas, de PI, técnicas e estabelecer um contacto na empresa para ajudar com estes tópicos, caso alguem tenha dúvidas.
- Fornecer incentivos para trabalhos publicados.
- Note que as contribuições da empresa/entidade reflectirão a qualidade do código e o envolvimento dela, por isso certifique-se de que a sua equipa de desenvolvimento fornece um código que seja suficientemente bom.

### Recursos

- A iniciativa [CHAOSS](https://chaoss.community/) da Linux Foundation tem ferramentas e indicações sobre como acompanhar as contribuições em desenvolvimento.
