## Código aberto permitindo a inovação

Activity ID: [GGI-A-36](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_36.md).

### Descrição

> A inovação é a implementação prática de ideias que resultam na introdução de bens ou serviços novos ou na melhoria da oferta de bens ou serviços.
>
> &mdash; <cite>Schumpeter, Joseph A.</cite>

O código aberto pode ser um fator chave para a inovação através de diversidade, colaboração e uma partilha fluente de ideias. Pessoas de origens e domínios diferentes podem ter perspetivas diferentes e fornecer respostas novas, melhoradas ou mesmo disruptivas a problemas conhecidos. É possível ativar a inovação ao ouvir opiniões diferentes e promover ativamente a colaboração aberta em projetos e tópicos.

Da mesma forma, participar na elaboração e implementação de normas abertas é um grande promotor de boas práticas e ideias para melhorar o trabalho diário da empresa. Também permite à entidade conduzir e influenciar a inovação para onde e o que necessita e aumenta a visibilidade e reputação global dela.

Através da inovação, o código aberto possibilita não só transformar os bens ou serviços que a sua empresa comercializa, mas também criar ou modificar todo o ecossistema em que a sua empresa quer prosperar.

Como exemplo, ao lançar o Android como código aberto, a Google convida centenas de milhares de empresas a construir os próprios serviços com base nesta tecnologia de código aberto. A Google está assim a criar um ecossistema inteiro do qual todos os participantes poderiam beneficiar. É claro que muito poucas empresas são suficientemente poderosas para criar um ecossistema pela própria decisão. Mas há muitos exemplos de alianças entre empresas para criar um ecossistema desses.

### Avaliação de oportunidades

É importante avaliar a posição da sua empresa em comparação com os seus concorrentes e parceiros e clientes porque muitas vezes seria arriscado para uma empresa afastar-se demasiado das normas e tecnologias utilizadas pelos próprios clientes, parceiros e concorrentes. Inovação significa obviamente ser diferente, mas o que é diferente não deve representar um alcance demasiado grande; caso contrário, a sua empresa não beneficiaria dos desenvolvimentos de software feitos pelas outras empresas do ecossistema e do impulso comercial que o ecossistema proporciona.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta atividade:

- [ ] As tecnologias - e as comunidades de código aberto que as desenvolvem - que têm influência sobre o negócio foram identificadas.
- [ ] O progresso e as publicações destas comunidades de código aberto são monitorizados -- estou mesmo ciente da estratégia delas antes dos lançamentos serem publicados.
- [ ] Os funcionários da organização são membros de (algumas) destas comunidades de código aberto e influenciam os respectivos roteiros e escolhas técnicas, por contribuírem com linhas de códigos e participarem nos órgãos de governação destas comunidades.

### Recomendações

De todas as tecnologias que são necessárias para gerir o seu negócio, deve identificar:

- as tecnologias que podem ser as mesmas que os seus concorrentes,
- as tecnologias que devem ser específicas para a sua empresa.

Mantenha-se actualizado sobre as tecnologias emergentes. O código aberto impulsionou a inovação durante a última década e muitas ferramentas poderosas do dia-a-dia vêm de lá (pense em Docker, Kubernetes, projetos de Apache Big Data ou Linux). Não é preciso saber tudo sobre tudo, mas é preciso conhecer o estado da arte o suficiente para identificar novas tendências interessantes.

Permita, e encoraje, as pessoas a submeter ideias inovadoras, e a apresentá-las. Se possível, despende recursos nestas iniciativas e fazê-las crescer. Confie na paixão e vontade das pessoas de criar e fomentar ideias e tendências emergentes.

### Recursos

- [4 innovations we owe to open source](https://www.techrepublic.com/article/4-innovations-we-owe-to-open-source/).
- [The Innovations of Open Source](https://dirkriehle.com/publications/2019-selected/the-innovations-of-open-source/), do professor Dirk Riehle.
- [Open source technology, enabling innovation](https://www.raconteur.net/technology/cloud/open-source-technology/).
- [Can Open Source Innovation Work in the Enterprise?](https://www.threefivetwo.com/blog/can-open-source-innovation-work-in-the-enterprise).
- [Europe: Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).
- [Europe: Open source software strategy 2020-2023](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
