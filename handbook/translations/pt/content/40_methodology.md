# Metodologia

A implementação da metodologia da OSS Good Governance é, ultimamente, uma iniciativa consequente e com impacto. Envolve várias categorias de pessoas, serviços e processos de empresas, desde as práticas quotidianas à gestão de RH e dos promotores de nível executivo. Não existe realmente um mecanismo mágico para implementar uma boa governação de código aberto. Tipos de organização e culturas e situações de empresas diferentes exigirão abordagens diferentes à boa governação de código aberto. Para cada organização, haverá constrangimentos e expectativas diferentes, levando a diferentes caminhos e formas de gestão do programa.

Com isto em mente, a Good Governance Initiative fornece um plano genérico de atividades que pode ser adaptado ao próprio domínio, cultura e requisitos de uma organização. Embora o projeto alegue ser abrangente, a metodologia pode ser implementada progressivamente. É possível arrancar com o programa simplesmente selecionando os objetivos e atividades mais relevantes no contexto específico de cada um. A ideia é construir um primeiro esboço do roteiro para ajudar a criar a iniciativa local.

Para além deste quadro, também recomendamos vivamente o contacto com colegas através de uma rede estabelecida como a iniciativa europeia [OSPO Alliance](https://ospo-alliance.org), ou outras iniciativas semelhantes do grupo TODO, ou OSPO++. O que é importante é poder trocar ideias com pessoas que dirigem uma iniciativa semelhante e partilhar problemas encontradas e soluções existentes.

## Preparar o palco

Dada a ambição da metodologia da boa governação e o seu impacto potencialmente amplo, é importante comunicar com uma variedade de pessoas numa organização. Seria apropriado envolvê-las para estabelecer um conjunto inicial de expectativas e requisitos realistas para um bom início, atrair interesse e apoio. Uma boa ideia é a publicação de Quadros de pontuação de atividades personalizados na plataforma de colaboração da organização para poderem ser utilizados para comunicar com as partes interessadas. Algumas dicas:

* Identifique as principais partes interessadas, faça-as concordar sobre um conjunto de objetivos primários. Envolva-os no sucesso da iniciativa, como parte da própria agenda.
* Obtenha a adesão inicial, concorde com as etapas e o ritmo e configure verificações regulares para os informar sobre o progresso.
* Certifique-se que eles entendam os benefícios do que pode ser alcançado e do que envolve: a melhoria esperada deve ser clara e o resultado visível.
* Estabeleça um primeiro diagnóstico ou estado da arte de código aberto na organização candidata. Resultado: um documento que descreva o que este programa irá alcançar, onde está a organização e para onde pretende ir.

## Fluxo de trabalho

Como profissionais de software modernos, gostamos de métodos ágeis que definem incrementos pequenos e seguros, uma vez que é boa prática reavaliar a situação regularmente e fornecer resultados intermédios significativos mínimos.

No contexto de um programa do OSPO em funcionamento, isto é altamente relevante, uma vez que muitos aspetos secundários irão mudar ao longo do tempo, desde a estratégia e resposta da organização de código aberto até à disponibilidade e envolvimento das pessoas. Reavaliação e iteração periódica também permitem a adaptação à aceitação do programa em curso, um melhor acompanhamento das tendências e oportunidades atuais, e benefícios incrementais pequenos para as partes interessadas e para a organização na totalidade.

Idealmente, a metodologia poderia ser implementada em cinco fases, como segue:

1. **Descoberta** Entender os conceitos-chave, apropriar-se na metodologia, alinhar as expectativas das metas.
1. **Personalização** Adaptação da descrição da atividade e avaliação de oportunidades às especificidades da organização.
1. **Priorização** Identificação de objetivos e resultados-chave, tarefas e ferramentas, cronograma de agendamento e elaboração de cronograma.
1. **Ativação** Finalização do cartão de pontuação, orçamento, atribuições, tarefas de documentação sobre o gestor de problemas.
1. **Iteração** Avaliar e pontuar resultados, destacar questões, melhorar, ajustar. Itere todos os trimestres ou semestres.

Preparação para a primeira iteração do programa:

* Identifique um primeiro conjunto de tarefas a trabalhar, e atribua prioridade conforme as necessidades (lacunas até ao estado desejado) e a linha do tempo. Resultado: uma lista de tarefas a trabalhar durante a iteração.
* Defina um conjunto de requisitos e áreas de melhoria, comunique às partes interessadas e utilizadores finais, e obtenha a sua aprovação ou compromisso.
* Preencha os cartões de pontuação para acompanhar o progresso. Um modelo de cartão de pontuação pode ser descarregado do [repositório GGI](https://gitlab.ow2.org/ggi/ggi/-/tree/main/resources/).

No final de cada iteração, faça uma retrospetiva e prepare-se para a próxima iteração:

* Comunique sobre as últimas melhorias.
* Avalie onde se encontra, se as tarefas visadas tiverem sido concluídas, refine o roteiro em conformidade.
* Verifique os pontos ou questões de dor restantes, peça apoio a outros atores ou serviços, se necessário.
* Redefine as tarefas conforme o contexto atualizado.
* Define um novo subconjunto de tarefas a executar.

## Configuração manual: usar Quadros de pontuação de atividades personalizados

Um quadro de pontuação de atividades personalizado é uma forma que descreve uma atividade canónica personalizada para as especificidades de uma organização. Em conjunto, o conjunto de quadros personalizados de pontuação de atividades fornece o roteiro para a gestão de software de código aberto.

`Observe que, a partir da experiência inicial com a metodologia, leva até uma hora para adaptar uma atividade canónica a um quadro personaleizado de pontuação específico a uma organização.`

O Quadro de pontuação de atividades personalizado contém as secções seguintes:

* **Desambiguação do título** Antes de mais, leve alguns minutos a desenvolver uma compreensão do que a atividade pode ser e da relevância dela, como se pode enquadrar na sua jornada global de gestão do OSS.
* **Descrição personalizada** Adapte a atividade às especificidades da organização, a delimitar o âmbito. Defina o âmbito da atividade, o caso de uso particular que irá abordar.
* **Avaliação de oportunidades** Explique porque é relevante empreender esta atividade, que necessidades aborda. Quais são os nossos pontos de dor? Quais são as oportunidades para progredir? O que se pode ganhar?
* **Objetivos** Define alguns objetivos cruciais para a atividade. Pontos de dor a serem corrigidos, oportunidades de progresso, desejos. Identifique tarefas-chave. O que pretendemos alcançar nesta iteração.
* **Ferramentas** Tecnologias, ferramentas e produtos utilizados na atividade.
* **Notas operacionais** Indicações sobre a abordagem, o método, a estratégia para progredir nesta atividade.
* **Resultados-chave** Defina resultados a esperar mensuráveis e verificáveis. Escolha resultados que indiquem o progresso em relação aos objetivos. Indique aqui os KPIs.
* **Progresso e pontuação** Progresso é, em %, a taxa de conclusão do resultado; pontuação é a classificação de sucesso pessoal.
* **Avaliação pessoal** Para cada resultado pode acrescentar uma breve explicação e explicar a sua taxa de satisfação pessoal expressa na pontuação.
* **Linha do tempo** Indicar datas de início e fim, tarefas de faseamento, passos críticos, marcos.
* **Esforços** Avalie o tempo e os recursos materiais solicitados, internos e de terceiros. Quais são os esforços esperados? Quanto é que vai custar? Quais são os recursos necessários?
* **Consignatários** Digam quem participa. Atribua tarefas ou liderança e responsabilidades da atividade.
* **Problemas** Identifique os problemas principais, dificuldades previstas, riscos, obstáculos, incertezas, pontos de atenção, dependências críticas.
* **Estado** Escreva aqui uma avaliação sintética de como a atividade decorre: saudável? Atrasada? Etc.
* **Avaliação global do progresso** A sua própria avaliação do progresso da atividade de alto nível sintética, orientada à gestão.

## Configuração automática: usar o recurso de implantação do GGI

A partir da versão 1.1 do Manual, o GGI propõe [My GGI Board](https://gitlab.ow2.org/ggi/my-ggi-board), uma ferramenta automatizada para implantar a sua própria instância do GGI como um projeto do GitLab. O processo de instalação leva menos de 10 minutos para ser configurado, é totalmente documentado e fornece uma maneira simples e confiável de personalizar as atividades, acompanhar a sua execução à medida que você progride e comunicar os resultados às partes interessadas. Um exemplo ao vivo da implantação pode ser visto no [GitLab da iniciativa](https://gitlab.ow2.org/ggi/my-ggi-board-test), com o site gerado automaticamente disponível nas [suas páginas do GitLab](https://ggi.ow2.io/my-ggi-board-test/).

![Atividades de implantação do GGI](resources/images/ggi_deploy_activities.png)

Aqui está um fluxo de trabalho padrão para usar o recurso de implantação:

1. Bifurque a My GGI Board na sua própria instância ou projeto do GitLab e configure-a seguindo as instruções no README do projeto: <https://gitlab.ow2.org/ggi/my-ggi-board>. Isso irá:

- Criar todas as atividades como problemas no projeto.
- Criar um bom quadro para ajudá-lo a visualizar e gerir as atividades.
- Criar um site estático, servido nas páginas da sua instância de GitLab, com as informações extraídas das atividades.
- Atualizar a descrição do projeto com as ligações apropriadas ao quadro de atividades e o seu site estático.

1. Desde aí, pode começar a rever as atividades e preencher a secção do cartão de pontuação.

- A secção do cartão de pontuação é o equivalente eletrónico (e simplificado) dos cartões de pontuação em ODT mencionados acima. São usados para adaptar a atividade ao seu contexto, listando os recursos locais, riscos e oportunidades e definindo objetivos personalizados necessários para concluir a atividade.
- Se alguma atividade não se aplicar ao seu contexto, basta marcá-la como "Não Selecionada" ou fechá-la.
- Este é um processo bastante demorado, mas altamente necessário, pois irá ajudá-lo, passo a passo, a definir o seu próprio roteiro e plano.

1. Quando as atividades forem definidas, poderá começar a implementar o seu próprio OSPO. Selecione algumas atividades que considera relevantes para começar e altere o rótulo de progresso de 'Não iniciado' para 'Em andamento'. Pode usar os recursos do GitLab para ajudá-lo a organizar o trabalho (comentários, responsáveis, etc.) ou qualquer outra ferramenta. É fácil de combinar as atividades e há muitas integrações excelentes disponíveis.
1. Regularmente (semanalmente, mensalmente, dependendo do seu horário), avalie e reveja as atividades atuais e, quando forem concluídas, altere o rótulo de 'Em andamento' para 'Concluído'. Selecione alguns outros e comece novamente na etapa 3 até que todos estejam concluídos.

O site oferece uma visão geral rápida das atividades atuais e passadas e extrai a secção do cartão de pontuação de problemas para exibir apenas as informações locais relevantes. Quando ocorrem alterações nos problemas (atividades) estas são atualizadas automaticamente no site gerado. Observe que os pipelines CI para a geração automática do site são executados automaticamente todas as noites, mas você pode iniciá-los facilmente na secção CI/CD do projeto GitLab. A imagem a seguir mostra a interface do site gerada automaticamente.

![Site de implantação do GGI](resources/images/ggi_deploy_website.png)

Pode fazer perguntas ou obter suporte para o recurso de implantação na nossa página inicial do GitLab e agradecemos comentários.

> Página inicial da implantação da GGI: <https://gitlab.ow2.org/ggi/my-ggi-board>

## Desfrutar

Comunique sobre o seu sucesso e desfrute da paz de espírito de uma estratégia de código aberto de última geração!

O OSS Good Governance é um método para implementar um programa de melhoria contínua e assim nunca termina. No entanto, é importante destacar os passos intermédios e apreciar as mudanças que produz, para tornar o progresso visível e partilhar os resultados.

* Comunique com interessados e utilizadores finais para lhes dar a conhecer as vantagens e benefícios que o esforço da iniciativa traz.
* Fomente a sustentabilidade do programa. Assegure que as práticas recomendadas e lições aprendidas com o programa são sempre aplicadas e atualizadas.
* Partilhe a sua experiência com os seus pares: dê opinião ao grupo de trabalho GGI e no seio da sua comunidade OSPO de adoção, e partilhe a sua abordagem.
