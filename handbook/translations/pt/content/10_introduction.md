# Introdução

Este documento apresenta uma metodologia para implementar uma gestão profissional de software de código aberto numa organização. Aborda a necessidade de utilizar software de código aberto de forma adequada e justa, salvaguardar a empresa das ameaças técnicas, legais e de PI e maximizar as vantagens do software de código aberto. Onde quer que uma organização se encontre nestes tópicos, este documento propõe orientações e ideias para avançar e fazer da sua jornada um sucesso.

## Contexto

A maioria dos grandes utilizadores e integradores de sistemas já utilizam Software Livre e de Código Aberto (FOSS) quer nos seus sistemas de informação, quer nas suas divisões de produtos e serviços. O cumprimento do código aberto tornou-se uma preocupação crescente, e muitas empresas grandes estabeleceram agentes de cumprimento. Contudo, enquanto a limpeza da cadeia de produção de uma empresa de código aberto - a conformidade - é fundamental, os utilizadores *devem* dar de volta às comunidades e contribuir para a sustentabilidade do ecossistema de código aberto. Vemos uma governação de código aberto que abrange todo o ecossistema, envolvendo as comunidades locais, alimentando uma relação saudável com fornecedores de software de código aberto e especialistas em serviços. Isto leva a conformidade ao próximo nível e é do que se trata numa "boa" governação de código aberto.

Esta iniciativa abrange mais do que o cumprimento e a responsabilidade. Trata-se de sensibilizar as comunidades de utilizadores finais (muitas vezes sendo os próprios criadores de software) e integradores de sistemas, e desenvolver relações mutuamente benéficas no ecossistema europeu de FOSS.

A OSS Good Governance permite organizações de todos os tipos -- empresas, pequenas e grandes, câmaras municipais, universidades, associações, etc. -- de maximizar os benefícios derivados ao código aberto por ajudá-las a alinhar pessoas, processos, tecnologia e estratégia. E nesta área, a de maximizar as vantagens do open source, especialmente na Europa, todos continuam a aprender e a inovar, sem que ninguém saiba em que ponto se encontram no que diz respeito ao estado da arte neste domínio.

Esta iniciativa visa ajudar organizações a alcançar esses objetivos com:

* Um catálogo estruturado de **atividades**, um roteiro para a implementação da gestão profissional de software de código aberto.
* A **ferramenta de gestão** para definir, monitorizar, relatar e comunicar sobre o progresso.
* Um **caminho claro e prático para a melhoria**, com passos pequenos e acessíveis para mitigar riscos, educar as pessoas, adaptar processos, comunicar para dentro e para fora do domínio da organização.
* **Orientações** e uma série de **referências selecionadas** sobre o licenciamento de código aberto, práticas recomendadas, formação, e envolvimento do ecossistema para alavancar a consciencialização e a cultura de código aberto, consolidar o conhecimento interno e ampliar a liderança.

Este guia foi desenvolvido com os seguintes requisitos em mente:

* Qualquer tipo de organização é abrangido: desde PMEs a grandes empresas e organizações sem fins lucrativos, desde autoridades locais (por exemplo, câmaras municipais) a grandes instituições (por exemplo, instituições europeias ou governamentais). O quadro fornece blocos de construção para uma estratégia e dicas para a sua realização, mas *como* as atividades são executadas depende inteiramente do contexto do programa e depende do gestor do programa. Pode ser útil procurar serviços de consultoria e trocar ideias com pares.
* Nenhuma suposição é feita sobre o nível de conhecimentos técnicos na organização ou do domínio de atividade. Por exemplo, algumas organizações terão de criar um currículo de formação completo, enquanto outras poderão simplesmente propor material improvisado às equipas.

Algumas atividades não serão relevantes para todas as situações, mas o quadro completo ainda assim fornece um roteiro abrangente e prepara o caminho para estratégias à medida.

## Sobre a Good Governance Initiative

Na OW2, uma iniciativa é um esforço conjunto para responder a uma necessidade do mercado. A [Good Governance Initiative](https://www.ow2.org/view/OSS_Governance) propõe um quadro metodológico para implementar uma gestão profissional de software de código aberto em organizações.

A Good Governance Initiative baseia-se num modelo abrangente inspirado na hierarquia popular de Abraham Maslow das necessidades e motivações humanas, como ilustrado pela imagem abaixo.

![Maslow e a GGI](resources/images/ggi_maslow.png)

Através de ideias, diretrizes e atividades, a Good Governance Initiative fornece um plano para a implementação de entidades organizacionais encarregadas da gestão profissional de software de código aberto, o que também é chamado OSPO (= Open Source Program Offices/Departamentos de Programas de Código Aberto). A metodologia é também um sistema de gestão para definir prioridades, e acompanhar e partilhar o progresso.

Ao implementarem a metodologia da OSS Good Governance, as organizações irão melhorar as suas competências em várias direções, incluindo:

* **utilizar** software de código aberto de forma adequada e segura numa empresa para melhorar a reutilização e manutenção do software e a velocidade de desenvolvimento do software;
* **mitigar** os riscos legais e técnicos associados a código externo e à colaboração;
* **identificar** a formação necessária para as equipas, dos programadores aos chefes de equipa e gestores, pelo que todos partilham a mesma visão;
* **prioritizar** objetivos e atividades, para desenvolver uma estratégia eficiente de código aberto;
* **comunicar** eficazmente numa empresa e com o exterior para desfrutar ao máximo da estratégia de código aberto;
* **melhorar** a competitividade e atratividade da organização para os melhores talentos de código aberto.

## Sobre a OSPO Alliance

A **OSPO Alliance** foi lançada por uma coligação das organizações europeias sem fins lucrativos de código aberto principais, incluindo OW2, Eclipse Foundation, OpenForum Europe e a Foundation for Public Code, com a missão de aumentar a sensibilização para o código aberto na Europa e no mundo e de promover a gestão estruturada e profissional de código aberto por empresas e administrações.

Enquanto a iniciativa da Good Governance se foca no desenvolvimento de uma metodologia de gestão, a OSPO Alliance tem o objetivo mais amplo de ajudar empresas, particularmente nos setores não tecnológicos e instituições públicas, a descobrir e compreender o código aberto, a começar a beneficiar dele em todas as suas atividades e a crescer para hospedar as próprias OSPOs.

A OSPO Alliance estabeleceu o site da **OSPO Alliance** hospedado em <https://ospo-alliance.org>. A OSPO Alliance serve a comunidade com um lugar seguro para discutir e trocar informação sobre os tópicos das OSPOs e fornece um repositório para um conjunto abrangente de recursos para empresas, instituições públicas e organizações académicas e de investigação. A OSPO.Zone liga-se à OSPOs em toda a Europa e no mundo, bem como a organizações comunitárias de apoio. Encoraja práticas recomendadas e fomenta a contribuição para a sustentabilidade do ecossistema de código aberto. Consulte o sítio web [OSPO Alliance](https://ospo-alliance.org) para uma rápida visão geral das estruturas complementares das práticas recomendadas de gestão de TI.

O sítio web [OSPO Alliance](https://ospo-alliance.org) é também o local onde recolhemos comentários sobre a iniciativa e o seu conteúdo (por exemplo, atividades, corpo de conhecimentos) da comunidade em geral.

## Traduções

Há um trabalho comunitário em andamento para traduzir o Manual da GGI em vários idiomas. À medida que o progresso evolui rapidamente, recomendamos que confira o nosso site oficial para obter uma lista completa de traduções disponíveis.

> Veja <https://hosted.weblate.org/projects/ospo-zone-ggi/#languages>

O manual do GGI é traduzido usando [Weblate](https://hosted.weblate.org/), um projeto e plataforma de código aberto que oferece hospedagem gratuita para projetos de código aberto. Queremos agradecer-lhes profundamente, assim como todos os nossos colaboradores de tradução. Vocês são incríveis.

## Colaboradores

As seguintes grandes pessoas contribuíram à Good Governance Initiative:

* Frédéric Aatz (Microsoft França)
* Boris Baldassari (Castalia Solutions, Eclipse Foundation)
* Philippe Bareille (cidade de Paris)
* Gaël Blondelle (Eclipse Foundation)
* Vicky Brasseur (Wipro)
* Philippe Carré (Nokia)
* Pierre-Yves Gibello (OW2)
* Michael Jaeger (Siemens)
* Sébastien Lejeune (Thales)
* Max Mehl (Free Software Foundation Europe)
* Hervé Pacault (Orange)
* Stefano Pampaloni (RIOS)
* Christian Paterson (OpenUp)
* Simon Phipps (Meshed Insights)
* Silvério Santos (Orange Business Services)
* Cédric Thomas (OW2)
* Nicolas Toussaint (Orange Business Services)

## Licença

Esta obra é licenciada sob a licença [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0). Do sítio web da Creative Commons:

> Você tem o direito de:
>
> * Partilhar — copiar e redistribuir o material em qualquer suporte ou formato
> * Adaptar — remixar, transformar, e criar a partir do material
>
> para qualquer fim, mesmo que comercial.
>
> Você deve atribuir o devido crédito, fornecer um link para a licença, e indicar se foram feitas alterações. Você pode fazê-lo de qualquer forma razoável, mas não de uma forma que sugira que o licenciante o apoia ou aprova o seu uso. [Fonte: https://creativecommons.org/licenses/by/4.0/deed.pt]

Todos os conteúdos são direitos autorais: 2020-2022 participantes da OW2 & The Good Governance Initiative.
