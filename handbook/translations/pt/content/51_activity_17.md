## Inventário de competências e recursos de código aberto

ID da atividade: [GGI-A-17](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_17.md).

### Descrição

Em qualquer fase, de uma perspetiva de gestão, é útil ter um mapeamento, um inventário de recursos de código aberto, bens, utilização e o seu estado, bem como as necessidades potenciais e soluções disponíveis. Inclui também a avaliação do esforço e das competências necessárias para preencher a lacuna.

Esta atividade visa capturar a situação de código aberto numa organização e no mercado e avaliar a ponte entre eles.

- Inventário da utilização de OSS na cadeia de desenvolvimento de software, bem como nos produtos e componentes de software utilizados na produção.
- Identificar tecnologias de código aberto (soluções, estruturas, características inovadoras) que possam servir as suas necessidades e ajudar a melhorar o seu processo.

Não incluído

- Identificar e qualificar ecossistemas e comunidades de OSS relacionados. (Objetivo da cultura)
- Identificar as dependências das bibliotecas e componentes do OSS. (Objetivo da confiança)
- Identificar as competências técnicas (por exemplo, línguas, estruturas...) e habilidades interpessoais (por exemplo, colaboração, comunicação) necessárias. (pertence às próximas atividades: crescimento das competências OSS e competências de desenvolvimento de software de código aberto)

### Avaliação de oportunidades

Um inventário dos recursos de código aberto disponíveis que ajudará a otimizar o investimento e a dar prioridade ao desenvolvimento de competências.

Esta atividade cria as condições para melhorar a produtividade do desenvolvimento, dada a eficiência e popularidade dos componentes OSS, princípios e ferramentas de desenvolvimento, particularmente no desenvolvimento de aplicações e infraestruturas modernas.

- Isto pode exigir a simplificação do portfólio de recursos do OSS.
- Isso pode exigir o retreinamento do pessoal.
- Isto permite a identificação de necessidades e preenche o seu roteiro de TI.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] Existe uma lista funcional de recursos OSS "Usamos", "Integramos", "Produzimos", "Alojamos", e as competências relacionadas
- [ ] Estamos no caminho de melhorar a eficiência através da utilização de métodos e ferramentas de última geração.
- [ ] Identificámos recursos de OSS não catalogados até agora (podem ter sido incorporados: temos elementos para definir uma política neste domínio?)
- [ ] Solicitamos que novos projetos suportem ou reutilizem recursos de OSS existentes. (Objetivo de Cultura?)
- [ ] Temos uma perceção e compreensão razoavelmente segura do alcance da utilização do OSS na nossa organização.

### Ferramentas

Há muitas formas diferentes de estabelecer tal inventário. Uma forma seria classificar os recursos do SAA em quatro categorias:

- OSS que usamos: software que usamos na produção ou no desenvolvimento
- OSS que integramos: por exemplo, bibliotecas de OSS que integramos numa aplicação feita à medida
- OSS que produzimos: por exemplo, uma biblioteca que publicámos no GitHub ou um projeto de OSS que desenvolvemos ou para o qual contribuímos regularmente.
- OSS que hospedamos: OSS que executamos para oferecer um serviço interno, como um CRM, GitLab, Nexus, etc. Uma tabela de exemplo seria parecida com a seguinte:

| Utilizamos | Integramos | Produzimos | Hospedamos | Habilidades |
| --- | --- | --- | --- | --- |
| Firefox, <br />OpenOffice, <br />Postgresql | Biblioteca slf4j | Biblioteca YY em GH | GitLab, <br />Nexus | Java, <br />Python |

A mesma identificação deve ser aplicada às habilidades

- Habilidades e experiências disponíveis através das equipas existentes
- Habilidades e experiências que poderiam ser desenvolvidas ou adquiridas internamente (treino, coaching, experimentação)
- Habilidades e experiências que precisam ser buscadas no mercado ou por parceria/contratação

### Recomendações

- Manter tudo simples.
- É um exercício de relativamente alto nível, não um inventário detalhado para o departamento de contabilidade.
- Embora esta atividade seja um bom ponto de partida, não é necessário tê-la 100% concluída antes de lançar outras atividades.
- Trate de questões, recursos e habilidades relacionadas ao **desenvolvimento de software** na atividade #42.
- O inventário deve abranger todas as categorias de TI: sistemas operativos, middlewares, SGBD, administração de sistemas, ferramentas de desenvolvimento e teste, etc.
- Comece a identificar comunidades relacionadas: é mais fácil obter apoio e comentário do projeto quando elas já o conhecem.

### Recursos

- Um curso excelente sobre [Free (/Libre), e Open Source Software (FOSS)](https://profriehle.com/open-courses/free-and-open-source-software), pelo Professor Dirk Riehle.
