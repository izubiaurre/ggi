# Organização

## Terminologia

O projeto da metodologia de Boa Governação do OSS está estruturado em torno de quatro conceitos-chave: objetivos, atividades canónicas, quadros de pontuação de atividades personalizados e Iteração.

* **Objetivos**: um objetivo é um conjunto de atividades associadas a uma área de preocupação comum, existem cinco objetivos: objetivo de utilização, objetivo de confiança, objetivo de cultura, objetivo de envolvimento e objetivo de estratégia. Os objetivos podem ser alcançados de forma independente, em paralelo, e iterativamente refinados por atividades.
* **Atividades canónicas**: num objetivo, uma atividade aborda uma única preocupação ou tópico de desenvolvimento - tal como a gestão da conformidade legal - que pode ser utilizada como um passo incremental em direção aos objetivos do programa. O conjunto completo de atividades, tal como definido pela GGI, chamam-se atividades canónicas.
* **Quadro de pontuação de atividades personalizado (CAS: Customised Activity Scorecard)**: para implementar a GGI numa determinada organização, as atividades canónicas devem ser adaptadas às especificidades do contexto, construindo assim um conjunto de quadros personalizados de pontuação de atividades. O quadro personalizado de pontuação de atividades descreve como a atividade será implementada no contexto da organização e como o progresso será monitorado.
* **Iteração**: o OSS Good Governance é um sistema de gestão e assim requer avaliações e revisões periódicas. Pense no sistema de contabilidade de uma organização, é um processo contínuo com pelo menos um ponto de controlo anual, o balanço; da mesma forma, o processo de OSS Good Governance requer pelo menos uma revisão anual, mas as revisões podem ser parciais ou mais frequentes, dependendo das atividades.

## Objetivos

As atividades canónicas definidas pela IGP estão organizadas em objetivos. Cada objetivo aborda uma área específica de progresso no âmbito do processo. Da utilização à estratégia, os objetivos cobrem questões relacionadas com todos os intervenientes, desde equipas de desenvolvimento até à administração superior.

* Objetivo **Utilização**: este objetivo cobre os passos básicos na utilização de software de código aberto. As atividades relacionadas ao objetivo de utilização cobrem os primeiros passos através de um programa de código aberto, a identificar a eficiência da utilização do código aberto e o que dá à organização. Inclui formação e gestão do saber, a produzir inventários de software de código aberto existente já utilizado internamente e apresenta conceitos de código aberto que podem ser utilizados ao longo de todo o processo.
* Objetivo **Confiança**: este objetivo trata-se da utilização segura de código aberto. O objetivo da confiança trata da conformidade legal, dependência e gestão de vulnerabilidades e visa geralmente criar confiança na forma como a organização utiliza e gere código aberto.
* Objetivo **Cultura**: o objetivo de cultura inclui atividades que visam fazer as equipas sentirem-se à vontade com código aberto, participar individualmente em atividades de colaboração, compreender e implementar as práticas recomendadas de código aberto. Este objetivo fomenta um sentimento de pertença à comunidade de código aberto entre os indivíduos.
* Objetivo **Envolvimento**: este objetivo é de se envolver com o ecossistema de código aberto a nível corporativo. Os recursos humanos e financeiros estão orçamentados para contribuir de volta para projetos de código aberto. Aqui, a organização afirma que é um cidadão responsável de código aberto e reconhece a sua responsabilidade de assegurar a sustentabilidade do ecossistema de código aberto.
* Objetivo **Estratégia**: este objetivo é o de fazer o código aberto visível e aceitável nos níveis mais altos da gestão empresarial. Trata-se de reconhecer que o código aberto é um facilitador estratégico da soberania digital, inovação de processos e, em geral, uma fonte de atratividade e boa vontade.

## Atividades canónicas

As atividades canónicas estão no centro do projeto GGI. Na sua versão inicial, a Metodologia GGI oferece cinco atividades canónicas por objetivo, 25 no total. As atividades canónicas são descritas a usar as seguintes secções predefinidas:

* *Descrição*: um resumo do tópico que a atividade aborda e os passos para a conclusão.
* *Avaliação de oportunidades*: descreve porque e quando é relevante realizar esta atividade.
* *Avaliação do progresso*: descreve como medir o progresso da atividade e avaliar o seu sucesso.
* *Ferramentas*: uma lista de tecnologias ou ferramentas que podem ajudar a realizar esta atividade.
* *Recomendações*: dicas e práticas recomendadas recolhidas dos participantes do GGI.
* *Recursos*: ligações e referências para ler mais sobre o tema abrangido pela atividade.

### Descrição

Esta secção fornece uma descrição de alto nível da atividade, um resumo do tópico para definir o objetivo da atividade no contexto da abordagem de código aberto num objetivo.

### Avaliação de oportunidades

Para ajudar a estruturar uma abordagem iterativa, cada atividade tem uma secção de "avaliação de oportunidades", com uma ou mais questões anexas. A avaliação de oportunidades concentra-se na razão pela qual é relevante realizar esta atividade, que necessidades visa colmatar. A avaliação da oportunidade ajudará a definir quais são os esforços esperados, os recursos necessários e ajudará a avaliar os custos e o RSI esperado.

### Avaliação do progresso

Este passo concentra-se na definição de objetivos, KPIs e no fornecer *pontos de verificação* que ajudam a avaliar o progresso na atividade. Os pontos de verificação são sugeridos, podem ajudar a definir um roteiro para o processo de Good Governance, as prioridades e como o progresso será medido.

### Ferramentas

Aqui estão as ferramentas listadas que podem ajudar na entrega da atividade ou instrumento de uma etapa específica das atividades. As ferramentas não são uma recomendação obrigatória, nem fingem ser exaustivas, mas são sugestões ou categorias a serem elaboradas com base no contexto existente.

### Recomendações

Esta secção é regularmente atualizada com opiniões dos utilizadores e todo o tipo de recomendações que podem ajudar a gerir a atividade.

### Recursos

Os recursos são propostos para alimentar a abordagem com estudos de fundo, documentos de referência, eventos ou conteúdo on-line para enriquecer e desenvolver a abordagem relacionada sobre a atividade. Os recursos não são exaustivos, são pontos de partida ou sugestões para expandir a semântica da atividade conforme o próprio contexto.

## Quadros de pontuação de atividades personalizados

Os quadros de pontuação de atividades personalizados (CAS: Customised Activity Scorecards) são ligeiramente mais detalhados do que as atividades canónicas. Um CAS inclui detalhes específicos para a organização que implementa a GGI. A utilização do CAS é descrita na secção metodologia.
