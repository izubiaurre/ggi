## Consciência da administração superior

Activity ID: [GGI-A-34](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_34.md).

### Descrição

A iniciativa de código aberto da organização só produzirá benefícios estratégicos se for aplicada ao nível mais elevado, integrando o ADN de código aberto na estratégia da empresa e no trabalho interno. Tal compromisso não pode acontecer se os executivos ao nível mais elevado e a administração superior não fizerem parte dela. A formação e a mentalidade de código aberto devem também ser aplicadas àqueles que moldam as políticas, decisões e estratégia global, tanto dentro como fora da empresa.

Este compromisso assegura que melhorias práticas, mudanças de mentalidade e novas iniciativas sejam cumpridas com um apoio consistente da hierarquia, benevolente e sustentável, introduzindo uma participação mais fervorosa dos trabalhadores. Molda a forma como os actores externos vêem a organização, introduzindo benefícios em termos de reputação e ecossistema. É também um meio de estabelecer a iniciativa e seus benefícios a médio e longo prazo.

### Avaliação de oportunidades

Esta atividade torna-se essencial se/quando:

- A organização definiu objetivos globais relevantes para a gestão de código aberto, mas luta para os atingir. É improvável que a iniciativa possa conseguir alguma coisa sem um bom conhecimento e um compromisso claro por parte dos executivos de nível elevado.
- A iniciativa já começou e está a progredir, mas os níveis superiores da hierarquia não lhe dão o seguimento adequado.

Esperemos que venha a ser evidente que tudo menos o uso ad hoc do código aberto requer uma abordagem consistente e bem pensada, devido à variedade de equipas e à mudança cultural que pode trazer.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] Existe um departamento/oficial de governação mandatado para definir uma estratégia uniforme de código aberto para toda a empresa e assegura que o âmbito é claro.
- [ ] Existe um compromisso claro e obrigatório da hierarquia para com a estratégia do OSS.
- [ ] Há uma comunicação transparente por parte da hierarquia sobre o compromisso dela com o programa.
- [ ] A hierarquia está disponível para discutir o software de código aberto. Pode ser solicitada e desafiada nas promessas que fez.
- [ ] Existe um orçamento e financiamento apropriados para a iniciativa.

### Recomendações

Exemplos de ações associadas a esta actividade incluem:

- Realizar formação para desmistificar a gestão do OSS para a administração superior.
- Obter apoio explícito e prático para a utilização e estratégia do OSS.
- Mencionar explicitamente e endossar o programa OSS em comunicações internas.
- Mencionar explicitamente e endossar o programa OSS em comunicações públicas.

Open source is a *strategic enabler* that embarks *enterprise culture*. What does this mean?

- O código aberto pode ser alavancado como um mecanismo para incomodar os fornecedores e reduzir os custos de aquisição de software.
   - Should open source come under the purview of *Software Asset Managers* or *purchasing departments*?
- Open source licences enshrine the freedoms that deliver the benefits of open source, but they also carry *obligations*. If not met appropriately, responsibilities can create legal, commercial and image risks to an organisation.
   - As condições de licenças conferirão visibilidade a áreas de código que devem permanecer confidenciais?
   - Terá impacto na carteira de patentes da minha organização?
   - Como devem as equipas de projetos ser formadas e apoiadas neste assunto?
- Contribuir de volta aos projetos externos de código aberto é onde reside o maior valor do código aberto.
   - Como deve a minha empresa encorajar (e acompanhar) isto?
   - Como devem os programadores utilizar GitHub, GitLab, Slack, Discord, Telegram ou qualquer outra ferramenta que os projetos de código aberto habitualmente utilizam?
   - Pode o código aberto ter impacto nas políticas de RH da empresa?
- Claro que não se trata apenas de contribuir de volta, e então os meus próprios projetos de código aberto?
   - Estou pronto para fazer inovação *aberta*?
   - Como é que os meus projetos irão gerir as contribuições *recebidas*?
   - Devo gastar o esforço de alimentar uma comunidade para um determinado projeto?
   - Como devo liderar a comunidade, que papel devem ter os membros da comunidade?
   - Estou pronto a ceder as decisões do roteiro a uma comunidade?
   - Pode o código aberto ser uma ferramenta valiosa para reduzir a siloização entre as equipas da empresa?
   - Preciso de tratar da transferência de código aberto de uma entidade da empresa para outra?
