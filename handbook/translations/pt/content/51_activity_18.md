## Crescimento de competências de código aberto

ID da atividade: [GGI-A-18](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_18.md).

### Descrição

Esta atividade tem a ver com o planeamento e a iniciação de capacidades técnicas e a experiência inicial com o OSS, após a realização de um inventário (#17). É também a oportunidade de começar a estabelecer um roteiro de desenvolvimento de habilidades básicas e leves.

- Identifique quais são as habilidades e o treino necessários.
- Estabeleça um projeto-piloto para iniciar a abordagem, aprenda fazendo, estabeleça um primeiro marco de realização.
- Aproveite das lições aprendidas e construa um corpo de conhecimento.
- Comece a identificar e documentar os próximos passos para uma adoção mais ampla.
- Elabore uma estratégia durante os próximos meses ou um ano para envolver a gestão e o apoio financeiro.

O âmbito da atividade:

- Linux, Apache, Debian, habilidades de administração.
- Bases de dados de código aberto MariaDB, MySQL, PostgreSQL, etc.
- Virtualização de código aberto e tecnologias de nuvem.
- Stack LAMP e as suas alternativas.

### Avaliação de oportunidades

Como qualquer tecnologia da informática, e provavelmente ainda mais, o código aberto traz inovação. O código aberto cresce rapidamente e muda rapidamente. Exige que as organizações se mantenham atualizadas.

Esta atividade ajuda a identificar áreas onde a formação pode ajudar as pessoas a tornarem-se mais eficientes e a sentirem-se mais seguras ao utilizar código aberto. Ajuda os funcionários a tomar decisões de desenvolvimento. Semear competências básicas de código aberto permite avaliar a oportunidade de:

- Ampliar as soluções informáticas com as tecnologias de mercado existentes, desenvolvidas pelo ecossistema.
- Desenvolver novas formas de colaboração dentro e fora da organização.
- Adquirir competências em tecnologias novas e inovadoras.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta atividade:

- [ ] Uma matriz de habilidades é desenvolvida.
- [ ] O âmbito das tecnologias OSS utilizadas é definido proativamente, isto é, evitando o uso descontrolado das tecnologias OSS.
- [ ] Um nível satisfatório de perícia é adquirido para essas tecnologias.
- [ ] As equipas receberam uma formação "básicos de código aberto" para começar.

### Ferramentas

Uma ferramenta chave aqui é chamada matriz (ou mapeamento) de atividade (ou competência).

Esta atividade pode ser realizada por:

- utilizar tutoriais online (há muitos gratuitos na Internet),
- participar em conferências de desenvolvedores,
- receber formação de vendedores, etc.

### Recomendações

- A utilização e desenvolvimento de componentes de código aberto de forma segura e eficiente requer uma mentalidade aberta e colaborativa que precisa de ser reconhecida e propagada tanto a partir do topo (gestão) como da base (desenvolvedores).
- Assegure que a abordagem é ativamente apoiada e promovida pela administração. Nada acontecerá se não houver nenhum compromisso por parte da hierarquia.
- Envolva as pessoas (desenvolvedores, intervenientes) no processo: organize mesas redondas e ouça ideias.
- Dê tempo e recursos para que as pessoas descubram, tentem e brincam com estes novos conceitos. Se possível, faça-o divertido - a gamificação e as recompensas são incentivos bons.

Um projeto-piloto com as etapas seguintes poderia servir de catalisador:

- Identifique a tecnologia ou estrutura com que começar.
- Encontre formação online, tutoriais e código de amostra para experimentar.
- Construa um protótipo da solução final.
- Identifique peritos para desafiar e orientar na implementação.

### Recursos

- [What is a Competency Matrix](https://blog.kenjo.io/what-is-a-competency-matrix): uma leitura introdutória rápida.
- [How to Make a Skills Matrix for your Team](http://www.managersresourcehandbook.com/download/Skills-Matrix-Template.pdf): um modelo com comentários.
- [MOOC on Free (libre) culture](https://librecours.net/parcours/upload-lc000/) (apenas em francês): este é um curso de 6 partes sobre a cultura livre, introdução aos direitos de autor, propriedade intelectual, licenciamento de código aberto
