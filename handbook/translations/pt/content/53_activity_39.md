## Primeiro a montante

Activity ID: [GGI-A-39](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_39.md).

### Descrição

Esta atividade trata-se de desenvolver a consciencialização em relação aos benefícios de contribuir de volta e aplicar o princípio de montante primeiro.

Com a primeira abordagem a montante, todo o desenvolvimento num projeto de código aberto deve ser feito com o nível de qualidade e abertura necessários para serem submetidos aos desenvolvedores principais de um projeto e publicados por eles.

### Avaliação de oportunidades

Escrever código com uma mentalidade de montante em primeiro:

- código de melhor qualidade,
- código que está pronto para ser submetido a montante,
- código mesclado no software principal,
- código que será compatível com a versão futura,
- reconhecimento pela comunidade do projeto e uma cooperação melhor e mais rentável.

> Montante primeiro é mais que apenas "ser gentil". Significa ter algo a dizer no projeto. Significa previsibilidade. Significa que está no controlo. Significa que age em vez de reagir. Significa que compreende o código aberto. ([Maximilian Michels](https://maximilianmichels.com/2021/upstream-first/))

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta atividade: o montante primeiro está implementado?

- [ ] Aumento significativo na quantidade de solicitações de pull/merge submetidas a projetos de terceiros.
- [ ] Uma lista de projetos de terceiros foi elaborada para os quais primeiro a montante devem ser aplicados.

### Recomendações

- Identifique programadores com mais experiência em interagir com programadores a montante.
- Facilite a interação entre programadores e programadores principais (eventos, hackathons, etc.)

### Recursos

- A explicação clara do princípio montante primeiro e porque se encaixa no objetivo da cultura: <https://maximilianmichels.com/2021/upstream-first/>.

> Montante primeiro significa que sempre que resolver um problema na sua cópia do código a montante do qual outros podem beneficiar, contribui com essas alterações de volta a montante, ou seja, envia um patch ou abre um pull request para o repositório a montante.

- [What is Upstream and Downstream in Software Development?](https://reflectoring.io/upstream-downstream/) Uma explicação clara.
- Explicado dos documentos de desenho do Chromium OS: [Upstream First](https://www.chromium.org/chromium-os/chromiumos-design-docs/upstream-first).
- Red Hat sobre a montante e as vantagens de [upstream first](https://www.redhat.com/en/blog/what-open-source-upstream).
