## French translation of GGI Handbook

To generate the french paperback version, from handbook/translations/fr directory:

```
bash ../../scripts/paperback_convert_handbook.sh -l fr
```

The generated PDF is in paperback_ggi_handbook.pdf .

