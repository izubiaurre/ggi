## Gérer les vulnérabilités des logiciels

Activity ID: [GGI-A-22](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_22.md).

### Description

Le code d’un développeur est autant sécurisé que la partie la moins sécurisée de son code. Des cas récents (par exemple, heartbleed[^heartbleed], equifax[^equifax]) ont démontré l’importance de vérifier les vulnérabilités dans les parties du code qui ne sont pas directement développées en interne. Les conséquences des expositions vont des fuites de données (avec un impact considérable sur la réputation) aux attaques par des logiciels malveillants (ransomware) et à l’indisponibilité de services menaçant l’activité.

Les logiciels libres sont connus pour leur meilleure gestion des vulnérabilités que les logiciels propriétaires, principalement pour les raisons suivantes :

- Davantage de personnes cherchent à trouver et à corriger les problèmes sur le code et les processus ouverts ;
- Les projets open source corrigent les vulnérabilités et publient des correctifs et de nouvelles versions beaucoup plus rapidement.

Par exemple, une [étude de WhiteSource](https://resources.whitesourcesoftware.com/blog-whitesource/3-reasons-why-open-source-is-safer-than-commercial-software) sur les logiciels propriétaires a montré que 95 % des vulnérabilités trouvées dans leurs composants open source avaient déjà fait l’objet d’un correctif au moment de l’analyse. L’enjeu est donc de **mieux gérer les vulnérabilités à la fois dans la base de code et dans ses dépendances**, qu’elles soient fermées ou open source.

Afin d’atténuer ces risques, il faut mettre en place un programme d’évaluation de ses actifs logiciels et un processus de vérification des vulnérabilités exécuté régulièrement. Mettez en place des outils qui alertent les équipes concernées, gèrent les vulnérabilités connues et préviennent les menaces provenant des dépendances logicielles.

### Évaluation de l’opportunité

Toute entreprise qui utilise des logiciels doit surveiller ses vulnérabilités dans :

- son infrastructure (par exemple, l’infrastructure du Cloud, l’infrastructure réseau, les magasins de données) ;
- ses applications métier (RH, outils CRM, gestion des données internes et relatives aux clients) ;
- son code interne : par exemple, le site web de l’entreprise, les projets de développement internes, etc.,
- et toutes les dépendances directes et indirectes des logiciels et services.

Le retour sur investissement des vulnérabilités est peu connu jusqu’à ce que quelque chose de grave se produise. Il faut envisager les conséquences d’une violation majeure des données ou de l’indisponibilité de services pour estimer le coût réel des vulnérabilités.

De même, il faut à tout prix éviter une culture du secret et de la dissimulation des problèmes de sécurité au sein de l’entreprise. Au contraire, les informations sur l’état de la vulnérabilité doivent être partagées et discutées afin de trouver les meilleures réponses auprès des bonnes personnes, allant des développeurs aux cadres dirigeants.

Les avantages de la prévention des cyberattaques par une gestion attentive des vulnérabilités logicielles sont multiples :

- Éviter les risques pour la réputation ;
- Éviter les pertes d’exploitation (DDoS, Ransomware, temps de reconstruction d’un système informatique alternatif après une attaque) ;
- Respecter les règles de protection des données.

La gestion des vulnérabilités des logiciels open source n’est qu’une partie du processus plus large de cybersécurité qui traite globalement de la sécurité des systèmes et des services de l’organisation.

### Suivi de l'avancement

Il doit y avoir une personne ou une équipe dédiée à la surveillance des vulnérabilités et des processus, facile à joindre, sur laquelle les développeurs peuvent s’appuyer. L’évaluation des vulnérabilités est une partie standard du processus d’intégration continue, et les personnes sont en mesure de surveiller l’état actuel des risques dans un tableau de bord dédié.

Les **points de contrôle** suivants dénotent une progression de cette Activité :

- [ ] Tous les logiciels et services internes sont évalués et surveillés pour détecter les vulnérabilités connues ;
- [ ] Un outil et un processus dédiés sont mis en œuvre dans la chaîne de production des logiciels pour empêcher l’introduction de brèches de sécurité dans les activités quotidiennes de développement ;
- [ ] Une personne ou une équipe est chargée d’évaluer le risque lié aux CVE/vulnérabilités par rapport à l’exposition aux risques ;
- [ ] Une personne ou une équipe est responsable de l’envoi des CVE/vulnérabilités aux personnes concernées (SysOps, DevOps, développeurs, etc.).

### Outils

- Outils GitHub
   - GitHub fournit des directives et des outils pour sécuriser le code hébergé sur la plateforme. Voir la [documentation de GitHub](https://docs.github.com/en/github/administering-a-repository/about-securing-your-repository) pour plus d’informations ;
   - GitHub fournit [Dependabot](https://docs.github.com/en/github/managing-security-vulnerabilities/about-alerts-for-vulnerable-dependencies) pour identifier automatiquement les vulnérabilités dans les dépendances ;
- [Eclipse Steady](https://eclipse.github.io/steady/) est un outil gratuit et open source qui analyse les vulnérabilités des projets Java et Python et aide les développeurs à les atténuer ;
- [OWASP dependency-check](https://owasp.org/www-project-dependency-check/) : un scanner de vulnérabilités open source ;
- [OSS Review Toolkit](https://github.com/oss-review-toolkit/ort) : un orchestrateur open source capable de collecter des avis de sécurité pour un projet et ses dépendances, à partir de différentes sources de vulnérabilités.

### Ressources

- La [base de données des vulnérabilités CVE du MITRE](https://cve.mitre.org/). Voir aussi la [base de données de sécurité des NVD du NIST](https://nvd.nist.gov/), et les ressources satellites comme [CVE Details](https://www.cvedetails.com/).
- Consultez également cette nouvelle initiative de Google : [les Vulnérabilités open source](https://osv.dev/).
- Le groupe de travail OWASP publie sur son site web une [liste de scanners de vulnérabilités](https://owasp.org/www-community/Vulnerability_Scanning_Tools), aussi bien du monde commercial que du monde open source.
- J. Williams et A. Dabirsiaghi. La malheureuse réalité des bibliothèques non sécurisées, 2012 (« The unfortunate reality of insecure libraries »).
- [Detection, assessment and mitigation of vulnerabilities in open source dependencies](https://link.springer.com/article/10.1007/s10664-020-09830-x), Serena Elisa Ponta, Henrik Plate & Antonino Sabetta, Empirical Software Engineering volume 25, pages 3175-3215 (2020).
- [A Manually-Curated Dataset of Fixes to Vulnerabilities of open source Software](https://arxiv.org/abs/1902.02595), Serena E. Ponta, Henrik Plate, Antonino Sabetta, Michele Bezzi, Cédric Dangremont. Il existe également une [boîte à outils en cours de développement pour mettre en œuvre l’ensemble de données susmentionné](https://sap.github.io/project-kb/).

[^heartbleed]: https://fr.wikipedia.org/wiki/Heartbleed
[^equifax] : https ://arstechnica.com/information-technology/2017/09/massive-equifax-breach-caused-by-failure-to-patch-two-month-old-bug/
