## Contribuer aux projets open source

Activity ID: [GGI-A-26](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_26.md).

### Description

Contribuer à des projets open source utilisés librement est un principe clé de la bonne gouvernance. Il s'agit d'éviter d'être un simple consommateur passif et de contribuer en retour aux projets. Lorsque les gens ajoutent une fonctionnalité ou corrigent un bug pour leur propre usage, ils doivent faire en sorte que cela soit suffisamment générique pour contribuer au projet. Les développeurs doivent avoir le temps de contribuer.

Cette activité couvre les domaines suivants :

- Travailler avec les projets open source "upstream".
- Signaler les bugs et les demandes de fonctionnalités.
- Contribuer au code et à la correction des bugs.
- Participer aux listes de diffusion de la communauté.
- Partage d'expérience.

### Évaluation de l’opportunité

Les principaux avantages de cette activité sont :

- augmenter la connaissance générale et l'engagement envers l'open source au sein de l'organisation : à mesure que les gens commencent à contribuer et à s'impliquer dans des projets open source, ils ont un sentiment d'utilité publique et améliorent leur réputation personnelle.
- développer la visibilité et la réputation de l'organisation au fur et à mesure que les contributions font leur chemin dans le projet. Cela montre que l'organisation est réellement impliquée dans l'open source, contribue en retour et promeut l'équité et la transparence.

### Suivi de l'avancement

Les **points de contrôle** suivants dénotent une progression de cette Activité :

- [ ] Il existe une voie claire et officielle pour les personnes souhaitant contribuer.
- [ ] Les développeurs sont encouragés à contribuer aux projets open source qu'ils utilisent.
- [ ] Un processus est en place pour assurer la conformité juridique et la sécurité des contributions des développeurs.
- KPI : Volume des contributions externes (code, listes de diffusion, problèmes…) par individu, équipe ou entité.

### Outils

Il peut être utile de suivre les contributions, à la fois pour garder une trace de ce qui est contribué et pour pouvoir communiquer sur l’effort de l’entreprise. Des tableaux de bord et des logiciels de suivi d’activité peuvent être utilisés à cette fin. Notamment :

- [GrimoireLab](https://chaoss.github.io/grimoirelab/) de Bitergia
- [ScanCode](https://scancode-toolkit.readthedocs.io)

### Recommandations

Encourager les personnes au sein de l’entité à contribuer à des projets externes, en :

- Leur donnant le temps d’écrire des corrections de bugs et fonctionnalités génériques bien testées, et de les reverser à la communauté.
- Fournissant une formation aux personnes sur la contribution aux communautés open source. Cela concerne à la fois les compétences techniques (améliorer les connaissances de votre équipe) et la communauté (appartenance aux communautés open source, code de conduite, etc.).
- Offrir des formations sur les questions juridiques, de propriété intellectuelle et techniques, et mettre en place un contact en interne pour aider sur ces sujets si les gens ont des doutes.
- Prévoir des incitations pour les travaux publiés.
- Notez que les contributions de l’entreprise/entité reflèteront la qualité de son code et son implication, donc assurez vous que votre équipe de développement fournit un code suffisamment bon.

### Ressources

- L’initiative [CHAOSS](https://chaoss.community/) de la Fondation Linux propose des outils et des conseils sur la manière de suivre les contributions dans le développement.
