# Conclusion

Comme nous l’avons déjà mentionné, la bonne gouvernance open source n’est pas une fin en soi mais bien un cheminement. Nous devons nous soucier de nos biens communs, des communautés et de l’écosystème qui les font prospérer, car notre réussite commune, et donc individuelle, en dépend.

En tant que praticiens du domaine des logiciels et passionnés d’open source, **nous** nous engageons à continuer d’améliorer ce mémento de bonne gouvernance et à travailler à sa diffusion et son rayonnement. Nous croyons fermement que les organisations, les individus et les communautés doivent travailler de concert pour construire un meilleur patrimoine de biens commun, accessible et bénéfique pour tous.

**Vous** êtes les bienvenus pour rejoindre l’OSPO Alliance, contribuer à notre travail, faire passer le mot et être l’ambassadeur d’une meilleure connaissance et gouvernance de l’open source au sein de votre propre écosystème. Il existe une grande variété de ressources disponibles, depuis les articles de blog et les articles de recherche jusqu’aux conférences et aux cours de formation en ligne. Nous fournissons également un ensemble de documents utiles sur [notre site web](https://ospo-alliance.org), et nous sommes ravis de pouvoir vous aider autant que possible.

**Définissons et construisons ensemble l'avenir de ce mémento de bonne gouvernance !**

## Contact

Le meilleur moyen pour entrer en contact avec l'OSPO Alliance est de poster un message sur notre liste de diffusion publique à l'adresse <https://accounts.eclipse.org/mailing-list/ospo.zone>. Vous pouvez également venir discuter avec nous lors des événements open source habituels, participer à nos webinaires mensuels OSPO OnRamp, ou encore prendre contact avec l'un des membres - ils vous redirigeront volontiers vers la bonne personne.

## Annexe : Modèle de tableau de bord personnalisé d'activité

La dernière version du modèle de tableau de bord personnalisé des activités est disponible dans la section « `ressources` » du [GitLab de l'initiative de bonne gouvernance](https://gitlab.ow2.org/ggi/ggi).
