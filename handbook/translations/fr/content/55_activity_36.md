## L’open source au service de l’innovation

Activity ID: [GGI-A-36](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_36.md).

### Description

> L'innovation est la mise en œuvre pratique d'idées qui aboutissent à l'introduction de nouveaux biens ou services ou à l'amélioration de l'offre de biens ou de services.
>
> &mdash; <cite>Schumpeter, Joseph A.</cite>

L'open source peut être un facteur clé d'innovation grâce à la diversité, à la collaboration et à un échange fluide d'idées. Des personnes issues d'horizons et de domaines différents peuvent avoir des points de vue divers et apporter des réponses nouvelles, améliorées, voire révolutionnaires aux problèmes connus. On peut favoriser l'innovation en écoutant les différents points de vue et en encourageant activement la collaboration ouverte sur des projets et des sujets.

De même, participer à l'élaboration et à la mise en œuvre de normes ouvertes est une excellente façon de promouvoir de bonnes pratiques et idées pour améliorer le travail quotidien de l'entreprise. Cela permet également à l'entité de piloter et d'influencer l'innovation là où elle en a besoin et d'améliorer globalement sa visibilité et sa réputation.

Grâce à l'innovation, l'open source permet non seulement de transformer les biens ou les services que votre entreprise commercialise, mais aussi de créer ou de modifier l'ensemble de l'écosystème dans lequel elle souhaite prospérer.

Par exemple, en diffusant Android en open source, Google invite des centaines de milliers d'entreprises à créer leurs propres services sur la base de cette technologie open source. Google crée ainsi un écosystème dont tous les participants peuvent bénéficier. Bien sûr, très peu d'entreprises sont assez puissantes pour créer un écosystème par leur propre décision : mais il existe de nombreux exemples d'alliances entre entreprises pour créer un tel écosystème.

### Évaluation de l’opportunité

Il est important d'évaluer la position de votre entreprise par rapport aux usages en vigueur chez les concurrents, partenaires et clients, pour ne pas trop l'éloigner des standards et technologies qu'ils utilisent. Innover, c'est évidemment se différencier, mais ce qui diffère ne doit pas représenter un périmètre trop large, sinon votre entreprise ne bénéficierait pas des développements logiciels réalisés par les autres acteurs de l'écosystème ni de l'élan commercial que celui-ci procure.

### Suivi de l'avancement

Les **points de contrôle** suivants dénotent une progression de cette activité :

- [ ] Les technologies - et les communautés open source qui les développent - ayant un impact sur l'entreprise ont été identifiées.
- [ ] Les progrès et les publications de ces communautés open source sont suivis - vous êtes même au courant de leur stratégie avant que les versions ne soient rendues publiques.
- [ ] Les employés de l'organisation sont membres de (certaines de) ces communautés open source et influencent leurs feuilles de route et leurs choix techniques en contribuant aux lignes de codes et en participant aux organes de gouvernance de ces communautés.

### Recommandations

Parmi toutes les technologies nécessaires au fonctionnement de votre entreprise, vous devez identifier :

- les technologies qui pourraient être les mêmes que celles de vos concurrents,
- les technologies qui doivent être spécifiques à votre entreprise.

Restez au courant des technologies émergentes. L'open source est le moteur de l'innovation depuis une dizaine d'années, et de nombreux outils puissants d'usage quotidien en sont issus (pensez à Docker, Kubernetes, aux projets Big Data d'Apache ou à Linux). Inutile de tout savoir sur tout, mais il faut connaître suffisamment l'état de l'art pour identifier les nouvelles tendances intéressantes.

Autorisez et encouragez les personnes à soumettre des idées innovantes, et à les mettre en avant. Si possible, consacrez des ressources à ces initiatives et faites-les croître. Comptez sur la passion et la volonté de chacun pour créer et favoriser les idées et les tendances émergentes.

### Ressources

- [4 innovations que nous devons à l'Open Source](https://www.techrepublic.com/article/4-innovations-we-owe-to-open-source/).
- [Les innovations de l'Open Source](https://dirkriehle.com/publications/2019-selected/the-innovations-of-open-source/), par le Professeur Dirk Riehle.
- [La technologie Open Source, source d'innovation](https://www.raconteur.net/technology/cloud/open-source-technology/).
- [L'innovation Open Source peut-elle fonctionner dans l'entreprise ?](https://www.threefivetwo.com/blog/can-open-source-innovation-work-in-the-enterprise).
- [Europe : stratégie en matière de logiciels Open Source](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).
- [Europe : stratégie 2020-2023 pour les logiciels Open Source](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
