## Montée en compétences open source

Identifiant de l'activité : [GGI-A-18](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_18.md).

### Description

Cette activité vise à planifier et initier les capacités techniques et les premières expériences avec l'open source après réalisation d'un inventaire (voir plus haut). C'est aussi l'occasion de commencer à établir une feuille de route simplifiée de développement des compétences.

- Identifier les compétences et formations requises.
- Mettre en place un projet pilote pour démarrer, apprendre au fil de l'eau, définir un premier jalon de réussite.
- Capitaliser sur les leçons apprises pour construire un corpus de connaissances.
- Commencer à identifier et documenter les prochaines étapes vers une adoption plus large.
- Élaborer une stratégie pour les prochains mois ou l'année, afin d'obtenir engagements et support financier.

Domaine de l'activité :

- Linux, Apache, Debian, compétences en administration ;
- Bases de données open source MariaDB, MySQL, PostgreSQL, etc. ;
- Technologies open source de virtualisation et cloud ;
- Pile LAMP et alternatives.

### Évaluation de l’opportunité

Comme toute technologie de l'information, et sans doute encore plus, l'open source induit l'innovation. L'open source croît et évolue rapidement : il est nécessaire que l'organisation se maintienne à jour.

Cette activité aide à identifier les domaines où la formation peut rendre les gens plus efficaces et plus confiants quant à l'usage de l'open source. Elle aide à prendre des décisions concernant l'évolution des employés. Initier des compétences minimales dans l'open source permet d'évaluer l'opportunité de :

- Compléter des solutions IT avec des technologies existantes du marché, développées par l'écosystème ;
- Développer de nouveaux types de collaboration internes et externes à l'organisation ;
- S'approprier des compétences en matière de technologies nouvelles et innovantes.

### Suivi de l'avancement

Les **points de contrôle** suivants dénotent une progression de cette activité :

- [ ] Une matrice de compétences est mise en place.
- [ ] Le domaine d'application des technologies OSS utilisées est défini de façon proactive, évitant ainsi leur usage incontrôlé.
- [ ] Un niveau d'expertise satisfaisant est acquis concernant ces technologies.
- [ ] Les équipes ont suivi une formation aux « bases de l'open source » pour démarrer.

### Outils

Un outil clé est la Matrice (ou Cartographie) d'Activité (ou Compétence(s)).

Cette activité peut être menée à bien par :

- l'utilisation de didacticiels en ligne (il en existe beaucoup de gratuits sur Internet) ;
- la participation à des conférences pour développeurs ;
- la formation professionnelle, etc.

### Recommandations

- Utiliser et développer des composants open source de manière sûre et efficace nécessite un état d'esprit ouvert et collectif, qu'il convient de reconnaître et propager tant de haut en bas (encadrement) que de bas en haut (développeurs).
- S'assurer que l'approche est activement supportée et promue par l'encadrement. Rien ne s'accomplira sans implication de la hiérarchie.
- Impliquer les gens (développeurs, parties prenantes) dans le processus : organiser des tables rondes et rester à l'écoute des idées.
- Laisser du temps et des ressources aux gens pour découvrir l'activité, tester et jouer avec ces nouveaux concepts. Si possible, de façon ludique : ludification et récompenses sont de bonnes incitations.

Un projet pilote avec les étapes suivantes peut servir de catalyseur :

- Identifier une technologie ou un framework pour démarrer ;
- Trouver de la formation en ligne, un didacticiel, des exemples de code pour expérimenter ;
- Construire un prototype de la solution attendue ;
- Identifier quelques experts pour challenger et guider l'implémentation.

### Ressources

- [Qu'est-ce qu'une Matrice de Compétences](https://blog.kenjo.io/what-is-a-competency-matrix) : introduction rapide.
- [Comment construire une Matrice de Compétences pour votre équipe](http://www.managersresourcehandbook.com/download/Skills-Matrix-Template.pdf) : un modèle commenté.
- [MOOC sur la culture libre](https://librecours.net/parcours/upload-lc000/) (en français) : cours en 6 parties sur la culture libre, introduction au droit d'auteur, à la propriété intellectuelle, aux licences open source.
