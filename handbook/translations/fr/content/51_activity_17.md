## Inventaire des ressources et compétences open source

Identifiant de l'activité : [GGI-A-20](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_20.md).

### Description

À chaque étape, dans une perspective de management, il est utile de disposer d'une cartographie, d'un inventaire des ressources, actifs et usages open source, de leur statut, ainsi que des besoins potentiels et solutions disponibles. Il s'agit également d'évaluer les efforts et compétences nécessaires à combler les manques.

Cette activité vise à prendre un cliché de la situation interne de l'open source, de l'état de l'art du marché, et d'évaluer l'écart entre eux.

- Inventorier l'usage de l'OSS dans la chaîne de développement logiciel comme dans les produits et composants logiciels utilisés en production.
- Identifier les technologies open source (solutions, frameworks, fonctionnalités innovantes) pouvant correspondre à vos besoins et aider à améliorer vos processus.

Ceci n'inclut pas

- L'identification et qualification des écosystèmes et communautés OSS liées (Objectif « Culture »).
- L'identification des dépendances vis-à-vis de librairies et composants OSS (Objectif « Confiance »).
- La détermination des compétences techniques (langages, frameworks…) et relationnelles (collaboration, communication…) nécessaires (faisant partie d'autres Activités : « Montée en compétence OSS » et « Compétences en développement logiciel open source »).

### Évaluation de l’opportunité

Inventaire des ressources open source disponibles pouvant aider à optimiser l'investissement et prioriser le développement des compétences.

Cette activité crée les conditions utiles à améliorer la productivité du développement, eu égard à l'efficacité et la popularité des composants, outils et principes de développement OSS pour produire des applications et infrastructures modernes.

- Ceci peut impliquer de simplifier le portefeuille de ressources OSS.
- Ceci peut nécessiter de former du personnel.
- Ceci permet d'identifier des besoins et alimenter votre feuille de route IT.

### Suivi de l'avancement

Les **points de contrôle** suivants dénotent une progression de cette Activité :

- [ ] Il existe une liste exploitable de ressources OSS que l'on « utilise », « intègre », « produit », « héberge », et les compétences associées.
- [ ] Nous sommes en voie d'amélioration de l'efficacité en utilisant des méthodes et outils à l'état de l'art.
- [ ] Nous avons identifié des ressources OSS non répertoriées à ce jour (qui peuvent avoir émergé en interne : est-il possible de définir une politique dans ce domaine ?)
- [ ] Nous demandons aux nouveaux projets d'approuver ou réutiliser des ressources OSS existantes (Objectif « Culture » ?).
- [ ] Nous avons une perception et compréhension raisonnablement fiables de l'étendue des usages OSS au sein de notre organisation.

### Outils

Il existe beaucoup de possibilités d'établir un tel inventaire. Une méthode peut consister à classer les ressources OSS en quatre catégories :

- Ce qu'on utilise : le logiciel utilisé en production ou développement ;
- Ce qu'on intègre : exemple, les librairies OSS que l'on intègre à une application maison ;
- Ce qu'on produit : exemple, une librairie que l'on a publiée sur GitHub, ou un projet OSS que l'on développe ou auquel on contribue régulièrement ;
- Ce qu'on héberge : l'open source que l'on fait tourner pour fournir un service interne comme un CRM, GitLab, nexus, etc. Un exemple de tableau pourrait ressembler à ceci :

| Utilisé | Intégré | Produit | Hébergé | Compétences |
| --- | --- | --- | --- | --- |
| Firefox, <br />LibreOffice, <br />Postgresql | Librairie slf4j | Librairie YY sur GH | GitLab, <br />Nexus | Java, <br />Python |

La même identification doit s'appliquer aux compétences et expérience :

- Disponibles au sein des équipes
- Pouvant être développées ou acquises en interne (formation, coaching, expérimentation)
- Nécessitant d'être obtenues via le marché, ou des partenariats / contrats.

### Recommandations

- Faire les choses simplement ;
- Il s'agit d'un exercice de niveau relativement général, pas d'un inventaire détaillé pour la comptabilité ;
- Bien que cette activité soit un bon point de départ, inutile de la terminer à 100% avant de lancer d'autres activités ;
- S'occuper des problèmes, ressources et compétences liés au **développement logiciel** fait partie de l'activité « Montée en compétences » (4.2) ;
- L'inventaire doit couvrir tous les domaines de l'IT : systèmes d'exploitation, intergiciels, bases de données, administration système, outils de développement et de tests, etc. ;
- Commencer à identifier les communautés pertinentes : il est plus facile d'obtenir du support et des retours concernant un projet lorsque sa communauté vous connaît déjà.

### Ressources

- Un excellent cours sur [Le logiciel Libre et Open Source (FOSS)](https://profriehle.com/open-courses/free-and-open-source-software), par le Professeur Dirk Riehle.
