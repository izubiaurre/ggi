## Politique d'achat de logiciels libres

Activity ID: [GGI-A-43](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_43.md).

### Description

Cette activité consiste à mettre en œuvre un processus de sélection, d'acquisition, d'achat de logiciels et de services open source. Il s'agit également de prendre en compte le coût réel des logiciels libres et de le provisionner. Les logiciels libres peuvent être « gratuits » à première vue, mais ils ne sont pas sans coûts internes et externes tels que l'intégration, la formation, la maintenance et le support.

Une telle politique exige que les solutions open source et propriétaires soient considérées de manière symétrique lors de l'évaluation de la valeur de l'investissement comme la combinaison optimale entre le coût total de possession et le rapport qualité-prix. Par conséquent, le département des achats informatiques doit considérer activement et équitablement les options open source, tout en s'assurant que les solutions propriétaires sont traitées sur un pied d'égalité dans les décisions d'achat.

La préférence pour l'open source peut être explicitement exprimée sur la base de la flexibilité intrinsèque de l'option open source lorsqu’il n'y a pas de différence de coût global significative entre les solutions propriétaires et open source.

Les services achat doivent comprendre que les entreprises qui offrent un soutien aux logiciels libres ne disposent généralement pas des ressources commerciales nécessaires pour participer à des appels d'offres, et adapter leurs politiques et processus d'achat de logiciels libres en conséquence.

### Évaluation de l’opportunité

Plusieurs raisons justifient les efforts pour mettre en place des politiques spécifiques d'approvisionnement en logiciels libres :

- L'offre de logiciels et de services commerciaux open source est croissante, ne peut être ignorée, et nécessite la mise en place de politiques et de processus d'approvisionnement spécifiques.
- Il existe une offre croissante de solutions commerciales open source très compétitives pour les systèmes d'information des entreprises.
- Même après avoir adopté un composant open source gratuit et l'avoir intégré dans une application, des ressources internes ou externes doivent être fournies pour maintenir ce code source.
- Le coût total de possession (TCO) est souvent (mais pas nécessairement) plus faible pour les solutions open source : pas de frais de licence à payer lors de l'achat/la mise à niveau, marché ouvert pour les fournisseurs de services, possibilité de fournir une partie ou la totalité de la solution soi-même.

### Suivi de l'avancement

Les **points de contrôle** suivants dénotent une progression de cette activité :

- [ ] Les nouveaux appels à propositions demandent de manière proactive des soumissions open source.
- [ ] Le service achats dispose d'un moyen d'évaluer les solutions open source par rapport aux solutions propriétaires.
- [ ] Un processus d'achat simplifié pour les logiciels et services open source a été mis en place et documenté.
- [ ] Un processus d'approbation faisant appel à une expertise inter fonctionnelle a été défini et documenté.

### Recommandations

- Assurez-vous de tirer parti de l'expertise de vos équipes IT, DevOps, cybersécurité, gestion des risques, et achats lors de la mise en place du processus ([5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/), en anglais).
- La loi sur la concurrence peut exiger que le terme « open source » ne soit pas spécifiquement mentionné.
- Sélectionnez la technologie en amont, puis passez à l'appel d'offres pour les services de personnalisation et d'assistance.

### Ressources

- [Facteurs de décision en faveur de l'achat de logiciel open source](http://oss-watch.ac.uk/resources/procurement-infopack) : ce n'est pas nouveau, mais c'est une excellente lecture de nos collègues d'OSS-watch au Royaume-Uni. Consultez-les [diapositives](http://oss-watch.ac.uk/files/procurement.odp).
- [5 bonnes pratiques d'approvisionnement Open Source](https://anchore.com/blog/5-open-source-procurement-best-practices/) : un article récent sur l'approvisionnement en logiciels libres avec des conseils utiles.
