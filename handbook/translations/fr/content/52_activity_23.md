## Gérer les dépendances logicielles

Activity ID: [GGI-A-23](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_23.md).

### Description

Un programme *d’identification des dépendances* recherche les dépendances réellement utilisées dans la base de code. Par conséquent, l’organisation doit établir et maintenir une liste de dépendances connues pour sa base de code et surveiller l’évolution des fournisseurs identifiés.

L’établissement et la mise à jour d’une liste de dépendances connues est un outil et un pré-requis pour :

- La vérification de la propriété intellectuelle et des licences : certaines licences ne peuvent pas être mélangées, même en tant que dépendance. Il faut connaître ses dépendances pour évaluer les risques juridiques associés.
- Gestion des vulnérabilités : l’ensemble d’un logiciel est aussi faible que sa partie la plus faible : voir l’exemple de la faille [Heartbleed](https://fr.wikipedia.org/wiki/Heartbleed). Il faut connaître ses dépendances pour évaluer les risques de sécurité associés ;
- Cycle de vie et durabilité : une communauté active sur un projet dont on dépend est un bon signe pour les corrections de bugs, les optimisations et les nouvelles fonctionnalités ;
- Sélection réfléchie des dépendances utilisées, selon des critères de « maturité » — l’objectif étant d’utiliser des composants open source sûrs, avec une base de code saine et bien maintenue, et une communauté vivante, active et réactive qui acceptera des contributions externes, etc.

### Évaluation de l’opportunité

L’identification et le suivi des dépendances est une étape nécessaire pour atténuer les risques associés à toute réutilisation de code. En outre, la mise en œuvre d’outils et de processus pour gérer les dépendances logicielles est une condition préalable pour gérer correctement la qualité, la conformité et la sécurité.

Considérez les questions suivantes :

- Quel est le risque pour l’entreprise (coût, réputation, etc.) si le logiciel est corrompu, attaqué ou poursuivi en justice ?
- La base de code est-elle considérée comme critique pour les personnes, l’organisation ou l’entreprise ?
- Que se passe-t-il si un composant dont dépend une application change de dépôt logiciel ?

La première étape, minimale, consiste à mettre en place un outil d’analyse de la composition du logiciel (SCA). Le soutien de sociétés de conseil spécialisées peut être nécessaire pour une analyse complète de la composition du logiciel ou une cartographie des dépendances.

### Suivi de l'avancement

Les **points de contrôle** suivants dénotent une progression de cette activité :

- [ ] Les dépendances sont identifiées dans tout le code développé en interne ;
- [ ] Les dépendances sont identifiées dans tout le code externe exécuté au sein de l’organisation ;
- [ ] Une procédure d’analyse de la composition du logiciel (SCA) ou d’identification des dépendances, facile à mettre en place, est disponible pour que les projets l’ajoutent à leur processus d’intégration continue ;
- [ ] Des outils d'analyse des dépendances sont utilisés.

### Outils

- [OWASP Dependency check](https://github.com/jeremylong/DependencyCheck) : outil d’analyse de composition logicielle (SCA) qui tente de détecter les vulnérabilités publiquement divulguées contenues dans les dépendances d’un projet ;
- [OSS Review Toolkit](https://oss-review-toolkit.org/) : une suite d’outils pour aider à l’examen des dépendances des logiciels Open Source ;
- [Fossa](https://github.com/fossas/fossa-cli) : analyse rapide, portable et fiable des dépendances. Supporte l’analyse des licences et des vulnérabilités. Ne dépend pas du langage, s’intègre à plus de 20 systèmes de build ;
- [Software 360](https://projects.eclipse.org/projects/technology.sw360).
- [Outil de licence Eclipse Dash](https://github.com/eclipse/dash-licenses) : prend une liste de dépendances et demande à [ClearlyDefined](https://clearlydefined.io) de vérifier leurs licences.
- [Le projet FOSSology](https://www.fossology.org/) : FOSSology est un projet open source dont la mission est de faire progresser la conformité aux licences open source.

### Recommandations

- Effectuer des audits réguliers sur les dépendances et les exigences de la propriété intellectuelle pour atténuer les risques juridiques ;
- Idéalement, intégrer la gestion des dépendances dans le processus d’intégration continue afin que les problèmes (nouvelle dépendance, incompatibilité de licence) soient identifiés et corrigés dès que possible ;
- Garder la trace des vulnérabilités liées aux dépendances, tenir les utilisateurs et les développeurs informés ;
- Informer les gens des risques associés à une mauvaise licence ;
- Proposer une solution facile pour que les projets puissent mettre en place une vérification des licences sur leur base de code ;
- Communiquer sur son importance et aider les projets à l’ajouter à leurs systèmes d’intégration continue ;
- Mettre en place un KPI visible pour les risques liés aux dépendances.

### Ressources

- Page du groupe des [outils de vérification de la licence des logiciels libres existants](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence-Compliance-Tools/) ;
- [Conformité des licences des logiciels libres et open source : Outils pour l’analyse de la composition des logiciels]((https://www.computer.org/csdl/magazine/co/2020/10/09206429/1npxG2VFQSk)), par Philippe Ombredanne, nexB Inc ;
- [Modèle de maturité de la durabilité des logiciels](http://oss-watch.ac.uk/resources/ssmm) ;
- [CHAOSS](https://chaoss.community/) : logiciel libre d’analyse de la santé communautaire ;
