# Organisation

## Terminologie

Le schéma directeur de la méthodologie « Good Governance » est structuré autour de quatre concepts : les objectifs, les activités, les tableaux de bord d'activité, et les itérations.

* **Objectifs** : un objectif est un ensemble d'activités liées à un sujet commun. On définit cinq objectifs : l'Usage, la Confiance, la Culture, l'Engagement et la Stratégie. Les objectifs peuvent être atteints indépendamment, en parallèle, et complétés de manière itérative, à travers les activités.
* **Activités** : dans le cadre d'un objectif, une activité concerne une préoccupation ou un sujet particulier - par exemple, la prise en compte de la conformité juridique - que l'on peut considérer comme une étape vers les objectifs du programme. L'ensemble des activités défini par la méthode GGI est appelé « activités canoniques ».
* **Tableaux de bord d'activité** : pour mettre en œuvre la GGI dans une organisation, les activités canoniques doivent être adaptées au contexte, ce qui donne naissance à un ensemble de « tableaux de bord d'activité » personnalisés. Un tableau de bord d'activité personnalisé décrit comment l'activité sera mise en œuvre dans le contexte d'une organisation, et comment son avancement sera contrôlé.
* **Itérations** : la méthodologie GGI est un système de management, qui nécessite donc des évaluations, examens et révisions périodiques. Prenons pour exemple la comptabilité dans une organisation : c'est un processus continu avec au moins un arrêté annuel, le compte de résultats ; de même, le processus GGI nécessite au moins une revue annuelle, quand bien même les revues peuvent être plus ou moins partielles ou fréquentes selon les activités.

## Objectifs

Les activités définies par GGI sont regroupées par « objectifs ». Chaque objectif concerne un domaine spécifique d'avancement du processus. De l'Usage à la Stratégie, les objectifs couvrent des problématiques liées à toutes les parties prenantes, des équipes de développement aux cadres dirigeants.

* **Usage** : cet objectif recouvre les étapes initiales de l'utilisation de logiciel open source. Les activités liées à cet objectif sont les premières étapes d'une programmation de l'open source, visant à identifier avec quelle efficacité l'open source est utilisé et ce qu'il apporte à l'organisation. Ceci inclut la formation et la gestion du savoir, l'inventaire de l'open source déjà utilisé en interne, et la mise en évidence de certains concepts de l'open source qui peuvent être mis en œuvre lors du processus.
* **Confiance** : cet objectif cible un usage sûr de l'open source. La confiance est question de conformité juridique, de gestion des dépendances et des vulnérabilités, il s'agit de confiance quant à l'usage et la gestion de l'open source au sein de l'organisation.
* **Culture** : l'objectif culturel se compose d'activités destinées à rendre les équipes à l'aise avec l'open source, la participation individuelle à des activités collaboratives, la compréhension et la mise en œuvre des bonnes pratiques de l'open source. Cet objectif favorise le sentiment d'appartenance des individus à la communauté de l'open source.
* **Engagement** : cet objectif vise à engager une organisation dans l'écosystème open source. Des ressources humaines et financières sont allouées pour contribuer à des projets open source. Ici, l'organisation assume son rôle citoyen dans l'open source et sa responsabilité à garantir la soutenabilité de l'écosystème.
* **Stratégie** : cet objectif consiste à rendre l'open source visible et acceptable au plus haut niveau du management de l'organisation. Il s'agit de reconnaître que l'open source est un catalyseur stratégique de souveraineté numérique, d'innovation, d'attractivité et d'image positive.

## Activités canoniques

Les « activités canoniques » sont au centre du schéma GGI. Dans la version actuelle, la méthodologie GGI propose cinq activités canoniques par objectif, soit vingt-cinq au total. Les activités canoniques sont décrites en utilisant le modèle suivant de division en sections :

* *Description* : résumé du thème abordé par l'activité et des étapes de sa mise en œuvre.
* *Évaluation des opportunités* : décrit quand et pourquoi il est pertinent d'entreprendre l'activité.
* *Suivi de l'avancement* : décrit comment mesurer la progression de l'activité et évaluer son succès.
* *Outils* : liste de technologies et d'outils qui peuvent aider à accomplir l'activité.
* *Recommandations* : conseils et bonnes pratiques proposés par les contributeurs à GGI.
* *Ressources* : liens et références pour en savoir plus quant à la thématique liée à l'activité.

### Description

Cette section propose une description générale de l'activité, un résumé thématique pour positionner l'activité et l'approche de l'open source dans le contexte d'un objectif.

### Évaluation de l’opportunité

Pour aider à structurer une approche itérative, chaque activité propose une section « Évaluation des opportunités », avec une ou plusieurs question(s) associée(s). L'évaluation se concentre sur ce qui est pertinent pour entreprendre cette activité, et les besoins auxquels elle répond. Évaluer l'opportunité aidera à définir les efforts et les ressources nécessaires, ainsi qu'à évaluer les coûts et le retour sur investissement attendu.

### Suivi de l'avancement

Cette étape se concentre sur la définition d'objectifs, de KPI, et la fourniture de *points de contrôle* aidant à évaluer la progression de l'activité. Les points de contrôle sont proposés, ils peuvent aider à définir un planning pour le processus GGI, ses priorités, et la manière d'en mesurer la progression.

### Outils

Cette section liste des outils pouvant aider à mettre en œuvre l'activité ou en instrumenter une étape particulière. Il ne s'agit pas de recommandations impératives, et la liste d'outils n'est pas exhaustive : il s'agit de suggestions sur lesquelles on peut se baser avant de les adapter au contexte.

### Recommandations

Cette section sera régulièrement mise à jour avec le retour d'expérience d'utilisateurs, entre autres recommandations pouvant aider à mener à bien l'activité.

### Ressources

Les ressources se composent d'études de cas, documents de référence, événements ou contenu en ligne, dont la thématique peut permettre d'enrichir et développer l'activité. Les ressources ne sont pas exhaustives, elles servent de point de départ ou de suggestions pour étendre la sémantique de l'activité à un contexte particulier.

## Tableaux de bord d'activité personnalisés

Les tableaux de bord d'activité personnalisés sont nettement plus détaillés que les « activités canoniques ». Un tableau de bord inclut des détails spécifiques à l'organisation qui met en œuvre GGI. L'usage des tableaux de bord est décrit dans la section « Méthodologie ».
