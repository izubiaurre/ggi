## Sensibiliser les cadres dirigeants

Activity ID: [GGI-A-34](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_34.md).

### Description

L'initiative open source de l'organisation ne produira ses avantages stratégiques que si elle est appliquée au plus haut niveau, en intégrant l'ADN open source dans la stratégie et le fonctionnement interne de l'entreprise. Un tel engagement ne peut se produire si les cadres supérieurs et le comité de direction n'en font pas eux-mêmes partie. La formation et l'état d'esprit open source doivent également être étendus à ceux qui façonnent les politiques, les décisions et la stratégie globale, tant à l'intérieur qu'à l'extérieur de l'entreprise.

Cet engagement garantit que les améliorations pratiques, les changements d'état d'esprit et les nouvelles initiatives bénéficient d'un soutien constant, bienveillant et durable de la part de la hiérarchie, ce qui entraîne une participation plus fervente des travailleurs. Il façonne la façon dont les acteurs externes perçoivent l'organisation, ce qui apporte des avantages en termes de réputation et d'écosystème. C'est également un moyen de légitimer l'initiative et ses avantages à moyen et long terme.

### Évaluation de l’opportunité

Cette activité devient essentielle si/quand :

- L'organisation a fixé des objectifs globaux relatifs à la gestion des logiciels open source, mais peine à les atteindre. Il est peu probable que l'initiative puisse aboutir sans une bonne connaissance et un engagement clair des cadres dirigeants.
- L'initiative a déjà commencé et progresse, mais les niveaux supérieurs de la hiérarchie ne la suivent pas correctement.

Il devrait devenir évident, en toute logique, que tout ce qui n’est pas une utilisation ad-hoc de l’open source nécessite une approche cohérente et réfléchie, étant donné l’étendue de l’activité en termes d’équipes impliquées et de changement culturel.

### Suivi de l'avancement

Les **points de contrôle** suivants dénotent une progression de cette Activité :

- [ ] Il existe un bureau/responsable de la gouvernance mandaté pour définir une stratégie uniforme en matière de logiciels open source dans toute l'entreprise et s'assurer que le champ d'application est clair.
- [ ] Il y a un engagement clair et contraignant de la hiérarchie envers la stratégie de logiciels open source.
- [ ] Il y a une communication transparente de la hiérarchie sur son engagement envers le programme.
- [ ] La hiérarchie est disponible pour discuter des logiciels libres. Elle peut être sollicitée et mise au défi sur ses promesses.
- [ ] Il existe un budget et un financement appropriés pour l'initiative.

### Recommandations

Voici quelques exemples d'actions associées à cette activité :

- Organiser des formations pour démystifier l'open source auprès des cadres dirigeants.
- Obtenir une approbation explicite et pratique de l'utilisation et de la stratégie du logiciel open source.
- Mentionner et approuver explicitement le programme open source dans les communications internes.
- Mentionner et approuver explicitement le programme open source dans les communications publiques.

Open source is a *strategic enabler* that embarks *enterprise culture*. What does this mean?

- L'open source peut être utilisé comme un mécanisme pour challenger les fournisseurs et réduire les coûts d'acquisition des logiciels.
   - Should open source come under the purview of *Software Asset Managers* or *purchasing departments*?
- Open source licences enshrine the freedoms that deliver the benefits of open source, but they also carry *obligations*. If not met appropriately, responsibilities can create legal, commercial and image risks to an organisation.
   - Les conditions de la licence permettront elles de voir des parties du code qui devraient rester confidentielles ?
   - Cela aura-t-il un impact sur le portefeuille de brevets de mon organisation ?
   - Comment les équipes projet doivent elles être formées et supportées à ce sujet ?
- La plus grande valeur de l'open source réside dans la contribution aux projets open source externes.
   - Comment mon entreprise doit-elle encourager (et suivre) cette démarche ?
   - Comment les développeurs doivent ils utiliser GitHub, GitLab, Slack, Discord, Telegram, ou tout autre outil que les projets open source utilisent habituellement ?
   - L'open source peut-il avoir un impact sur les politiques de ressources humaines de l'entreprise ?
- Bien sûr, il ne s'agit pas seulement de contribuer en retour, qu'en est-il de mes propres projets open source ?
   - Suis-je prêt à faire de l'innovation *ouverte* ?
   - Comment mes projets vont-ils gérer les contributions *entrantes* ?
   - Dois-je faire l'effort d'entretenir une communauté pour un projet donné ?
   - Comment dois-je diriger la communauté, quel rôle doivent jouer ses membres ?
   - Suis-je prêt à céder les décisions relatives à la feuille de route à une communauté ?
   - L'open source peut-il être un outil précieux pour réduire le cloisonnement entre les équipes de l'entreprise ?
   - Dois-je gérer le transfert de l'open source d'une entité de l'entreprise à une autre ?
