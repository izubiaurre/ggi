## Point de vue des RH

Activity ID: [GGI-A-28](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_28.md).

### Description

Le passage à la culture open source a de profondes répercussions sur les RH :

- **Nouveaux processus et contrats** : Les contrats doivent être adaptés pour permettre et promouvoir les contributions externes. Cela inclut les questions de propriété intellectuelle et de licence pour le travail effectué au sein de l'entreprise, mais aussi la possibilité pour l'employé ou le contractant d'avoir ses propres projets.
- **Différents types de personnes** : Les personnes travaillant dans le domaine de l'open source ont souvent des motivations et des mentalités différentes de celles des entreprises propriétaires. Les processus et les mentalités doivent s'adapter à ce paradigme axé sur la réputation de la communauté, afin d'attirer de nouveaux types de talents et de les retenir.
- **Développement de carrière** : il est nécessaire d'offrir un plan de carrière qui encourage et valorise les employés pour leurs compétences techniques et non techniques ainsi que pour les compétences attendues par votre organisation (collaboration pour diriger les efforts communautaires, communication pour agir en tant que porte-parole de votre entreprise, etc.). Les RH ont un rôle clé à jouer pour faire de l'open source un objectif culturel.

**Main d'œuvre** Pour un développeur qui travaille depuis longtemps sur la même solution propriétaire, passer à l'open source peut sembler un changement important et nécessiter une adaptation. Mais pour la plupart des développeurs, les logiciels open source n'apportent que des avantages.

Les développeurs qui sortent aujourd’hui de l'école ou de l'université ont tous travaillé sur des logiciels libres. Au sein d'une entreprise, la grande majorité des développeurs utilisent des langages open source et importent quotidiennement des librairies ou des extraits de code open source. Il est en effet beaucoup plus facile de coller des lignes de code open source dans un programme que de déclencher le processus de sourcing interne, qui passe par de multiples validations à travers la ligne hiérarchique.

L'open source rend le travail du développeur plus intéressant car avec l'open source, un développeur est toujours à l'affût de ce qui est inventé par ses pairs hors de l'entreprise. Il reste donc à la pointe de la technologie.

Pour une organisation, il est nécessaire d'avoir une stratégie RH pour 1/ qualifier ou requalifier la main d'œuvre existante 2/ positionner l'entreprise sur le recrutement de nouveaux talents, en reflétant l'attractivité de l'entreprise par le biais de l'open source.

> Attirer des personnes avec un bon état d'esprit open source, qui comprennent déjà le code et savent comment travailler avec les autres, est merveilleux. L'alternative d'évangélisation / formation / stage est intéressante mais plus coûteuse et plus longue.
>
> &mdash; <cite>PDG d'un fournisseur de logiciels OSS</cite>

Cela illustre que l'embauche de personnes dotées d'un ADN open source est une voie d'accélération à prendre en compte dans la stratégie des RH.

#### Processus

- Établir ou revoir les descriptions de poste (compétences techniques, compétences générales, expérience)
- Programmes de formation : auto-formation, formation formelle, coaching de la direction, lien avec les pairs, communautés
- Établir ou revoir le plan de carrière : compétences, résultats/impact et étapes clés de la carrière

### Évaluation de l’opportunité

1. Encadrer les pratiques de développement : le problème n'est probablement pas tant d'inciter les développeurs à utiliser davantage de logiciels libres, que de s'assurer qu'ils les utilisent en toute sécurité, en respectant les conditions de licence de chaque technologie libre, et sans abandonner les contrôles de sécurité traditionnels (les lignes de code libres peuvent contenir des codes malveillants),
1. Revoir les pratiques de collaboration : avec les pratiques de développement vient l'opportunité d'étendre l'agilité et la collaboration à d'autres secteurs d'activité de votre organisation. Le sourcing interne est souvent utilisé pour favoriser ces comportements, bien qu'étant à mi-chemin de la culture open source,
1. Culture de l'organisation : en fin de compte, tout dépend de la culture de votre organisation. L'open source peut être le porte-drapeau de valeurs telles que l'ouverture, la collaboration, l'éthique et la durabilité.

### Suivi de l'avancement

Les **points de contrôle** suivants dénotent une progression de cette Activité :

- [ ] Une formation est disponible pour présenter à la fois les avantages et les contraintes (conformité aux conditions de propriété intellectuelle de la licence) liés à l'open source.
- [ ] Chaque développeur, chaque architecte, chaque chef de projet (ou Product Owner/Business Owner) comprend les avantages et les contraintes (conformité aux conditions de propriété intellectuelle de la licence) liés à l'open source.
- [ ] Les développeurs sont encouragés à contribuer aux communautés open source, et à en assumer la responsabilité, et peuvent recevoir une formation adéquate pour le faire.
- [ ] Les aptitudes et les compétences sont reflétées dans les descriptions de poste et les étapes de carrière de l'organisation.
- [ ] L'expérience que les développeurs ont acquise dans le domaine de l'open source (contributions aux communautés open source, participation au processus de conformité interne, interventions externes pour l'entreprise…) est prise en compte dans le processus d'évaluation des RH.

### Outils

- Matrice de compétences.
- Programmes de formation publics (ex. open source school).
- Sourcing : GitHub, GitLab, LinkedIn, Meetups, Epitech, Epita …
- Modèles de contrat (clause de loyauté).
- Descriptions de poste (modèles) et étapes de carrière (modèles).

### Recommandations

La plupart du temps, les développeurs connaissent déjà certains principes de l'open source et sont prêts à travailler avec et sur des logiciels open source. Cependant, il y a encore quelques actions que la direction devrait prendre :

- Privilégier l'expérience des logiciels libres à l'embauche, même si le poste pour lequel le développeur est recruté ne concerne que la technologie propriétaire. Il y a de fortes chances, avec la transformation numérique, que le développeur soit un jour amené à travailler sur de l'open source.
- Programme de formation aux logiciels libres : chaque développeur, chaque architecte, chaque chef de projet (ou Product Owner/Business Owner) devrait avoir accès à des ressources de formation (vidéos ou formations en présentiel) qui présentent les avantages de l'open source mais aussi les contraintes en termes de propriété intellectuelle et de conformité des licences.
- Des formations devraient être mises à disposition des développeurs qui souhaitent contribuer aux communautés open source et faire partie des organes de gouvernance de ces communautés (certifications Linux).
- Reconnaissance dans les processus d'évaluation personnelle des RH de la contribution de l'employé (développeur ou architecte) aux sujets liés à l'open source tels que les contributions aux communautés open source et la conformité aux conditions de propriété intellectuelle des licences. La plupart des sujets sont partagés et s'inscrivent dans les parcours de carrière techniques, tandis que certains pourraient ou devraient être spécifiques.
- Stratégie de communication et positionnement de l'entreprise : il faut aborder les aspects de communication (dans quelle mesure le sujet est essentiel pour votre organisation au point d'être reflété dans votre rapport annuel), comment cela affecte-t-il votre stratégie de communication (un contributeur open source pourrait être un porte-parole de votre entreprise, y compris pour les contacts avec la presse).

### Ressources

- En ce qui concerne la capacité des personnes à s'exprimer en dehors de l'entreprise lors d'événements, veuillez consulter l'activité « Affirmer publiquement l'utilisation de l'open source » de l'objectif « Engagement ».
