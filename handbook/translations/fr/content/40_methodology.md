# Méthodologie

Mettre en place la méthodologie GGI est une initiative à impact, ayant des conséquences. Cela implique plusieurs services, processus et catégories de personnel, des développeurs aux cadres dirigeants et du travail quotidien à la gestion des ressources humaines. Il n'y a pas de solution miracle pour établir une bonne gouvernance de l'open source. Différents types de culture d'entreprise et de situations impliqueront différentes approches de gouvernance de l'open source. Pour chaque organisation, contraintes et attentes seront différentes, conduisant à différentes manières de mettre en œuvre le programme.

Dans ces conditions, la méthodologie GGI fournit un schéma directeur générique pouvant être adapté au domaine, à la culture et aux besoins particuliers d'une organisation. Alors que le schéma se veut complet, la méthodologie peut être mise en œuvre progressivement. Il est possible de démarrer le programme en sélectionnant les objectifs et activités jugés pertinents dans un contexte précis : l'idée étant de construire une première ébauche de feuille de route, aidant à démarrer l'initiative.

Au-delà de ce cadre, nous recommandons aussi d'entrer en contact avec ses homologues ou confrères via un réseau établi, comme l'initiative [OSPO Alliance](https://ospo-alliance.org) en Europe, ou d'autres initiatives comparables telles que TODO Group ou OSPO++. L'important étant de pouvoir échanger avec des gens engagés dans une initiative similaire, et partager les problèmes rencontrés et les solutions appliquées.

## Préparer le terrain

Vu l'ambition de la méthodologie GGI et son impact potentiel, il est important de communiquer avec diverses personnes au sein de l'organisation. Il serait adéquat de les impliquer afin de définir un ensemble d'attentes et de besoins réalistes pour partir sur de bonnes bases, et susciter l'intérêt et l'adhésion. Il paraît opportun de publier les tableaux de bord personnalisés dans la plateforme collaborative de l'organisation, afin qu'ils puissent servir à communiquer avec les parties prenantes. Quelques conseils :

* Identifier les parties prenantes, les faire s'entendre sur un ensemble d'objectifs primaires. Les impliquer dans le succès de l'initiative dans le cadre de leur activité quotidienne.
* Obtenir l'adhésion nécessaire à démarrer, un accord sur les étapes et le rythme, et prévoir des points réguliers pour informer les acteurs de l'avancement.
* S'assurer que les acteurs comprennent les avantages de ce qui sera réalisé, et ce que ça implique : les améliorations attendues doivent être clairement formulées, et les résultats visibles.
* Établir un diagnostic de l'état de l'art quant à l'open source au sein de l'organisation concernée. Résultat : un document décrivant ce que le programme va réaliser, où en est l'organisation, et où elle souhaite aller.

## Flux de travail

En tant que praticiens modernes du logiciel, nous apprécions les méthodes agiles qui s'appuient sur des incréments progressifs et sûrs, considérant de bon aloi de réévaluer la situation régulièrement et de fournir un minimum de résultats intermédiaires significatifs.

Dans le contexte vivant d'un programme d'OSPO, ceci est notoirement pertinent, car de nombreux aspects secondaires vont évoluer au fil du temps, depuis la stratégie et la réaction de l'organisation vis-à-vis de l'open source jusqu'à l'implication et la disponibilité des personnes impliquées. L'itération et la réévaluation périodique permettent également de s'adapter à l'acceptation du programme, de mieux suivre les tendances et opportunités, et de laisser l'organisation et les parties prenantes prendre acte des améliorations progressives.

Idéalement, la méthodologie peut être mise en œuvre en cinq phases, comme suit :

1. **Découverte** Comprendre les concepts clés, s'approprier la méthodologie, aligner les objectifs et les attentes.
1. **Personnalisation** Adapter la description des activités et l'évaluation des opportunités aux spécificités de l'organisation.
1. **Priorisation** Identifier les résultats et objectifs principaux, les tâches et outils, planifier les étapes et établir un premier échéancier.
1. **Activation** Finaliser les tableaux de bord, le budget, les affectations, documenter les tâches et les intégrer à son outillage de gestion projet.
1. **Itération** Évaluer et mesurer les résultats, mettre en évidence les problèmes, améliorer, ajuster. Itérer chaque trimestre ou semestre.

Se préparer à la première itération du programme :

* Identifier un premier ensemble de tâches à réaliser, et les prioriser selon les besoins (écarts à la situation cible) et l'échéancier. Résultat : liste des tâches sur lesquelles travailler durant l'itération.
* Définir un ensemble d'exigences et de domaines d'amélioration, les communiquer aux parties prenantes et utilisateurs, et obtenir leur approbation ou engagement.
* Remplir les tableaux de bord pour suivre la progression. Un modèle de tableau de bord peut être téléchargé sur le [dépôt GGI](https://gitlab.ow2.org/ggi/ggi/-/tree/main/resources/).

À la fin de chaque itération, faire une rétrospective et se préparer à l'itération suivante :

* Communiquer sur les derniers progrès.
* Évaluer où vous en êtes, si les tâches prévues ont été finalisées, et redéfinir le planning en fonction.
* Vérifier les points bloquants et les problèmes, demander assistance aux autres acteurs ou services si besoin.
* Re-prioriser les tâches selon le contexte mis à jour.
* Définir un nouveau sous-ensemble de tâches à effectuer.

## Configuration manuelle : utiliser les tableaux de bord personnalisés

Un tableau de bord personnalisé est un formulaire décrivant une « activité canonique » adaptée aux besoins spécifiques d'une organisation. L'ensemble des tableaux de bord personnalisés constitue la feuille de route pour la gestion de l'open source.

`Pour information, de par l'expérience acquise, compter environ une heure pour adapter une activité canonique et l'intégrer à un tableau de bord personnalisé pour votre organisation.`

Le tableau de bord personnalisé se compose des sections suivantes :

* **Intitulé mis au clair** Avant tout, prenez quelques minutes pour bien comprendre à quoi s'applique l'Activité et évaluer sa pertinence, dans le cadre de votre cheminement global vers la gestion de l'open source.
* **Description personnalisée** Adaptez l'activité aux spécificités de votre organisation : définissez son champ d'application, le cas d'usage particulier auquel l'appliquer.
* **Évaluation de l'opportunité** Expliquez en quoi il est pertinent d'entreprendre cette activité, à quels besoins elle répond. Quels sont nos points faibles ? Nos opportunités de progrès ? Que peut-on gagner ?
* **Objectifs** Définissez quelques objectifs centraux pour l'activité. Points à renforcer, opportunités de progrès, souhaits. Identifiez les tâches essentielles. Ce que l'on souhaite réaliser dans cette itération.
* **Outils** Technologies, outils et produits utiles à cette activité.
* **Notes opérationnelles** Indications sur l'approche, la méthode, la stratégie pour avancer dans cette activité.
* **Résultats clés** Définissez des résultats mesurables et vérifiables. Choisissez des résultats attestant de progrès en phase avec les objectifs. Listez les KPI correspondants.
* **Progression et score** La progression est, en %, le pourcentage d'achèvement du résultat ; Le score est la note de réussite personnelle.
* **Évaluation personnelle** Pour chaque résultat, vous pouvez fournir une courte explication et exprimer votre taux de satisfaction personnelle via le score.
* **Chronologie** Précisez les dates de début et de fin, un phasage des tâches, les étapes critiques, les jalons.
* **Efforts** Évaluez les besoins en temps et ressources matérielles, internes et externes. Quels sont les efforts attendus ? Quel en sera le coût ? De quelles ressources a-t-on besoin ?
* **Acteurs** Listez les participants. Définissez qui est responsable ou en charge des tâches et activités.
* **Problèmes et enjeux** Identifiez les principaux enjeux, les difficultés attendues, les risques, points de blocage, incertitudes, points d'attention, dépendances critiques.
* **État** Rédigez une évaluation synthétique expliquant comment se porte l'activité : situation saine ? retards ? etc.
* **Évaluation globale des progrès** Votre propre évaluation synthétique du progrès de l'activité, axée sur la gestion et envisagée globalement.

## Configuration automatique : utilisation de la fonctionnalité de déploiement de GGI

À partir de la version 1.1 du manuel, GGI propose [My GGI Board](https://gitlab.ow2.org/ggi/my-ggi-board), un outil automatisé permettant de déployer votre propre instance de la GGI en tant que projet GitLab. Le processus d'installation prend moins de 10 minutes, est entièrement documenté et offre un moyen simple et fiable de personnaliser les activités, de suivre leur exécution au fur et à mesure de leur progression et de communiquer les résultats à vos partenaires. Voici un exemple concret du déploiement : [GitLab de l'initiative](https://gitlab.ow2.org/ggi/my-ggi-board-test), avec le site Web généré automatiquement disponible sur [ses pages GitLab](https://ggi.ow2.io/my-ggi-board-test/).

![Activités de déploiement de GGI](resources/images/ggi_deploy_activities.png)

Voici un workflow standard pour utiliser la fonctionnalité de déploiement :

1. Forkez My GGI Board vers votre propre instance ou projet GitLab et configurez-le en suivant les instructions du fichier README : <https://gitlab.ow2.org/ggi/my-ggi-board> du projet. Cela permettra :

- Créez toutes les activités en tant que tickets dans le projet.
- Créez un joli dashboard pour vous aider à visualiser et à gérer les activités.
- Créez un site web statique, hébergé sur les pages de votre instance GitLab, avec les informations extraites des activités.
- Mettez à jour la description du projet avec les liens appropriés vers le dashboard d'activités et votre site web statique.

1. À partir de là, vous pouvez commencer à examiner les différentes activités et à remplir la section du tableau de bord.

- La section des tableaux de bord est l'équivalent électronique (et simplifié) des tableaux de bord ODT mentionnés ci-dessus. Elles permettent d'adapter l'activité à votre contexte, en répertoriant les ressources locales, les risques et les opportunités, et en définissant des objectifs personnalisés nécessaires à la réalisation de l'activité.
- Si une activité ne s'applique pas à votre contexte, il suffit de la marquer comme "Non Sélectionnée" ou de la fermer.
- Il s'agit d'un processus assez long, mais essentiel car il vous aidera, étape par étape, à définir votre propre feuille de route et votre propre plan.

1. Une fois les activités définies, vous pouvez commencer à mettre en œuvre votre propre OSPO. Sélectionnez quelques activités qui vous semblent pertinentes pour commencer, et changez leur label de progression de l'état "Pas encore commencé" à l'état "En cours". Vous pouvez utiliser les fonctionnalités de GitLab pour vous aider à organiser le travail (commentaires, attributaires, etc.) ou tout autre outil. Il est facile de créer des liens avec les activités, et il existe de nombreuses intégrations de qualité.
1. Régulièrement (chaque semaine, chaque mois, selon votre emploi du temps), évaluez et passez en revue les activités en cours et lorsqu'elles sont terminées, changez le label de «En cours» à «Terminé». Sélectionnez-en quelques autres et recommencez à l'étape 3 jusqu'à ce qu'ils soient tous terminés.

Le site web propose une vue d'ensemble rapide des activités actuelles et passées, et extrait la section tableau de bord des tickets pour n'afficher que les informations pertinentes au niveau local. Lorsque des modifications sont apportées aux tickets (activités), elles sont automatiquement mises à jour dans le site web généré. A noter que les pipelines CI pour la génération automatique du site web sont exécutés automatiquement chaque nuit, mais vous pouvez facilement les lancer à partir de la section CI/CD du projet GitLab. L'image suivante montre l'interface du site web généré automatiquement.

![Site web de déploiement GGI](resources/images/ggi_deploy_website.png)

Vous pouvez poser des questions ou obtenir de l'aide pour la fonction de déploiement sur notre page d'accueil GitLab, les commentaires sont les bienvenus.

> Page d'accueil de GGI Deploy : <https://gitlab.ow2.org/ggi/my-ggi-board>

## Profitez-en !

Communiquez sur votre réussite et appréciez la tranquillité d'esprit que procure une stratégie open source à l'état de l'art !

GGI est une méthodologie permettant de déployer un programme d'amélioration continue, et à ce titre, n'a pas de fin. Quoi qu’il en soit, il est important de mettre en valeur ses étapes intermédiaires et apprécier les évolutions qu’elles engendrent, pour rendre le progrès perceptible et partager les résultats.

* Communiquez avec les parties prenantes et les utilisateurs pour leur faire connaître les avantages et bénéfices apportés par les efforts qu'impose l'initiative.
* Favorisez la durabilité du programme. Assurez-vous que les bonnes pratiques et les leçons acquises via le programme continuent à être appliquées et mises à jour.
* Partagez votre expérience avec vos pairs : fournissez un retour d'information au groupe de travail GGI et à votre communauté OSPO d'adoption, et partagez votre approche.
