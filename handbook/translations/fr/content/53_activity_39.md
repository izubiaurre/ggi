## Regarder vers l’amont (« Upstream first »)

Activity ID: [GGI-A-39](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_39.md).

### Description

Cette activité vise à faire prendre conscience des bénéfices à contribuer en retour, donc à instituer comme principe de regarder ou penser vers l’amont.

Dans cette optique, tout développement sur un projet open source doit être réalisé avec le niveau de qualité et d'ouverture requis pour être transmis aux développeurs principaux du projet, et rendu public par eux.

### Évaluation de l’opportunité

Écrire du code pensé pour une intégration amont a pour résultat :

- du code de meilleure qualité,
- du code prêt à être soumis au projet amont,
- du code fusionné au cœur du projet,
- du code qui sera compatible avec les versions futures,
- la reconnaissance par la communauté du projet, pour une coopération meilleure et plus profitable.

> Regarder vers l'amont est bien plus qu'être simplement « bienveillant ». Ça signifie avoir son mot à dire dans le projet. Ça signifie contrôler la situation. Ça signifie agir plutôt que réagir. Ça signifie que vous comprenez l'open source. ([Maximilian Michels](https://maximilianmichels.com/2021/upstream-first/))

### Suivi de l'avancement

Les **points de contrôle** suivants dénotent une progression dans cette activité : la vision amont est-elle réelle ?

- [ ] Un accroissement significatif du nombre de « pull/merge requests » soumises à des projets tiers.
- [ ] L'existence d'une liste de projets tiers pour lesquels une démarche de contribution pourrait être envisagée.

### Recommandations

- Identifier les développeurs les plus expérimentés en matière d'interaction avec les développeurs amont.
- Faciliter l'interaction entre vos développeurs et les développeurs principaux des projets ciblés (événements, hackathons, etc.)

### Ressources

- Une explication claire du principe « Upstream First » et en quoi il s'intègre à l'objectif « Culture » : <https://maximilianmichels.com/2021/upstream-first/>.

> « Upstream First » signifie qu'à chaque fois que vous résolvez dans votre copie locale un problème d'une manière pouvant profiter à d'autres, vous reversez vos évolutions au projet amont, par exemple en envoyant un patch ou en ouvrant une « pull request » dans le dépôt amont.

- [Que sont l'Amont et l'Aval dans le développement logiciel ?](https://reflectoring.io/upstream-downstream/) : une explication limpide.
- Explications via les documents de conception de Chromium OS : [Upstream First](https://www.chromium.org/chromium-os/chromiumos-design-docs/upstream-first).
- Red Hat, à propos des avantages de l’approche [upstream first](https://www.redhat.com/en/blog/what-open-source-upstream).
