## Gérer les compétences et les ressources en matière de développement de logiciels open source

Identifiant de l'activité : [GGI-A-42](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_42.md).

### Description

Cette activité est axée sur les compétences et les ressources de **développement de logiciels**. Elle comprend les technologies et les compétences de développement spécifiques des développeurs, ainsi que les processus, méthodes et outils de développement.

Une grande quantité de documentation, de forums et de discussions issus de l'écosystème, et de ressources publiques est disponible pour les technologies open source. Afin de bénéficier pleinement de leur approche open source, il faut établir une feuille de route de ses actifs actuels et des objectifs souhaités afin de mettre en place un programme cohérent pour les compétences, les méthodes et les outils de développement au sein des équipes.

#### Domaines d'application

Il faut établir les domaines dans lesquels le programme sera appliqué, et comment il améliorera la qualité et l'efficacité du code et des pratiques. Par exemple, le programme n'aura pas les mêmes avantages s'il n'y a qu'un seul développeur travaillant sur des composants open source, ou si l'ensemble du cycle de vie du développement est optimisé pour inclure les meilleures pratiques open source.

Il faut définir le champ d'application du développement open source : composants techniques, applications, modernisation ou création de nouveaux développements. Voici quelques exemples de pratiques de développement qui peuvent bénéficier de l'open source :

- Administration du Cloud.
- Applications cloud-natives, comment innover avec ces technologies.
- DevOps, intégration continue / livraison continue.

#### Catégories

- Compétences et ressources nécessaires au développement de logiciels open source : propriété intellectuelle, licences, pratiques.
- Compétences et ressources nécessaires pour développer des logiciels à l'aide de composants, de langages et de technologies open source.
- Compétences et ressources nécessaires pour utiliser les méthodes et processus de l'open source.

### Évaluation de l’opportunité

Les outils open source sont de plus en plus populaires parmi les développeurs. Cette Activité répond au besoin d'éviter la prolifération d'outils hétérogènes au sein d'une équipe de développement. Elle aide à définir une politique dans ce domaine. Elle permet d'optimiser la formation et l'acquisition d'expérience. Un inventaire des compétences est utilisé pour le recrutement, la formation et la planification de la succession au cas où un employé clé quitterait l'entreprise.

Nous aurions besoin d'une méthodologie pour cartographier les compétences en matière de développement de logiciels open source.

### Suivi de l'avancement

Les **points de contrôle** suivants dénotent une progression de cette Activité :

- [ ] Voici une description de la chaîne de production des logiciels libres (la "chaîne d'approvisionnement en logiciels"),
- [ ] Il existe un plan (ou une liste de souhaits) pour la rationalisation des ressources de développement,
- [ ] Il existe un inventaire des compétences qui résume les compétences, la formation et l'expérience des développeurs actuels,
- [ ] Il existe une liste de souhaits en matière de formation et un programme visant à combler les lacunes en matière de compétences,
- [ ] Il existe une liste des meilleures pratiques manquantes en matière de développement de logiciels libres et un plan pour les appliquer.

### Recommandations

- Commencez simplement, développez régulièrement l'analyse et la feuille de route.
- Lors du recrutement, mettez l'accent sur les compétences et l'expérience en matière de logiciels libres. Il est toujours plus facile de former et de coacher des personnes ayant déjà un ADN open source.
- Consultez les programmes de formation des fournisseurs de logiciels et des écoles de logiciels libres.

### Ressources

Plus d'informations :

- Une introduction à [ce qu'est un inventaire des compétences](https://managementisajourney.com/management-toolbox-better-decision-making-with-a-skills-inventory) de Robert Tanner.
- Un article sur les compétences en matière de logiciels libres : [5 compétences en matière d'open source pour améliorer votre jeu et votre CV](https://sourceforge.net/blog/5-open-source-skills-game-resume/)

Cette activité peut inclure des ressources et des compétences techniques telles que :

- **Langages populaires** (comme Java, PHP, Perl, Python).
- **Frameworks open source** (Spring, AngularJS, Symfony) et outils de test.
- Méthodes de développement Agile, DevOps et open source **et meilleures pratiques**.

Activités associées :

- [GGI-A-28 perspective RH](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_28.md)
