## Contribuire a progetti open source

Activity ID: [GGI-A-26](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_26.md).

### Descrizione

Contribuire a progetti open source di libero utilizzo è uno dei principi chiave della buona governance. Il punto è evitare di essere un semplice consumatore passivo e dare un contributo ai progetti. Quando le persone aggiungono una funzionalità o correggono un bug per i propri scopi, dovrebbero renderlo abbastanza generico da contribuire al progetto. Agli sviluppatori deve essere concesso il tempo necessario per contribuire.

Questa attività copre i seguenti ambiti:

- Lavorare con progetti open source upstream.
- Segnalazione di bug e richieste di funzionalità.
- Contribuire al codice e alla correzione di bug.
- Partecipare alle mailing list della community.
- Condividere le esperienze.

### Valutazione delle opportunità

I principali benefici di questa attività sono:

- Aumenta la conoscenza generale e l'impegno verso l'open source all'interno dell'azienda, in quanto le persone iniziano a contribuire e a essere coinvolte in progetti open source. Si percepisce un senso di utilità pubblica e si migliora la propria reputazione personale.
- L'azienda aumenta la propria visibilità e reputazione man mano che i contributi si fanno strada nel progetto. Questo dimostra che l'azienda è effettivamente coinvolta nell'open source, contribuisce e promuove correttezza e trasparenza.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa attività:

- [ ] Esiste un percorso chiaro e ufficiale per le persone disposte a contribuire.
- [ ] Gli sviluppatori sono incoraggiati a contribuire ai progetti open source che utilizzano.
- [ ] È in atto un processo per garantire la conformità legale e la sicurezza dei contributi da parte degli sviluppatori.
- KPI: Volume dei contributi esterni (codice, mailing list, issue...) da parte di individui, team o entità.

### Strumenti

Può essere utile seguire i contributi, sia per tenere traccia di ciò che viene apportato sia per poter comunicare l'impegno dell'azienda. A questo scopo si possono utilizzare dashboard e software di tracciamento delle attività. Controllare:

- [GrimoireLab](https://chaoss.github.io/grimoirelab/) di Bitergia
- [ScanCode](https://scancode-toolkit.readthedocs.io)

### Raccomandazioni

Incoraggiare le persone all'interno dell'entità a contribuire a progetti esterni:

- Permettere di scrivere correzioni di bug e funzionalità generiche e ben testate e di contribuire alla community.
- Fornire formazione alle persone sul contributo alle comunità open source. Questo riguarda sia le competenze tecniche (migliorare le conoscenze del team) che la community (appartenenza alle community open source, codice di condotta, ecc.).
- Fornire formazione su questioni legali, di proprietà intellettuale e tecniche e create un contatto all'interno dell'azienda che possa aiutarvi su questi temi in caso di dubbi.
- Fornire incentivi per i lavori pubblicati.
- Si noti che i contributi dell'azienda/ente rifletteranno la qualità del codice e il suo coinvolgimento, quindi assicuratevi che il vostro team di sviluppo fornisca codice sufficientemente buono.

### Risorse

- L'iniziativa [CHAOSS](https://chaoss.community/) della Linux Foundation offre alcuni strumenti e indicazioni su come tracciare i contributi allo sviluppo.
