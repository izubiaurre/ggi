## Crescita delle competenze open source

ID attività: [GGI-A-18](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_18.md).

### Descrizione

Questa attività riguarda la pianificazione e l'avvio delle competenze tecniche e delle prime esperienze con gli OSS, una volta effettuato l'inventario (#17). È anche l'occasione per iniziare a costruire una tabella di marcia semplificata per lo sviluppo delle competenze.

- Identificare le competenze e la formazione necessarie.
- Creare un progetto pilota per dare il via all'approccio, imparare "facendo", identificare una prima milestone.
- Capitalizzare le lezioni apprese e costruire un insieme di conoscenze.
- Iniziare a identificare e documentare i passi successivi per un'adozione più ampia.
- Elaborare una strategia nei prossimi mesi o un anno per ottenere il supporto finanziario e gestionale.

L'ambito dell'attività:

- Linux, Apache, Debian, competenze amministrative.
- Database open source MariaDB, MySQL, PostgreSQL, ecc.
- Virtualizzazione open source e tecnologie cloud.
- Lo stack LAMP e le sue alternative.

### Valutazione delle opportunità

Come ogni tecnologia IT, e probabilmente anche di più, l'open source porta innovazione. L'open source cresce velocemente e cambia rapidamente. Richiede alle organizzazioni di tenersi aggiornate.

Questa attività aiuta a identificare le aree in cui la formazione potrebbe aiutare le persone a diventare più efficienti e a sentirsi più sicure nell'uso dell'open source. Aiuta a prendere decisioni sullo sviluppo dei dipendenti. L'acquisizione di competenze di base sull'open source consente di valutare l'opportunità di:

- Estendere le soluzioni IT con le tecnologie di mercato esistenti sviluppate dall'ecosistema.
- Sviluppare nuove modalità di collaborazione all'interno e all'esterno dell'organizzazione.
- Acquisire competenze in tecnologie nuove e innovative.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa attività:

- [ ] Viene sviluppata una matrice di competenze.
- [ ] L'ambito delle tecnologie OSS utilizzate è definito in modo proattivo, evitando cioè l'uso incontrollato di tecnologie OSS.
- [ ] Per queste tecnologie viene acquisito un livello di competenza soddisfacente.
- [ ] I team hanno ricevuto una formazione "open source Basics" per iniziare.

### Strumenti

Uno strumento chiave in questo caso è la Matrice (o Mappatura) delle attività (o delle competenze).

Questa attività può essere svolta:

- utilizzando tutorial online (molti gratuitamente su Internet),
- partecipando a conferenze per sviluppatori,
- Attraverso la formazione professionale, ecc.

### Raccomandazioni

- L'utilizzo e lo sviluppo di componenti open source in modo sicuro ed efficiente richiede una mentalità aperta e collaborativa che deve essere riconosciuta e diffusa sia dall'alto (management) che dal basso (sviluppatori).
- Assicurarsi che l'approccio sia attivamente sostenuto e promosso dalla direzione. Non si otterrà nulla senza il coinvolgimento del management.
- Coinvolgere le persone (sviluppatori, stakeholder) nel processo: organizzare tavole rotonde e ascoltare le idee.
- Lasciate che le persone abbiano tempo e risorse per scoprire, provare e giocare con questi nuovi concetti. Se possibile, rendetelo divertente: la gamification e i premi sono ottimi incentivi.

Un progetto pilota con le seguenti fasi potrebbe fungere da catalizzatore:

- Identificare la tecnologia o il framework con cui iniziare.
- Trovate formazione online, esercitazioni e codice di esempio per sperimentare.
- Costruire un prototipo della soluzione finale.
- Identificate alcuni esperti per sfidare e allenare l'implementazione.

### Risorse

- [Cos'è una matrice di competenze](https://blog.kenjo.io/what-is-a-competency-matrix): una rapida lettura introduttiva.
- [Come creare una matrice di competenze per il vostro team](http://www.managersresourcehandbook.com/download/Skills-Matrix-Template.pdf): un modello con commenti.
- [MOOC sulla cultura libera (libre)](https://librecours.net/parcours/upload-lc000/) (solo in francese): si tratta di un corso in 6 parti sulla cultura libera, con introduzione ai diritti d'autore, alla proprietà intellettuale e alle licenze open source
