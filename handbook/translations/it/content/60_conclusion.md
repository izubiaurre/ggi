# Conclusioni

Come abbiamo già detto, la Good Governance dell'open source non è una destinazione, ma un viaggio. Dobbiamo prenderci cura dei nostri beni comuni, delle comunità e dell'ecosistema che li fanno prosperare, perché da questo dipende il nostro successo comune, e quindi individuale.

**Noi**, in qualità di professionisti del software e appassionati di open source, ci impegniamo a continuare a migliorare il manuale della Good Governance Initiative e a lavorare sulla sua diffusione e portata. Crediamo fermamente che le organizzazioni, gli individui e le comunità debbano lavorare fianco a fianco per costruire un insieme di beni comuni migliore e più ampio, disponibile e vantaggioso per tutti.

**Voi** siete invitati a unirvi alla OSPO Alliance, a contribuire al nostro lavoro, a spargere la voce e a farvi ambasciatori di una migliore consapevolezza e governance dell'open source all'interno del vostro ecosistema. Le risorse disponibili sono numerose, dai post sui blog agli articoli di ricerca, dalle conferenze ai corsi di formazione online. Anche noi mettiamo a disposizione una serie di materiali utili sul [nostro sito web](https://ospo-alliance.org), e siamo felici di poter dare il nostro contributo per quanto possibile.

**Definiamo e costruiamo insieme il futuro della Good Governance Initiative!**

## Contatti

Il modo migliore per entrare in contatto con la OSPO Alliance è quello di inviare un messaggio alla nostra mailing list pubblica all'indirizzo <https://accounts.eclipse.org/mailing-list/ospo.zone>. Potete anche venire a discutere con noi ai consueti eventi open source, partecipare ai nostri webinar mensili OSPO OnRamp o mettervi in contatto con un qualsiasi membro, che vi indirizzerà gentilmente alla persona giusta.

## Appendice: Modello di Customised Activity Scorecard

L'ultima versione del modello di Customised Activity Scorecard è disponibile nella sezione `resources` del [Good Governance Initiative GitLab](https://gitlab.ow2.org/ggi/ggi) di OW2.
