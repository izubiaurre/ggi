## Impegnarsi con i fornitori open source

Activity ID: [GGI-A-33](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_33.md).

### Descrizione

Assicuratevi i contratti con i fornitori di software open source che forniscono software di importanza cruciale per voi. Le aziende e le entità che producono software open source devono prosperare per garantire la manutenzione e lo sviluppo di nuove funzionalità. Le loro competenze specifiche sono necessarie per il progetto e la comunità di utenti fa affidamento sulla loro attività e sui loro contributi.

L'impegno con i fornitori open source assume diverse forme:

- Sottoscrivere piani di supporto.
- Appalto a società di servizi locali.
- Sponsorizzare gli sviluppi.
- Pagare per una licenza commerciale.

Questa attività implica la considerazione dei progetti open source come prodotti completi che valgono la pena di essere pagati, proprio come i prodotti proprietari, anche se di solito molto meno costosi.

### Valutazione delle opportunità

L'obiettivo di questa attività è garantire un supporto professionale al software open source utilizzato nell'organizzazione. Presenta diversi vantaggi:

- Continuità del servizio grazie alla tempestiva correzione dei bug.
- Prestazioni di servizio grazie all'ottimizzazione dell'installazione.
- Chiarimento dello status legale/commerciale del software utilizzato.
- Accesso alle nuove informazioni.
- Previsioni di bilancio stabili.

Il costo è ovviamente quello dei piani di supporto scelti. Un altro costo potrebbe essere quello di abbandonare l'outsourcing di massa a grandi integratori di sistemi a favore di contratti di precisione con PMI esperte.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa attività:

- [ ] L'open source utilizzato nell'organizzazione è sostenuto da un supporto commerciale.
- [ ] Sono stati stipulati piani di supporto per alcuni progetti open source.
- [ ] Il costo dei piani di supporto open source è una voce legittima del budget IT.

### Raccomandazioni

- Quando possibile, trovare PMI esperte a livello locale.
- Diffidate dei grandi integratori di sistemi che rivendono competenze di terzi (rivendendo piani di assistenza che in realtà sono forniti da PMI esperte di open source).

### Risorse

Un paio di link che illustrano la realtà commerciale del software open source:

- [Il punto di vista di un investitore sull'evoluzione della comunità verso il business dei progetti open source](https://a16z.com/2019/10/04/commercializing-open-source/).
- [Una lettura veloce per capire l'open source commerciale](https://www.webiny.com/blog/what-is-commercial-open-source).
