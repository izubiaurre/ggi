# Introduzione

Questo documento introduce una metodologia per implementare una gestione professionale del software open source in un'organizzazione. Il documento affronta la necessità di utilizzare il software open source in modo corretto ed equo, di salvaguardare l'azienda da minacce tecniche, legali e di proprietà intellettuale e di massimizzare i vantaggi dell'open source. Qualunque sia la posizione di un'organizzazione su questi temi, questo documento propone indicazioni e idee per andare avanti e rendere il vostro viaggio un successo.

## Contesto

La maggior parte degli utilizzatori finali e degli integratori di sistemi utilizza già codice libero e aperto (FOSS: Free e Open Source Software), sia nei propri sistemi informativi che all'interno delle proprie divisioni di prodotto o di servizi. La conformità al corretto utilizzo di software open source è una questione la cui importanza è cresciuta nel tempo, e molte grandi aziende hanno predisposto delle strutture dedicate alla conformità open source. Tuttavia, mentre è fondamentale impostare correttamente la filiera produttiva open source di un'azienda, attività che è poi proprio l'ambito di cui si occupa la conformità, *è necessario* che gli utilizzatori finali forniscano in cambio il proprio contributo alla sostenibilità dell'intero ecosistema open source. Noi consideriamo la buona governance dell'open source come un qualcosa che riguarda l'intero ecosistema, stabilendo legami con le comunità open source, promuovendo e alimentando una sana relazione con chi offre prodotti open source e servizi specialistici ad esso legati. Tutto questo porta la conformità ad un livello più elevato, e questo è ciò di cui si occupa la Good Governance Initiative.

Questa iniziativa va oltre le questioni di conformità e responsabilità. Essa riguarda la costruzione di una nuova consapevolezza nelle comunità di utilizzatori finali (spesso costituite esse stesse da sviluppatori) e integratori di sistemi, e riguarda lo sviluppo di relazioni mutuamente benefiche all'interno dell'ecosistema open source europeo.

Una Buona Governance dell'Open Source mette in grado organizzazioni di tutti i tipi (aziende piccole e grandi, amministrazioni civiche, università, associazioni, eccetera) di massimizzare i benefici derivanti dall'open source, aiutandole ad allineare persone, processi, tecnologie e strategie. E in quest'area di massimizzazione dei vantaggi del software open source, specialmente in Europa, chiunque può ancora imparare e innovare, senza che nessuno debba necessariamente sapere dove collocarsi riguardo allo stato dell'arte del settore.

Questa iniziativa ha l'obiettivo di aiutare le organizzazioni a raggiungere questi obiettivi tramite:

* Un catalogo strutturato di **attività**, una tabella di marcia per lo sviluppo di un approccio professionale alla gestione di software open source.
* Uno **strumento di gestione** per stabilire, controllare, riportare e comunicare tutte le informazioni relative ai progressi effettuati.
* Un **percorso chiaro e fattibile di progresso continuo**, eseguito a piccoli passi per ridurre i rischi, formare le persone, adattare i processi, comunicare internamente ed esternamente all'ambito organizzativo.
* **Linee guida** e un insieme di **riferimenti selezionati** circa licenze open source, buone pratiche, formazione e coinvolgimento nell'ecosistema per far leva su consapevolezza e cultura open source, consolidare la conoscenza interna ed estendere la direzione.

Questa guida è stata realizzata avendo in mente questi requisiti:

* Adottabilità del modello da parte di qualsiasi tipo di organizzazione: dalle piccole e medie imprese alle grandi aziende e alle organizzazioni senza scopo di lucro, dalle amministrazioni locali (ad esempio i Comuni) alle grandi istituzioni (ad esempio le istituzioni europee o i governi). Il modello fornisce gli elementi costitutivi per predisporre una strategia, assieme ai suggerimenti per la sua realizzazione, ma *come* le attività debbano essere svolte, dipende interamente dal contesto del programma di adozione e dal responsabile del programma stesso. Può risultare utile avvalersi di servizi di consulenza specifica e scambiare esperienze e pareri con altre organizzazioni che hanno intrapreso questo percorso.
* Non sono richiesti prerequisiti sul livello di conoscenza tecnica presente all'interno dell'organizzazione o dell'ambito di attività. Per esempio, alcune organizzazioni potranno avere bisogno di un percorso completo di formazione, mentre altre potranno semplicemente fornire del materiale ad hoc ai diversi gruppi.

Alcune attività potranno non essere adatte a tutte le situazioni, ma l'intero modello fornisce una tabella di marcia e apre la strada a strategie per un suo adattamento o personalizzazione al contesto specifico.

## Informazioni riguardo la Good Governance Initiative

In OW2, un'iniziativa rappresenta un impegno congiunto per soddisfare una specifica esigenza di mercato. La [Good Governance Initiative](https://www.ow2.org/view/OSS_Governance) propone un modello metodologico per sviluppare all'interno delle organizzazioni una modalità di gestione professionale dell'open source.

La Good Governance Initiative è basata su un modello esaustivo ispirato dalla gerarchia dei bisogni e delle motivazioni umane di Abraham Maslow, rappresentata nella figura seguente.

![Relazione tra gerarchia di Maslow e modello della Good Governance Initiative](resources/images/ggi_maslow.png)

Attraverso idee, linee guida e attività, la Good Governance Initiative fornisce una specifica per la predisposizione di unità organizzative incaricate di gestire professionalmente il software open source. Queste unità organizzative sono talvolta indicate anche come OSPO, una sigla che sta per Open Source Program Office (un ufficio amministrativo espressamente dedicato alla gestione dell'open source). La metodologia è anche un sistema per definire le priorità, tenere sotto controllo e condividere i progressi dell'adozione del modello.

Man mano che progrediranno nell'adozione della metodologia GGI, le organizzazioni vedranno crescere le loro competenze in diversi ambiti, tra cui:

* l'**utilizzo** appropriato e sicuro del software open source all'interno dell'organizzazione, con il miglioramento del riuso, della manutenibilità e della rapidità nello sviluppo del software;
* la **mitigazione** dei rischi legali e tecnici associati all'utilizzo di codice e collaborazioni esterne all'organizzazione;
* l'**identificazione** della formazione necessaria ai diversi gruppi, dai singoli sviluppatori ai responsabili e ai dirigenti, in modo che tutti condividano la stessa visione;
* la capacità di stabilire **priorità** di obiettivi e attività, per sviluppare una strategia efficiente riguardo al software open source;
* la capacità di **comunicare** in maniera efficiente all'interno dell'organizzazione e verso l'esterno per sfruttare al meglio la strategia open source;
* il **miglioramento** della competitività dell'organizzazione e la sua capacità di attrarre i migliori talenti open source.

## Informazioni riguardo la OSPO Alliance

La **OSPO Alliance** è stata avviata da una coalizione delle principali organizzazioni europee senza scopo di lucro, tra cui OW2, Eclipse Foundation, OpenForum Europe, e la Foundation for Public Code, avendo quale missione quella di far crescere la consapevolezza dell'open source in Europa e nel mondo, e di promuovere la gestione strutturata e professionale dell'open source da parte di aziende e amministrazioni.

Laddove la Good Governance Initiative si concentra sullo sviluppo di una metodologia di gestione, la OSPO Alliance ha l'obiettivo di più ampio respiro di aiutare le aziende, particolarmente nei settori non tecnologici, e le istituzioni pubbliche di conoscere e comprendere l'open source, iniziando a trarne beneficio nelle loro attività e a costituire al loro interno uffici amministrativi dedicati alla gestione dell'open source (OSPO: Open Source Program Office).

La OSPO Alliance ha predisposto il sito ufficiale **OSPO Alliance**, raggiungibile all'indirizzo <https://ospo-alliance.org>. La OSPO Alliance è al servizio della comunità offrendo un contesto affidabile per discutere e scambiare informazioni relative agli uffici per la gestione dell'open source (OSPO), e mettendo a disposizione un archivio contenente un insieme esaustivo di risorse per società, istituzioni pubbliche, organizzazioni accademiche e di ricerca. La OSPO Alliance consente di mettere in collegamento tutti i diversi uffici per la gestione dell'open source (OSPO) in Europa e nel mondo, così come quelle organizzazioni che a loro volta supportano le comunità. Incoraggia le migliori pratiche e promuove i contributi dedicati alla sostenibilità dell'open source. Consulta il sito [OSPO Alliance](https://ospo-alliance.org) per una veloce panoramica dei principali modelli relativi alle buone pratiche della gestione IT.

Il sito della [OSPO Alliance](https://ospo-alliance.org) è anche lo spazio in cui raccogliamo i riscontri riguardanti l'iniziativa e i suoi contenuti (ad esempio le attività, il corpus dei materiali) provenienti in generale dalla comunità.

## Traduzioni

La traduzione del Manuale della Good Governance Initiative è un lavoro continuo portato avanti dalla comunità. Poiché il lavoro di traduzione è in continua evoluzione, raccomandiamo di consultare il nostro sito ufficiale per una lista aggiornata delle traduzioni disponibili.

> Consulta la sezione <https://hosted.weblate.org/projects/ospo-zone-ggi/#languages>

La traduzione del Manuale della Open Source Good Governance Initiative è effettuata facendo uso di [Weblate](https://hosted.weblate.org/), un progetto e una piattaforma open source che permette di ospitare gratuitamente progetti open source di traduzione. Desideriamo ringraziarli vivamente, così come desideriamo ringraziare tutti colore che contribuiscono alla traduzione. Siete meravigliosi.

## Contributori

Queste persone straordinarie hanno contribuito alla Good Governance Initiative:

* Frédéric Aatz (Microsoft France)
* Boris Baldassari (Castalia Solutions, Eclipse Foundation)
* Philippe Bareille (Città di Parigi)
* Gaël Blondelle (Eclipse Foundation)
* Vicky Brasseur (Wipro)
* Philippe Carré (Nokia)
* Pierre-Yves Gibello (OW2)
* Michael Jaeger (Siemens)
* Sébastien Lejeune (Thales)
* Max Mehl (Free Software Foundation Europe)
* Hervé Pacault (Orange)
* Stefano Pampaloni (RIOS)
* Christian Paterson (OpenUp)
* Simon Phipps (Meshed Insights)
* Silvério Santos (Orange Business Services)
* Cédric Thomas (OW2)
* Nicolas Toussaint (Orange Business Services)

## Licenza

Questo documento è rilasciato con licenza [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0). Dal sito Creative Commons:

> Hai la libertà di:
>
> * Condividerlo - copiare e redistribuire il materiale su qualunque supporto e in qualunque formato
> * Adattarlo - usare il materiale per mischiare, trasformare e costruire nuovi contenuti a partire da esso
>
> per qualsiasi scopo, anche commerciale.
>
> Nel fornire la corretta paternità del contenuto, fornisci un collegamento alla licenza, indicando le eventuali modifiche che hai apportato. Puoi farlo in qualunque ragionevole maniera, purché questo non dia l'idea che il licenziante approvi te o il tuo utilizzo.

Tutti i diritti riservati: 2020-2022 OW2 e i partecipanti alla Open Source Good Governance Initiative.
