## Eseguire revisioni del codice

Activity ID: [GGI-A-44](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_44.md).

### Descrizione

La revisione del codice è un'attività di routine che prevede la revisione manuale e/o automatizzata del codice sorgente di un'applicazione prima di rilasciare un prodotto o consegnare un progetto al cliente. Nel caso del software open-source, la revisione del codice non si limita a individuare opportunamente gli errori, ma è un approccio integrato allo sviluppo collaborativo condotto a livello di team.

La revisione del codice dovrebbe riguardare sia il codice sviluppato internamente sia quello riutilizzato da fonti esterne, in quanto migliora la fiducia generale nel codice e ne rafforza la proprietà. È anche un modo eccellente per migliorare le competenze e le conoscenze globali all'interno del team e per promuovere la collaborazione tra i membri.

### Valutazione delle opportunità

Le revisioni del codice sono preziose ogni volta che l'organizzazione sviluppa software o riutilizza software esterni. Pur essendo una fase standard del processo di ingegneria del software, le revisioni del codice nel contesto dell'open-source apportano vantaggi specifici quali:

- Quando si pubblica il codice sorgente interno, permette di verificare che vengano rispettate le linee guida sulla qualità.
- Quando si contribuisce a un progetto open source esistente, permette di verificare che le linee guida del progetto in questione siano rispettate.
- La documentazione pubblica viene aggiornata di conseguenza.

È anche un'ottima occasione per condividere e far rispettare alcune regole di conformità legale della vostra azienda, come ad esempio:

- Non rimuovete mai le intestazioni di licenza o i diritti d'autore esistenti nel codice open source riutilizzato.
- Non copiare e incollare il codice sorgente da Stack Overflow senza previa autorizzazione del team legale.
- Includere la linea di copyright corretta quando richiesto.

Le revisioni del codice garantiscono la fiducia e la sicurezza del codice. Se le persone non sono sicure della qualità o dei rischi potenziali dell'uso di un prodotto software, dovrebbero effettuare revisioni del codice e raccogliere feedback dai colleghi.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa attività:

- [ ] La revisione del codice open source è riconosciuta come un passo necessario.
- [ ] Sono previste revisioni del codice open source (regolarmente o in momenti critici).
- [ ] È stato definito e accettato collettivamente un processo di revisione del codice open-source.
- [ ] Le revisioni del codice open-source sono una parte standard del processo di sviluppo.

### Raccomandazioni

- La revisione del codice è un compito collettivo che funziona meglio in un buon ambiente collaborativo.
- Non esitate a utilizzare gli strumenti e i modelli esistenti nel mondo open-source, dove la revisione del codice è uno standard da anni (decenni).

### Risorse

- [What is Code Review?](https://openpracticelibrary.com/practice/code-review/): una lettura didattica sulla revisione del codice trovata sulla Open Practice Library di Red Hat.
- [Best Practices for Code Reviews](https://www.perforce.com/blog/qac/9-best-practices-for-code-review): un'altra prospettiva interessante su cosa sia la revisione del codice.
