## Gestire le dipendenze del software

Activity ID: [GGI-A-23](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_23.md).

### Descrizione

Un programma di *identificazione delle dipendenze * cerca le dipendenze effettivamente utilizzate all'interno del codice base. Di conseguenza, l'organizzazione deve stabilire e mantenere un elenco di dipendenze note e osservare l'evoluzione dei fornitori identificati.

Stabilire e mantenere un elenco di dipendenze conosciute è un fattore abilitante e un prerequisito per:

- Controllo della proprietà intellettuale e delle licenze: alcune licenze non possono essere mescolate, nemmeno come dipendenza. È necessario conoscere le dipendenze per valutare i rischi legali associati.
- Gestione delle vulnerabilità: l'intero software è debole quanto la sua parte più debole: si veda l'esempio della falla [Heartbleed flaw](https://it.wikipedia.org/wiki/Heartbleed). È necessario conoscere le sue dipendenze per valutare i rischi di sicurezza associati.
- Ciclo di vita e sostenibilità: una comunità attiva sul progetto di dipendenza è un segnale positivo per la correzione di bug, ottimizzazioni e nuove funzionalità.
- Selezione ponderata delle dipendenze utilizzate, in base a criteri di "maturità": l'obiettivo è utilizzare componenti open source sicuri, con una base di codice sana e ben mantenuta, una comunità viva, attiva e reattiva che accetti contributi esterni, ecc.

### Valutazione delle opportunità

L'identificazione e la tracciabilità delle dipendenze è una fase necessaria per ridurre i rischi associati al riutilizzo del codice. Inoltre, l'implementazione di strumenti e processi per gestire le dipendenze del software è un prerequisito per gestire correttamente la qualità, la conformità e la sicurezza.

Considerate le seguenti domande:

- Qual è il rischio dell'azienda (costo, reputazione, ecc.) se il software viene danneggiato, attaccato o citato in giudizio?
- La codebase è considerata critica per le persone, l'organizzazione o l'azienda?
- Cosa succede se un componente da cui dipende un'applicazione cambia il suo repository?

Il primo passo consiste nell'implementare uno strumento di analisi della composizione del software (software composition analysis, SCA). Per una SCA completa o una mappatura delle dipendenze può essere necessario il supporto di società di consulenza specializzate.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa attività:

- [ ] Le dipendenze sono identificate in tutto il codice sviluppato internamente.
- [ ] Le dipendenze vengono identificate in tutto il codice esterno eseguito all'interno dell'azienda.
- [ ] È disponibile un'analisi della composizione del software o una procedura di identificazione delle dipendenze facile da impostare, che i progetti possono aggiungere al loro processo di integrazione continua.
- [ ] Vengono utilizzati strumenti di analisi delle dipendenze.

### Strumenti

- [OWASP Dependency check](https://github.com/jeremylong/DependencyCheck): dependency-Check è uno strumento di analisi della composizione del software (SCA) che tenta di rilevare le vulnerabilità pubblicamente divulgate contenute nelle dipendenze di un progetto.
- [OSS Review Toolkit](https://oss-review-toolkit.org/): una suite di strumenti per la revisione delle dipendenze del software open source.
- [Fossa](https://github.com/fossas/fossa-cli): analisi delle dipendenze veloce, portatile e affidabile. Supporta la scansione delle licenze e delle vulnerabilità. È indipendente dal linguaggio; si integra con oltre 20 sistemi di compilazione.
- [Software 360](https://projects.eclipse.org/projects/technology.sw360).
- [Eclipse Dash license tool](https://github.com/eclipse/dash-licenses): prende un elenco di dipendenze e chiede a [ClearlyDefined](https://clearlydefined.io) di controllare le loro licenze.
- [Il progetto FOSSology](https://www.fossology.org/): FOSSology è un progetto open source con la missione di promuovere la conformità alle licenze open source.

### Raccomandazioni

- Condurre audit regolari sulle dipendenze e sui requisiti di proprietà intellettuale per ridurre i rischi legali.
- L'ideale è integrare la gestione delle dipendenze nel processo di integrazione continua, in modo che i problemi (nuove dipendenze, incompatibilità di licenze) vengano identificati e risolti il prima possibile.
- Tenere traccia delle vulnerabilità legate alle dipendenze, tenere informati utenti e sviluppatori.
- Informare le persone sui rischi associati a licenze errate.
- Proporre una soluzione semplice per i progetti per impostare il controllo delle licenze sul loro codice.
- Comunicare la sua importanza e aiutare i progetti ad aggiungerlo ai loro sistemi di CI.
- Impostare un KPI visibile per i rischi legati alle dipendenze.

### Risorse

- Pagina del gruppo [Strumenti di conformità alle licenze OSS](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence-Compliance-Tools/) esistente.
- [[Free and Open Source Software licence Compliance: Tools for Software Composition Analysis](https://www.computer.org/csdl/magazine/co/2020/10/09206429/1npxG2VFQSk), di Philippe Ombredanne, nexB Inc.
- [Software Sustainability Maturity Model](http://oss-watch.ac.uk/resources/ssmm).
- [CHAOS](https://chaoss.community/): Community Health Analytics Open Source Software.
