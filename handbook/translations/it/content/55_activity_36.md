## L'open source che consente l'innovazione

Activity ID: [GGI-A-36](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_36.md).

### Descrizione

> L'innovazione è l'attuazione pratica di idee che portano all'introduzione di nuovi beni o servizi o al miglioramento dell'offerta di beni o servizi.
>
> &mdash; <cite>Schumpeter, Joseph A.</cite>

L'open source può essere un fattore chiave per l'innovazione grazie alla diversità, alla collaborazione e allo scambio fluente di idee. Persone con background e ambiti diversi possono avere prospettive diverse e fornire risposte nuove, migliori o addirittura dirompenti a problemi noti. Si può favorire l'innovazione ascoltando i diversi punti di vista e promuovendo attivamente una collaborazione aperta su progetti e argomenti.

Allo stesso modo, partecipare all'elaborazione e all'implementazione di standard aperti è un grande promotore di buone pratiche e idee per migliorare il lavoro quotidiano dell'azienda. Inoltre, consente all'azienda di guidare e influenzare l'innovazione identificando cosa le serve e in quale fase, aumentando la sua visibilità e reputazione a livello globale.

Attraverso l'innovazione, l'open source permette non solo di trasformare i beni o i servizi che la vostra azienda commercializza, ma anche di creare o modificare l'intero ecosistema in cui la vostra azienda vuole prosperare.

Ad esempio, rilasciando Android come open source, Google invita centinaia di migliaia di aziende a creare i propri servizi basati su questa tecnologia open source. Google sta quindi creando un intero ecosistema di cui tutti i partecipanti possono beneficiare. Naturalmente, poche aziende sono abbastanza potenti da creare un ecosistema con la propria decisione. Ma ci sono molti esempi di alleanze tra aziende per creare un tale ecosistema.

### Valutazione delle opportunità

È importante valutare la posizione della vostra azienda rispetto ai concorrenti, ai partner e ai clienti, perché spesso sarebbe rischioso per un'azienda allontanarsi troppo dagli standard e dalle tecnologie utilizzate da clienti, partner e concorrenti. Innovare significa ovviamente essere diversi, ma ciò che si differenzia non deve avere una portata troppo ampia; in caso contrario, l'azienda non potrebbe beneficiare degli sviluppi software realizzati dalle altre aziende dell'ecosistema e dello slancio commerciale che l'ecosistema offre.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa attività:

- [ ] Sono state identificate le tecnologie - e le comunità open source che le sviluppano - che hanno un impatto sull'azienda.
- [ ] I progressi e le pubblicazioni di queste comunità open source sono monitorati - sono persino a conoscenza della loro strategia prima che i rilasci siano resi pubblici.
- [ ] I dipendenti dell'organizzazione sono membri di (alcune) di queste comunità open source e ne influenzano le roadmap e le scelte tecniche contribuendo con linee di codice e partecipando agli organi di governance di queste comunità.

### Raccomandazioni

Tra tutte le tecnologie necessarie per la gestione della vostra azienda, dovreste individuarne alcune:

- le tecnologie che potrebbero essere le stesse dei vostri concorrenti,
- le tecnologie che devono essere specifiche per la vostra azienda.

Rimanere aggiornati sulle tecnologie emergenti. L'open source ha guidato l'innovazione nell'ultimo decennio e molti strumenti potenti di giorno in giorno provengono da lì (si pensi a Docker, Kubernetes, ai progetti Apache Big Data o a Linux). Non è necessario sapere tutto su tutto, ma si dovrebbe conoscere abbastanza lo stato dell'arte per identificare nuove tendenze interessanti.

Consentite e incoraggiate le persone a presentare idee innovative e a portarle avanti. Se possibile, destinare risorse a queste iniziative e farle crescere. Affidatevi alla passione e alla volontà delle persone di creare e promuovere idee e tendenze emergenti.

### Risorse

- [4 innovazioni che dobbiamo all'open source](https://www.techrepublic.com/article/4-innovations-we-owe-to-open-source/).
- [The Innovations of Open Source](https://dirkriehle.com/publications/2019-selected/the-innovations-of-open-source/), del professor Dirk Riehle.
- [Open source technology, enabling innovation](https://www.raconteur.net/technology/cloud/open-source-technology/).
- [Can Open Source Innovation Work in the Enterprise?](https://www.threefivetwo.com/blog/can-open-source-innovation-work-in-the-enterprise).
- [Europe: Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).
- [Europe: Open source software strategy 2020-2023](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
