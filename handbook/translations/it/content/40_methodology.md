# Metodologia

L'implementazione della metodologia di Good Governance OSS è in definitiva un'iniziativa di forte impatto. Coinvolge diverse categorie di persone, servizi e processi aziendali, dalle pratiche quotidiane alla gestione delle risorse umane, dagli sviluppatori ai dirigenti di alto livello. Per implementare una buona governance dell'open source non esiste un meccanismo universale. Diversi tipi di organizzazione, diverse culture e situazioni aziendali richiederanno approcci diversi alla governance dell'open source. Per ogni organizzazione ci saranno vincoli e aspettative diverse, che porteranno a percorsi e modi diversi di gestione del programma.

In queste circostanze, la metodologia GGI fornisce un progetto generico che può essere adattato al settore, alla cultura e alle esigenze specifiche di un'organizzazione. Sebbene il progetto sia inteso come completo e autosufficiente, la metodologia può essere implementata progressivamente. È possibile avviare il programma selezionando gli obiettivi e le attività ritenuti rilevanti in un contesto specifico: l'idea è quella di costruire una prima bozza di roadmap, che aiuti a dare il via all'iniziativa.

Oltre a questo quadro di riferimento, consigliamo vivamente di entrare in contatto con i colleghi attraverso una rete consolidata come l'iniziativa europea [OSPO Alliance](https://ospo-alliance.org), o altre iniziative simili del gruppo TODO o OSPO++. L'importante è poter scambiare feedback con persone che gestiscono un'iniziativa simile e condividere i problemi incontrati e le soluzioni esistenti.

## Preparare il terreno

Data l'ambizione della metodologia GGI e il suo impatto potenzialmente ampio, è importante comunicare con diverse persone all'interno di un'organizzazione. Sarebbe opportuno coinvolgerli per stabilire una serie iniziale di aspettative e requisiti realistici per iniziare con il piede giusto, attirare l'interesse e il sostegno. Una buona idea è quella di pubblicare le Customised Activity Scorecards (CAS) sulla piattaforma collaborativa dell'organizzazione, in modo che possano essere utilizzate per comunicare con le parti interessate. Alcuni suggerimenti:

* Identificate le principali parti interessate e fate in modo che si accordino su una serie di obiettivi primari. Coinvolgeteli nel successo dell'iniziativa come parte del loro programma.
* Ottenete il consenso iniziale, concordate le fasi e il ritmo e stabilite controlli regolari per informarli dei progressi.
* Assicuratevi che comprendano i vantaggi di ciò che si può ottenere e di ciò che comporta: i miglioramenti attesi devono essere chiari e i risultati visibili.
* Stabilite una prima diagnosi o lo stato dell'arte dell'open source nell'organizzazione candidata. Il risultato: un documento che descrive i risultati di questo programma, la situazione dell'organizzazione e la direzione che intende prendere.

## Workflow

Come moderni professionisti del software, ci piacciono i metodi agili che definiscono miglioramenti piccoli e sicuri, poiché è buona norma rivalutare regolarmente la situazione e fornire risultati intermedi significativi.

Questo è molto importante nel contesto di un programma OSPO in corso, poiché molti aspetti cambieranno nel tempo, dalla strategia e dalla risposta dell'organizzazione all'open source alla disponibilità e all'impegno delle persone. La rivalutazione e l'iterazione periodica consentono inoltre di adattarsi all'accettazione del programma in corso, di monitorare meglio le tendenze e le opportunità attuali e di ottenere piccoli vantaggi incrementali per gli stakeholder e per l'organizzazione nel suo complesso.

Idealmente, la metodologia potrebbe essere implementata in cinque fasi come segue:

1. **Discovery** Comprendere i concetti chiave, appropriarsi della metodologia, allineare gli obiettivi e le aspettative.
1. **Customisation** Adattare la descrizione dell'attività e la valutazione delle opportunità alle specificità dell'organizzazione.
1. **Prioritisation** Identificare gli obiettivi e i risultati chiave, i compiti e gli strumenti, programmare le tappe e redigere la tempistica.
1. **Activation** Finalizzazione di Scorecard, budget, incarichi, documentazione dei compiti su issue manager.
1. **Iteration** Valutare e raccogliere i risultati, evidenziare i problemi, migliorare, aggiustare. Iterazione ogni trimestre o semestre.

Preparazione della prima iterazione del programma:

* Identificare una prima serie di compiti su cui lavorare e stabilire una priorità in base alle esigenze (i passaggi che separano dallo stato desiderato) e alla tempistica. Risultato: un elenco di compiti su cui lavorare durante l'iterazione.
* Definire una serie di requisiti e aree di miglioramento, comunicarli alle parti interessate e agli utenti finali e ottenere la loro approvazione o il loro impegno.
* Compilare le schede di valutazione per monitorare i progressi. Un modello di scorecard può essere scaricato dal [repository GGI](https://gitlab.ow2.org/ggi/ggi/-/tree/main/resources/).

Alla fine di ogni iterazione, fate una retrospettiva e preparatevi per l'iterazione successiva:

* Comunicare gli ultimi miglioramenti.
* Valutate a che punto siete, se i compiti mirati sono stati completati, perfezionate la roadmap di conseguenza.
* Verificate i punti dolenti o i problemi rimanenti, chiedete il supporto di altri responsabili o servizi se necessario.
* Ridefinire le priorità dei compiti in base al contesto aggiornato.
* Definire un nuovo sottoinsieme di compiti da eseguire.

## Setup manuale: utilizzo delle CAS (Customised Activity Scorecards)

Una CAS è un modulo che descrive una Canonical Activity personalizzata in base alle specificità di un'organizzazione. L'insieme delle schede di valutazione delle attività personalizzate fornisce la tabella di marcia per la gestione del software open source.

`Si noti che, in base alle prime esperienze con la metodologia, è necessaria fino a un'ora per adattare una Canonical Activity alla relativa Score Card personalizzata di un'organizzazione.`

Le Customised Activity Scorecard contengono le seguenti sezioni:

* **Title Disambiguation** Prima di tutto, prendete qualche minuto per capire di cosa potrebbe trattarsi l'attività e la sua rilevanza, e come può inserirsi nel vostro percorso complessivo di gestione degli OSS.
* **Customised Description** Adattare l'attività alle specificità dell'organizzazione. Definite l'ambito dell'attività, il caso d'uso specifico che affronterete.
* **Opportunity Assessment** Spiegate perché è importante intraprendere questa attività, quali esigenze soddisfa. Quali sono i nostri punti dolenti? Quali sono le opportunità di progresso? Cosa si può ottenere?
* **Objectives** Definire un paio di obiettivi cruciali per l'attività. Punti dolenti da risolvere, opportunità di progresso, desideri. Identificare i compiti chiave. Ciò che si vuole ottenere in questa iterazione.
* **Tools** Tecnologie, strumenti e prodotti utilizzati nelle Activity.
* **Operational Notes** Indicazioni sull'approccio, il metodo e la strategia da seguire in questa Activity.
* **Key Results** Definire risultati attesi misurabili e verificabili. Scegliere i risultati che indicano i progressi rispetto agli Obiettivi. Indicare qui i KPI.
* **Progress and Score** Progress è, in %, la percentuale di completamento del risultato; Score è la valutazione del successo personale.
* **Personal Assessment** Per ogni risultato è possibile aggiungere una breve spiegazione e spiegare il proprio grado di soddisfazione personale espresso nel punteggio.
* **Timeline** Indicare le date di inizio-fine (Start-End dates), le fasi di lavoro (Phasing tasks), i passaggi critici (Critical steps), le tappe (Milestones).
* **Efforts** Valutare le risorse di tempo e materiali richieste, interne e di terzi. Quali sono gli sforzi previsti? Quanto costerà? Di quali risorse abbiamo bisogno?
* **Assignees** Dire chi partecipa. Assegnate i compiti o la leadership e le responsabilità dell'Activity.
* **Issues** Identificare i problemi chiave, le difficoltà previste, i rischi, gli ostacoli, le incertezze, i punti di attenzione, le dipendenze critiche.
* **Status** Scrivere qui una valutazione sintetica di come sta l'attività: in salute? In ritardo? ecc.
* **Overall Progress Rating** La vostra valutazione sintetica dei progressi dell'Activity, di alto livello e orientata alla gestione.

## Configurazione automatica: utilizzo della funzione GGI Deployment

A partire dalla versione Handbook 1.1, il GGI propone [My GGI Board](https://gitlab.ow2.org/ggi/my-ggi-board), uno strumento automatizzato per distribuire la propria istanza del GGI come progetto GitLab. Il processo di installazione richiede meno di 10 minuti, è completamente documentato e fornisce un modo semplice e affidabile per personalizzare le attività, seguirne l'esecuzione man mano che si compiono progressi e comunicare i risultati alle parti interessate. Un esempio dal vivo della distribuzione può essere visto nella [initative's GitLab](https://gitlab.ow2.org/ggi/my-ggi-board-test), con il sito web generato automaticamente disponibile sulle [relative GitLab Pages](https://ggi.ow2.io/my-ggi-board-test/).

![Attività di deployment GGI](resources/images/ggi_deploy_activities.png)

Ecco un flusso di lavoro standard per utilizzare la funzione di deployment:

1. Effettuare un fork della scheda My GGI alla propria istanza o progetto GitLab e configurarla seguendo le istruzioni contenute nel README del progetto: <https://gitlab.ow2.org/ggi/my-ggi-board>. In questo modo:

- Creare tutte le attività come issue nel progetto.
- Creare una tabella dall'aspetto accurato per aiutarvi a visualizzare e gestire le attività.
- Creare un sito web statico, servito dalle pagine dell'istanza GitLab, con le informazioni estratte dalle attività.
- Aggiornare la descrizione del progetto con i link corretti che puntano alla board delle attività e al vostro sito web statico.

1. Da qui si può iniziare a esaminare le diverse attività e a compilare la sezione della scorecard.

- La sezione delle schede di valutazione è l'equivalente elettronico (e semplificato) delle schede di valutazione ODT di cui sopra. Si usa per adattare l'attività al proprio contesto, elencando le risorse locali, i rischi e le opportunità e definendo gli obiettivi personalizzati necessari per completare l'attività.
- Se un'attività non si applica al vostro contesto, contrassegnatela semplicemente come "Not Selected" o chiudetela.
- Si tratta di un processo che richiede molto tempo, ma che è assolutamente necessario perché vi aiuterà, passo dopo passo, a definire la vostra tabella di marcia e il vostro piano.

1. Una volta definite le attività, è possibile iniziare a implementare il proprio OSPO. Selezionate alcune attività che ritenete rilevanti per iniziare e modificate la loro etichetta di avanzamento da "Not Started" a "In Progress". Potete utilizzare le funzioni di GitLab per organizzare il lavoro (commenti, assegnatari, ecc.) o qualsiasi altro strumento. È facile collegare le attività e sono disponibili molte integrazioni.
1. A intervalli regolari (settimanali, mensili, a seconda del vostro calendario), valutate e rivedete le attività in corso e, una volta completate, cambiate l'etichetta da "In Progress" a "Done". Selezionatene altre e ricominciate dal punto 3 finché non saranno tutte completate.

Il sito web propone una rapida panoramica delle attività attuali e passate ed estrae la sezione scorecard dei temi per visualizzare solo le informazioni rilevanti a livello locale. Quando si verificano cambiamenti nelle issue (attività), questi vengono aggiornati automaticamente nel sito web generato. Si noti che le pipeline CI per la generazione automatica del sito web vengono eseguite automaticamente di notte, ma è possibile lanciarle facilmente dalla sezione CI/CD del progetto GitLab. L'immagine seguente mostra l'interfaccia del sito web generato automaticamente.

![Sito web di GGI](resources/images/ggi_deploy_website.png)

È possibile porre domande o ottenere supporto per la funzione di distribuzione sulla nostra homepage di GitLab e siamo lieti di ricevere feedback.

> Homepage di GGI Deploy: <https://gitlab.ow2.org/ggi/my-ggi-board>

## Buon proseguimento

Comunicate il vostro successo e godetevi la tranquillità che deriva da una strategia open source all'avanguardia!

L'OSS Good Governance è un metodo per attuare un programma di miglioramento continuo, e come tale non finisce mai. Tuttavia, è importante evidenziare le fasi intermedie e apprezzare i cambiamenti che produce, per rendere visibili i progressi e condividere i risultati.

* Comunicare con gli stakeholder e gli utenti finali per far conoscere loro i vantaggi e i benefici che lo sforzo dell'iniziativa comporta.
* Promuovere la sostenibilità del programma. Assicurare che le migliori pratiche e le lezioni apprese dal programma siano sempre applicate e aggiornate.
* Condividete la vostra esperienza con i vostri colleghi: fornite un feedback al gruppo di lavoro GGI e alla vostra comunità di adozione OSPO e condividete il vostro approccio.
