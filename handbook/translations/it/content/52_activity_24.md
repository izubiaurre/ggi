## Gestire gli indicatori chiave

Activity ID: [GGI-A-24](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_24.md).

### Descrizione

Questa attività raccoglie e monitora una serie di indicatori che supportano le decisioni manageriali quotidiane e le opzioni strategiche relative al software open source gestito professionalmente.

Le metriche chiave relative al software open source costituiscono lo sfondo del buon andamento dei programmi di governance. L'attività prevede la selezione di alcuni indicatori, la loro pubblicazione ai team e alla direzione e l'invio di aggiornamenti regolari sull'iniziativa, ad esempio tramite una newsletter o un notiziario aziendale.

Questa attività richiede:

- presenza degli stakeholder per discutere e definire gli obiettivi del programma,
- l'implementazione di uno strumento di misurazione e raccolta dati connesso all'infrastruttura di sviluppo,
- la pubblicazione di almeno una dashboard per gli stakeholder e per tutte le persone coinvolte nell'iniziativa.

Gli indicatori si basano su dati che devono essere raccolti da fonti pertinenti. Fortunatamente, ci sono molte fonti per l'ingegneria del software open source. Alcuni esempi sono:

- l'ambiente di sviluppo, la catena di produzione CI/CD,
- il dipartimento Risorse Umane,
- gli strumenti di test e di analisi della composizione del software,
- i repository.

Esempi di indicatori sono:

- Numero di dipendenze risolte, visualizzate per tipo di licenza.
- Numero di dipendenze obsolete/vulnerabili.
- Numero di problemi di licenza/ip rilevati.
- Contributi a progetti esterni.
- Bug open time.
- Numero di collaboratori di un componente, numero di commit, ecc.

Questa attività consiste nel definire i requisiti e le esigenze di misurazione e nell'implementare un cruscotto che mostri in modo semplice ed efficace i principali indicatori del programma.

### Valutazione delle opportunità

Gli indicatori chiave aiutano a capire e a gestire meglio le risorse dedicate al software open source e a misurare i risultati per comunicare in modo efficace e raccogliere tutti i benefici dell'investimento. Grazie a un'ampia comunicazione, un maggior numero di persone può seguire l'iniziativa e sentirsi coinvolto, il che, in ultima analisi, la rende un obiettivo e una priorità dell'organizzazione.

Sebbene ogni attività abbia dei criteri di valutazione che aiutano a rispondere alle domande sui progressi raggiunti, c'è ancora bisogno di un monitoraggio fatto di numeri e indicatori quantitativi.

Che si tratti di una piccola startup o di una grande azienda globale, le metriche chiave aiutano a mantenere i team concentrati e a monitorare le prestazioni. Le metriche sono fondamentali perché supportano il processo decisionale e sono la base per il monitoraggio delle decisioni già prese.

Con numeri e grafici semplici e pratici, i membri di tutta l'organizzazione saranno in grado di seguire e sincronizzare gli sforzi relativi all'open source, rendendolo una preoccupazione e un'azione condivisa. Questo permette anche ai vari attori di entrare meglio nel corso, di contribuire al progetto e di ottenere i benefici complessivi.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa attività:

- [ ] È stato stabilito un elenco di metriche e di modalità di raccolta.
- [ ] Vengono utilizzati strumenti per raccogliere, memorizzare, elaborare e visualizzare gli indicatori.
- [ ] Tutti i partecipanti hanno a disposizione una dashboard generale che mostra i progressi dell'iniziativa.

### Strumenti

- [GrimoireLab](https://chaoss.github.io/grimoirelab) di Bitergia.
- Anche gli strumenti di BI generici (elasticsearch, grafana, visualizzazioni R/Python...) sono adatti, se i connettori appropriati sono impostati in base agli obiettivi definiti.

### Raccomandazioni

- Scrivere gli obiettivi e la roadmap della governance open source.
- Comunicare all'interno dell'azienda le azioni e lo stato dell'iniziativa.
- Coinvolgere le persone nella definizione dei KPI, per assicurarsi che
   - siano ben compresi,
   - forniscano una visione completa delle esigenze e delle
   - che vengano prese in considerazione e seguite.
- Costruire almeno una dashboard che possa essere visualizzata da tutti (ad esempio su uno schermo nella stanza), con indicatori essenziali per mostrare i progressi e la situazione generale.

### Risorse

- La [comunità CHAOSS](https://chaoss.community/) ha molti buoni riferimenti e risorse relative agli indicatori open source.
- Verificate le metriche per [Attributi del progetto](https://www.ow2.org/view/MRL/Stage2-ProjectAttributes) dalla [metodologia OW2 Market Readiness Levels](https://www.ow2.org/view/MRL/Overview).
- [A New Way of Measuring Openness: The Open Governance Index](https://timreview.ca/article/512) di Liz Laffan è una lettura interessante sull'apertura nei progetti open source.
- [Governance Indicators: A Users' Guide](https://anfrel.org/wp-content/uploads/2012/02/2007_UNDP_goveranceindicators.pdf) è la guida delle Nazioni Unite sugli indicatori di governance. Sebbene sia applicata alla democrazia, alla corruzione e alla trasparenza delle nazioni, vale la pena di leggere le basi della misurazione e degli indicatori applicati alla governance.
