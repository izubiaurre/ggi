## Appartenere alla comunità open source

Activity ID: [GGI-A-27](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_27.md).

### Descrizione

Questa attività mira a sviluppare tra gli sviluppatori un senso di appartenenza alla comunità open source. Come in ogni community, le persone e le entità devono partecipare e contribuire all'insieme. Rafforza i legami tra i professionisti e porta sostenibilità e attività all'ecosistema. Da un punto di vista più tecnico, permette di scegliere le priorità e la roadmap dei progetti, migliora il livello di conoscenza generale e di consapevolezza tecnica.

Questa attività copre quanto segue:

- **Identificare gli eventi** a cui vale la pena partecipare. Mettere in contatto le persone, conoscere le nuove tecnologie e creare una rete di contatti sono fattori chiave per ottenere tutti i vantaggi dell'open-source.
- Considerate **l'adesione a fondazioni**. Le fondazioni e le organizzazioni open-source sono una componente chiave dell'ecosistema open-source. Forniscono risorse tecniche e organizzative ai progetti e sono un buon luogo neutrale per gli sponsor per discutere problemi e soluzioni comuni o per lavorare sugli standard.
- Osservate i **gruppi di lavoro**. I gruppi di lavoro sono spazi di lavoro collaborativi neutrali in cui gli esperti interagiscono su un dominio specifico come l'IoT, la modellazione o la scienza. Sono un meccanismo molto efficiente ed economico per affrontare insieme problemi comuni, anche se specifici del dominio.
- **Partecipazione al bilancio**. In definitiva, il denaro è il catalizzatore. Pianificate le spese necessarie, date alle persone un tempo retribuito per queste attività, anticipate le azioni successive, in modo che il programma non debba fermarsi dopo pochi mesi per mancanza di fondi.

### Valutazione delle opportunità

L'open source funziona meglio quando è in relazione con la comunità open source in generale. Facilita la correzione di bug, la condivisione di soluzioni, ecc.

È anche un buon modo per le aziende di dimostrare il loro sostegno ai valori dell'open-source. Comunicare il coinvolgimento dell'azienda è importante sia per la sua reputazione che per l'ecosistema open-source.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa attività:

- [ ] È stato stilato un elenco di eventi a cui le persone potrebbero partecipare.
- [ ] È disponibile un monitoraggio delle conferenze pubbliche tenute dai membri del team.
- [ ] Le persone possono inviare richieste di partecipazione agli eventi.
- [ ] Le persone possono presentare progetti da sponsorizzare.

### Raccomandazioni

- Fate un sondaggio tra le persone per sapere quali sono gli eventi che preferiscono o che sarebbero più utili per il loro lavoro.
- Creare comunicazioni interne (newsletter, centro risorse, inviti...) in modo che le persone sappiano delle iniziative e possano partecipare.
- Assicuratevi che queste iniziative possano andare a beneficio di vari tipi di persone (sviluppatori, amministratori, supporto...), non solo dei dirigenti di livello dirigenziale (C-Level).

### Risorse

- [Cosa spinge uno sviluppatore a contribuire al software open source?](https://clearcode.cc/blog/why-developers-contribute-open-source-software/) Un articolo di Michael Sweeney su clearcode.cc.
- [Perché le aziende contribuiscono all'open source](https://blogs.vmware.com/opensource/2020/12/01/why-companies-contribute-to-open-source/) Un articolo di Velichka Atanasova di VMWare.
- [Perché i vostri dipendenti dovrebbero contribuire all'open source](https://www.cloudbees.com/blog/why-your-employees-should-be-contributing-to-open-source/) Una buona lettura di Robert Kowalski di CloudBees.
- [7 modi in cui la vostra azienda può sostenere l'open source](https://www.infoworld.com/article/2612259/7-ways-your-company-can-support-open-source.html) Un articolo di Simon Phipps per InfoWorld.
- [Eventi: la forza vitale dell'open source](https://www.redhat.com/en/blog/events-life-force-open-source) Un articolo di Donna Benjamin di RedHat.
