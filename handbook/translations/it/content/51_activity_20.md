## Software aziendale open source

ID Activity: [GGI-A-20](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_20.md).

### Descrizione

Questa attività riguarda la selezione proattiva di soluzioni OSS, sia di fornitori che di comunità, in aree orientate al business. Può anche riguardare la definizione di politiche di preferenza per la selezione di software applicativo aziendale open source.

Sebbene il software open source sia più spesso utilizzato dai professionisti dell'IT (sistema operativo, middleware, DBMS, amministrazione di sistema, strumenti di sviluppo), non è ancora stato riconosciuto nelle aree in cui i professionisti del business sono gli utenti principali.

L'attività riguarda aree quali: Suite per ufficio, ambienti di collaborazione, gestione degli utenti, gestione dei flussi di lavoro, gestione delle relazioni con i clienti, e-mail, e-Commerce, ecc.

### Valutazione delle opportunità

Man mano che l'open source tende a generalizzarsi si estende ben oltre i sistemi operativi e gli strumenti di sviluppo, trovando sempre più spazio negli strati superiori dei sistemi informativi, fino alle applicazioni aziendali. È importante identificare quali applicazioni OSS vengono utilizzate con successo per soddisfare le esigenze dell'organizzazione e come possono diventare la scelta preferita di un'organizzazione per risparmiare sui costi.

L'attività può comportare alcuni costi di riqualificazione e migrazione.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa attività:

- [ ] Esiste un elenco di soluzioni OSS consigliate per soddisfare le esigenze in sospeso nelle applicazioni aziendali.
- [ ] Viene redatta una politica di preferenza per la selezione di software applicativi aziendali open source.
- [ ] Le applicazioni aziendali proprietarie in uso vengono valutate rispetto agli equivalenti OSS.
- [ ] I processi di approvvigionamento e gli inviti a presentare proposte specificano la preferenza per l'open source (se legalmente possibile).

### Strumenti

Strumenti per la mappatura di software e applicazioni aziendali?

Al momento non siamo a conoscenza di strumenti rilevanti per questa attività.

### Raccomandazioni

- Parlate con i colleghi, imparate da ciò che fanno le altre aziende simili alla vostra.
- Visitate gli eventi di settore locali per scoprire le soluzioni OSS e il supporto professionale.
- Provate le edizioni della comunità e il supporto della comunità prima di impegnarvi in piani di supporto a pagamento.

### Risorse

- [Cos'è l'open source aziendale?](https://www.redhat.com/en/blog/what-enterprise-open-source): una rapida lettura sull'open source pronto per le imprese.
- [101 applicazioni open source per aiutare la vostra azienda a prosperare](https://digital.com/creating-an-llc/open-source-business/): Un elenco indicativo di soluzioni open source orientate al business.
