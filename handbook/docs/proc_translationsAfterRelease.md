# Purpose

This is to describe the process to perform after a new release of the handbook.

# Process

1. Create translated content files as they are on the repo (e.g. run: git fetch)
2. Run handbook/scripts/copyFromOriginal.sh.
3. Commit to translations branch.
4. Tag old translation status on translations branch.
5. Create MR of *content* files from branches main to translations. This excludes pot files!
6. Extract updated pot files from the changes from the MR, e.g. *2po.sh scripts, manual changes for images, etc.
7. Review pot files: string order in pot = source file, comments for translators (lines starting with #.)
8. Commit to translations branch
