#!/bin/bash
# by Silvério Santos for the OW2 Good Governance Initiative
# Usage: cd into the scripts folder and run this without any parameters
# Requires:
# bash packaqe

# Echoes a program header to standard output if help asked.
function echoHelp() {
    echo "copyFromOriginal.sh"
    echo ""
    echo "Copies original files as they are or creates symlinks into the translation subfolders"
    echo "Usage: cd into the scripts folder and run this command without parameters."
    echo ""
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
    echo ""
}

function processFiles() {
    # Languages configured as subfolders of translations
    declare -a langs=()
    for translatedLang in $(find ../translations -mindepth 1 -maxdepth 1 -type d); do
        langs+=($(basename "$translatedLang"))
    done
    echo "Files: resources/latex/title.tex, resources/images/ggi_deploy_activities.png, resources/images/ggi_deploy_website.png"
    for lang in "${langs[@]}";  do
        echo "- Processing language: $lang"
        mkdir -p ../translations/"$lang"/resources/latex/
        cp -u ../resources/latex/title.tex ../translations/"$lang"/resources/latex/
	mkdir -p ../translations/"$lang"/resources/images/
        cp -u ../resources/images/ggi_deploy_activities.png ../translations/"$lang"/resources/images/
        cp -u ../resources/images/ggi_deploy_website.png ../translations/"$lang"/resources/images/
    done
}

while [ $# -gt 0 ]; do
    case $1 in
        -h|--help)
            echoHelp
            exit 0
            ;;
        *)
            echo "Error: unknown parameter $1!"
            echoHelp  
            echo "Exiting."
            exit 1
    esac
done

processFiles
echo "Done."
exit 0
