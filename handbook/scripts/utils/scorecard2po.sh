#! /bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires:
#   Package translation toolkit (odf2xliff, xliff2po),
#   Package gettext (msgmerge, msginit),

# Global var: directory for temporary files
tmpdir=""
# Global var: Languages configured as subfolders of translations
declare -a langs=()

# Find directory for temporary files
function findTmpDir {
    if [ -n "$TMPDIR" ] && [ -d "$TMPDIR" ]; then
        # POSIX compatible
        tmpdir="$TMPDIR"
    elif [ -d /tmp ]; then
        # fixed
        tmpdir="/tmp"
    else
        echo "Error: no folder for temporary files found."
        exit 1
    fi
}

# Find languages configured as subfolders of translations
function findLanguages {
    while IFS= read -r -d '' translatedLang
    do
        langs+=($(basename "$translatedLang"))
    done <   <(find ../../translations -mindepth 1 -maxdepth 1 -type d -print0)
}

# Echoes a program header to standard output
function echoHeader() {
    echo "scorecard2po.sh"
    echo ""
    echo "Extracts/updates translatable strings from an .ODT file over a"
    echo "temporary XLIFF file into a POT file and into all found language's"
    echo "PO files for translation."
    echo "Usage: cd into the scripts folder and run this command without parameters."
    echo ""
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
    echo ""
}

# Checks POSIX compatible if the required commands are available.
function checkConditions() {
    if ! command -v odf2xliff &> /dev/null; then
        echo "Command odf2xliff not found, please install the translate-toolkit package."
        exit 2
    fi
    if ! command -v xliff2po &> /dev/null; then
        echo "Command xliff2po not found, please install the translate-toolkit package."
        exit 2
    fi
    if ! command -v msgmerge &> /dev/null; then
        echo "Command msgmerge not found, please install the gettext package."
        exit 2
    fi
    if ! command -v msginit &> /dev/null; then
        echo "Command msginit not found, please install the gettext package."
        exit 2
    fi
}

# Extracts/updates translatable strings from an .ODT file over a
# temporary XLIFF file into a POT file and into all found language's
# PO files for translation.
function odtToPoFile() {
    echo "File: $1"

    odf2xliff --progress=none -i ../../../resources/scorecards/"$1".odt  -o "$tmpdir"/"$1".xlf
    xliff2po --progress=none -i "$tmpdir"/"$1".xlf  -o ../../resources/pot_files/resources/"$1".pot -P

    for lang in "${langs[@]}";  do
        echo
        echo "Language: $lang"
        if [ -f ../../translations/"$lang"/po_files/resources/"$1".po ]; then
            msgmerge -U ../../translations/"$lang"/po_files/resources/"$1".po ../../resources/pot_files/resources/"$1".pot;
        else
            msginit -i ../../resources/pot_files/resources/"$1".pot -l "$lang"".UTF-8" --no-translator -o ../../translations/"$lang"/po_files/resources/"$1".po
        fi
    done
}

# End
function endPrg() {
    echo "Done."
    exit 0
}

echoHeader
findTmpDir
findLanguages
checkConditions
echo "Language(s) to process: ${langs[@]}"
odtToPoFile "Scorecard-Template-v0"
endPrg
