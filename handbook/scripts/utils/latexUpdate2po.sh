#! /bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires: 
#   Package po4a (po4a), 
#   Package gettext (msggrep, msgmerge), 

# Echoes a program header to standard output
function echoHelp() {
    echo "latexUpdate2po.sh"
    echo ""
    echo "Based on latexPo4a.cfg this extracts translatable strings from the "
    echo "configured tex source files, updates the po files in the configured "
    echo "languages and combines those to translated files (if 100% translated)."
    echo "Change latexPO4a.cfg for more languages, more LaTex files"
    echo ""
    echo "By Silvério Santos for the OW Good Governance Initiative."
    echo ""
}

# Checks POSIX compatible if the required commands are available.
function checkConditions() {
    if ! command -v po4a &> /dev/null; then
        echo "Command po4a not found, please install the po4a package."
        exit 1
    fi
    if ! command -v msggrep &> /dev/null; then
        echo "Command msggrep not found, please install the gettext package."
        exit 1
    fi
    if ! command -v msgmerge &> /dev/null; then
        echo "Command msgmerge not found, please install the gettext package."
        exit 1
    fi
}

# Script parameter handling
while [ $# -gt 0 ]; do
    case $1 in
        -h|--help)
            echoHelp
            exit 0
            ;;
        *)
            echo "Error: unknown parameter $1!"
            echoHelp
            echo "Exiting."
            exit 1
    esac
done

checkConditions
po4a --keep 0 -M UTF-8 --copyright-holder "OW2 and others" --msgid-bugs-address "ossgovernance@ow2.org" utils/latexPo4a.cfg
