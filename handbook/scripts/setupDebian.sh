#! /bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires:
#   Package bash
#   Package apt
#   Package dpkg

# Echoes a program header to standard output
function echoHeader {
    echo "setupDebian.sh"
    echo ""
    echo "Makes sure all required software is installed to create translated files."
    echo "Usage: cd into the scripts folder and run this command without parameters. The calling user should have sudo rights."
    echo ""
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
    echo "" 
}

# Checks if run as root and for required commands
function checkConditions {
    if ! command -v apt &> /dev/null; then
        echo "Error: Command apt not found. This should be default on Debian'ish distributions.."
        exit 1
    fi
    if ! command -v dpkg &> /dev/null; then
        echo "Error: Command dpkg not found. This should be default on Debian'ish distributions.."
        exit 1
    fi
}

# Install required packages
function installReqPks {
    # Try to install packages, previously installed ones are skipped/updated by APT
    echo
    echo "APT: update lists and install or update always required applications. Requires root rights."
    sudo apt-get  update \
        && sudo apt-get install -y \
        git gettext imagemagick translate-toolkit po4a default-jre \
        texlive-xetex pandoc pipx python3-pip python3-venv sed
    pipx ensurepath
    
    # Install LibreOffice Writer NOGUI only if the GUI version is NOT installed.
    echo
    echo "Checking if LibreOffice Writer NOGUI has to be installed and do so if needed"
    dpkg -s libreoffice-writer &> /dev/null
    if [ $? -ne 0 ]; then
        sudo apt install -y libreoffice-writer-nogui
    fi
    echo "OK"

    echo "Clean APT cache."
    apt-get clean

    # Install mdpo through pip
    echo
    echo "Checking if the po2md command exists and installs the mdpo through pip if needed"
    if ! command -v po2md &> /dev/null; then
        pipx install mdpo
    fi
    echo "OK"
}

# Changes configurations to enable the tools to work
function changeConfig {
    echo
    echo "Adapt ImageMagick's policy to allow converting to PDF"
    if [ -f /etc/ImageMagick-6/policy.xml ]; then
        if grep -q '<policy domain="coder" rights="none" pattern="PDF" />' /etc/ImageMagick-6/policy.xml; then
            sudo sed -i 's/\<policy domain\="coder" rights\="none" pattern\="PDF"/policy domain\="coder" rights\="read|write" pattern\="PDF"/g' /etc/ImageMagick-6/policy.xml
        fi
    fi
    echo "OK"
    
    echo
    echo "Checking if pip command po2md exists in user's ~/.local/bin, but that path is not in the PATH variable"
    if [ -f ~/.local/bin/po2md ]; then
        if [[ $PATH != */.local/bin* ]]
        then
            echo "Error: .local/bin is not in the PATH variable";
        fi
    fi
    echo "OK"
}

function echoSuggestions {
    echo
    echo "Your action might be required:"
    echo "To improve language specific tex support, you may want to install texlive-lang-[language] packages, e.g. by running sudo apt-get texlive-lang-[language] (where [language] is replaced by the full language name)."
}

# End
function endPrg {
    echo "Done."
    exit 0
}

echoHeader
checkConditions
installReqPks
changeConfig
echoSuggestions
endPrg
