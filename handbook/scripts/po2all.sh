#! /bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires: 
#  po2markdown.sh
#  tsltCvrImg.sh
#  tsltNtroImg.sh
#  latexUpdate2po.sh
#  po2scorecard.sh

# This is required to run in docker
export PATH=$PATH:~/.local/bin/

# Checks if the required scripts are available.
function checkConditions {
    if [ ! -x utils/po2markdown.sh ]; then
        echo "Error: cannot find the executable po2markdown.sh script.";
        exit 1;
    fi
    if [ ! -x utils/po2CvrImg.sh ]; then
        echo "Error: cannot find the executable po2CvrImg.sh script.";
        exit 1;
    fi
    if [ ! -x utils/po2MaslowImg.sh ]; then
        echo "Error: cannot find the executable po2MaslowImg.sh script.";
        exit 1;
    fi
    if [ ! -x utils/latexUpdate2po.sh ]; then
        echo "Error: cannot find the executable latexUpdate2po.sh script.";
        exit 1;
    fi
    if [ ! -x utils/po2scorecard.sh ]; then
        echo "Error: cannot find the executable po2scorecard.sh script.";
        exit 1;
    fi
}

# Calls all "output" scripts with supported parameters
function callScripts {
    params=""
    if [ -n "$lang" ]; then
	params="${params} -l $lang"
    fi
    if [ -n "$overwrite" ]; then
	params="${params} -o"
	echo
    fi
    
    if [ -n "$lang" ]; then 
	echo "Only language to be processed: $lang"
	if [[ -d "../translations/${lang}" ]]; then
	    echo "Checking if directory [../translations/${lang}] exists: OK."
	else
	    echo "Error: the translated files folder for the passed language does not exist."
	    exit 1
	fi
    fi

    echo "# Setting LANG."
    export LANG=$(locale -a | grep $lang | head -1)
    
    echo ""
    echo "# Executing po2markdown.sh $params"
    utils/po2markdown.sh $params
    if [ $? -ne 0 ]; then
	echo "Error code $? running po2markdown.sh."
	exit 1
    fi
    
    echo ""
    echo "# Executing po2CvrImg.sh $params"
    utils/po2CvrImg.sh $params
    if [ $? -ne 0 ]; then
	echo "Error code $? running po2CvrImg.sh."
	exit 1
    fi
    
    echo ""
    echo "# Executing po2MaslowImg.sh $params"
    utils/po2MaslowImg.sh $params
    if [ $? -ne 0 ]; then
	echo "Error code $? running po2MaslowImg.sh."
	exit 1
    fi
    
    echo ""
    echo "# Executing latexUpdate2po.sh"
    utils/latexUpdate2po.sh 
    if [ $? -ne 0 ]; then
	echo "Error code $? running latexUpdate2po.sh."
	exit 1
    fi
    
    echo ""
    echo "# Executing po2scorecard.sh $params"
    utils/po2scorecard.sh $params
    if [ $? -ne 0 ]; then
	echo "Error code $? running po2scorecard.sh."
	exit 1
    fi
	
}

# Echoes help
function echoHelp {
    echo "usage: po2all.sh [-h|--help] | [ [-l|--language LANGCODE] [-o|--overwrite]] ]"
    echo ""
    echo "Calls all specialized scripts to create their respective translated files."
    echo "When called with the parameter -l the result should be the converted md files, "
    echo "where untranslated strings stay in English."
    echo "Default behavior:"
    echo " - does not overwrite existing target files"
    echo " - iterates through all found languages resp. subfolders of the"
    echo "translations folder"
    echo
    echo "Cd into the scripts folder to run this command."
    echo
    echo "options:"
    echo "  -h, --help       Echoes this help message, then exits"
    echo "  -l, --language LANGCODE"
    echo "                   Limit processing to the language LANGCODE. Creates PDF."
    echo "  -o, --overwrite  Overwrite all target files."
    echo ""
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
}

# Script parameter handling
while [ $# -gt 0 ]; do
    case $1 in
        -h|--help)
            echoHelp
            exit 0
            ;;
        -l|--language)
            lang=$2
            shift # shift argument
            shift # shift value
            ;;
        -o|--overwrite)
            overwrite=1
            shift # shift argument
            ;;
        *)
            echo "Error: unknown parameter $1!"
            echoHelp  
            echo "Exiting."
            exit 1
    esac
done

checkConditions
callScripts
