#!/bin/sh

#
# This script uses a Docker container to generate all the required files to build
# the translated PDF for a specific language, and then creates it. If the container
# doesn't exist, it is automatically built.
#

# Parameter: language code (required)

docker_image=ggi-translation

if [ -z $1 ]; then
    echo "Parameter language code is required."
    exit 1
fi

lang=$1

# Build Docker image.
if docker image ls -q $docker_image | grep -q . ; then
    echo "# Docker image $docker_image exists, using it."    
else
    echo "# Docker image $docker_image doesn't exist, building it."
    docker build -t $docker_image -f ./Dockerfile-po2all --build-arg USER_ID=$(id -u) \
	   --build-arg GROUP_ID=$(id -g) .
fi

# Execute conversions and build PDF.
echo "# Compiling po files and building the PDF."
docker run -v $(dirname $(readlink -f $0))/../../:/ggi -w /ggi/handbook/scripts/ \
       --rm $docker_image bash convert_translation.sh $lang
# --user $(id -u):$(id -g) 

# Only for debugging:
#docker run -v $(dirname $(readlink -f $0))/../../:/ggi -ti $docker_image
