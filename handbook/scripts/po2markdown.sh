#! /bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires: 
#   Package bash
#   Package mdpo (po2md), 
#   File structure as created by markdown2po.sh

# Find languages configured as subfolders of translations
function findLanguages {
    if (( ${#langs[@]} == 0 )); then
        while IFS= read -r -d '' translatedLang
        do
            langs+=($(basename "$translatedLang"))
        done <   <(find ../translations -mindepth 1 -maxdepth 1 -type d -print0)
    else
        # Checking existence of the passed language's translated files folder
        if [ ! -d ../translations/"${langs[0]}" ]; then
            echo "Error: the translated files folder for the passed language does not exist."
            exit 1
        fi
    fi
}

# Echoes a program header to standard output
function echoHeader {
    echo "po2markdown.sh"
    echo "Add the option -h or --help for help."
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
    echo "----------------------------------------------------------"
}

# Checks POSIX compatible if the required commands are available.
function checkConditions {
    if ! command -v po2md &> /dev/null; then
        echo "Error: Command po2md not found, please install the mdpo package."
        exit 2
    fi
}

# Create folders for translated MD files, if not exist
function createFolders {
    echo "Language(s) to process:"
    for lang in "${langs[@]}";  do
        echo "$lang"
        if [ ! -d ../translations/"$lang"/content ]; then
            mkdir -p ../translations/"$lang"/content
        fi
        if [ ! -d ../translations/"$lang"/resources/html ]; then
            mkdir -p ../translations/"$lang"/resources/html
        fi
            if [ ! -d ../translations/"$lang"/resources/images ]; then
            mkdir -p ../translations/"$lang"/resources/images
        fi
        if [ ! -d ../translations/"$lang"/resources/latex ]; then
            mkdir -p ../translations/"$lang"/resources/latex
        fi
done
}

# Creates a translated MD file based on the language specific (translated) PO file and the original MD file
# Avoid overwriting except told to do so.
function poToMdFile {
    for lang in "${langs[@]}";  do
    	targetFile=../translations/"$lang"/content/"$1".md
    	if [ -f "$targetFile" ] && [ -z "$overwrite" ]; then
		    	# Skip this language if the file exists, but overwrite is not set  
	    	echo "The target file $targetFile exists, but the overwrite option is not set."
    	else 
        	po2md -q -w 0 --po-encoding UTF-8 -p ../translations/"$lang"/po_files/content/"$1".po -s "$targetFile" ../content/"$1".md
    	fi
    done
}

# Translates PO files as in subfolder translate into MD files
function translateContent {
    echo "Files:"
    for path in $(find ../resources/pot_files/content/ -type f -name '*.pot'); do
        echo "$(basename "${path}" .pot)"
        poToMdFile "$(basename "${path}" .pot)"
    done
}

# End
function endPrg {
    echo "Done."
    exit 0
}

# Echoes help
function echoHelp {
	echo "usage: po2markdown.sh [-h|--help] | [ [-l|--language LANGCODE] [-o|--overwrite]] ]"
	echo
    echo "Transforms all configured language's translations from .po files"
    echo "based on their original .md files into translated .md files."
	echo "Default behavior:"
	echo " - does not overwrite exisitng target files"
	echo " - iterates through all found languages resp. subfolders of the"
	echo "translations folder"
	echo
	echo "Cd into the scripts folder to run this command."
	echo
	echo "options:"
	echo "  -h, --help       Echoes this help message, then exits"
	echo "  -l, --language LANGCODE"
	echo "                   Limit processing to the language LANGCODE."
	echo "  -o, --overwrite  Overwrite target file."
}

# Global var: Languages configured as subfolders of translations
declare -a langs=()
# Global var: overwrite target file(s) 
overwrite="" 

# Script parameter handling
while [ $# -gt 0 ]; do
    case $1 in
        -h|--help)
            echoHelp
            exit 0
            ;;
        -l|--language)
            langs=("$2")
            shift # shift argument
            shift # shift value
            ;;
        -o|--overwrite)
            overwrite=1
            shift # shift argument
            ;;
        *)
        	echo "Error: unknown parameter $1!"
        	echoHelp
        	echo "Exiting."
        	exit 1
    esac
done

echoHeader
findLanguages
checkConditions
echo "Language(s) to process:"
for lang in "${langs[@]}";  do
    echo "$lang"
done
createFolders
translateContent
endPrg
