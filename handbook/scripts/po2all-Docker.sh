#!/bin/sh
# Parameter: language code (required)

if [ -z $1 ]; then
    echo "Parameter language code is required."
    exit 1
fi

docker build -t compile_ggi -f ./Dockerfile-po2all .
docker run -v $(dirname $(readlink -f $0))/../../:/ggi -w /ggi/handbook/scripts/ compile_ggi ./copyFromOriginal.sh&&./po2all.sh -l "$1"
# Only for debugging:
#docker run -v $(dirname $(readlink -f $0))/../../:/ggi -ti compile_ggi
