#!/bin/sh

#
# This script generates all the required files to build the translated PDF for a
# specific language, and then creates it.
#

# Parameter: language code (required)

if [ -z $1 ]; then
    echo "Parameter language code is required."
    exit 1
fi

lang=$1

pipx ensurepath

echo ""
echo "# Executing copyFromOriginal.sh"
bash ./copyFromOriginal.sh 

echo ""
echo "# Executing po2all.sh -o -l $lang"
bash ./po2all.sh -o -l $lang

echo ""
echo "# Converting handbook for $lang"
if [ -n "$lang" ]; then
    cd ../translations/"$lang"
    bash ../../scripts/convert_handbook.sh -m -o -l "$lang"
    if [ $? -ne 0 ]; then
	echo "Error code $? running convert_handbook.sh."
	exit 1
    fi
    cd - > /dev/null
fi
