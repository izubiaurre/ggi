#
msgid "Self-Actualization"
msgstr ""

msgid "Esteem"
msgstr ""

msgid "Love & Belonging"
msgstr ""

msgid "Safety"
msgstr ""

msgid "Physiological Needs"
msgstr ""

msgid "Abraham Maslow's Hierarchy of Behavioral Motives"
msgstr ""

msgid "Strategy"
msgstr ""

# Please add a backslash+n \n to the middle of the string, where the newline is
msgid ""
"Embracing the full potential of OSS. Proactively "
"using OSS for innovation and competetiveness."
msgstr ""

msgid "Engagement"
msgstr ""

# Please add a backslash+n \n to the middle of the string, where the newline is
msgid ""
"Engaging with the OSS ecosystem. Contributing "
"back. Developing visibility, event participation."
msgstr ""

msgid "Culture"
msgstr ""

# Please add a backslash+n \n to the middle of the string, where the newline is
msgid ""
"Implementing best practices. Developing OSS "
"culture. Sharing experience."
msgstr ""

msgid "Trust"
msgstr ""

# Please add a backslash+n \n to the middle of the string, where the newline is
msgid ""
"Securely and responsibly using OSS. Compliance "
"and dependency management policies."
msgstr ""

msgid "Usage"
msgstr ""

# Please add a backslash+n \n to the middle of the string, where the newline is
msgid ""
"Technically using OSS. Technical ability and "
"experience with OSS. Some OSS awareness."
msgstr ""

msgid "OW2 Goals to OSS Good Governance"
msgstr ""
