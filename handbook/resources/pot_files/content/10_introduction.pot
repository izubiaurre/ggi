#
msgid ""
msgstr ""
"Language: en\n"
"Content-Type: text/plain; charset=UTF-8\n"

#: ../content/10_introduction.md:block 1 (header)
msgid "Introduction"
msgstr ""

#: ../content/10_introduction.md:block 2 (paragraph)
msgid "This document introduces a methodology to implement professional management of open source software in an organisation. It addresses the need to use open source software properly and fairly, safeguard the company from technical, legal and IP threats, and maximise the advantages of open source. Wherever an organisation stands on these topics this document proposes guidance and ideas to move forward and make your journey a success."
msgstr ""

#: ../content/10_introduction.md:block 3 (header)
msgid "Context"
msgstr ""

#: ../content/10_introduction.md:block 4 (paragraph)
msgid "Most large end-users and systems integrators already use Free and Open-Source Software (FOSS) either in their information systems or product and service divisions. Open source compliance has become an ever-growing concern, and many large companies have established compliance officers. However, while sanitising a company's open-source production chain – which is what compliance is about – is fundamental, users *must* give back to communities and contribute to the sustainability of the open-source ecosystem. We see open source governance encompassing the whole ecosystem, engaging with local communities, nurturing a healthy relationship with open source software vendors and service specialists. This takes compliance to the next level, and this is what open source *good* governance is about."
msgstr ""

#: ../content/10_introduction.md:block 5 (paragraph)
msgid "This initiative goes beyond compliance and liability. It is about building awareness in communities of end-users (often software developers themselves) and systems integrators, and developing mutually beneficial relationships within the European FOSS ecosystem."
msgstr ""

#: ../content/10_introduction.md:block 6 (paragraph)
msgid "OSS Good Governance enables organisations of all types -- companies, small and large, city councils, universities, associations, etc. -- maximise the benefits derived from open source by helping them align people, processes, technology and strategy. And in this area, that of maximising the advantages of open source, especially in Europe, everyone is still learning and innovating, with nobody knowing where they actually stand regarding state of the art in the domain."
msgstr ""

#: ../content/10_introduction.md:block 7 (paragraph)
msgid "This initiative aims to help organisations achieve these goals with:"
msgstr ""

#: ../content/10_introduction.md:block 8 (unordered list)
msgid "A structured catalog of **activities**, a roadmap for the implementation of professional management of open source software."
msgstr ""

#: ../content/10_introduction.md:block 8 (unordered list)
msgid "A **management tool** to define, monitor, report and communicate about progress."
msgstr ""

#: ../content/10_introduction.md:block 8 (unordered list)
msgid "A **clear and practical path for improvement**, with small, affordable steps to mitigate risks, educate people, adapt processes, communicate inwards and outwards the organisation's realm."
msgstr ""

#: ../content/10_introduction.md:block 8 (unordered list)
msgid "**Guidance** and a range of **curated references** about open-source licensing, best practices, training, and ecosystem engagement to leverage open-source awareness and culture, consolidate internal knowledge and extend leadership."
msgstr ""

#: ../content/10_introduction.md:block 9 (paragraph)
msgid "This guide has been developed with the following requirements in mind:"
msgstr ""

#: ../content/10_introduction.md:block 10 (unordered list)
msgid "Any type of organisation is covered: from SMEs to large companies and not-for-profit organisations, from local authorities (e.g. town councils) to large institutions (e.g. European or governmental institutions). The framework provides building blocks for a strategy and hints for its realisation, but *how* the activities are executed depends entirely on the program's context and is up to the program manager. It may prove helpful to look for consulting services and to exchange with peers."
msgstr ""

#: ../content/10_introduction.md:block 10 (unordered list)
msgid "No assumption is made about the level of technical knowledge within the organisation or the domain of activity. For example, some organisations will need to set up a complete training curriculum, while others might simply propose ad-hoc material to the teams."
msgstr ""

#: ../content/10_introduction.md:block 11 (paragraph)
msgid "Some activities will not be relevant to all situations, but the whole framework still provides a comprehensive roadmap and paves the way for tailored strategies."
msgstr ""

#: ../content/10_introduction.md:block 12 (header)
msgid "About the Good Governance Initiative"
msgstr ""

#: ../content/10_introduction.md:block 13 (paragraph)
msgid "At OW2, an initiative is a joint effort to address a market need. The [OW2 OSS Good Governance Initiative](https://www.ow2.org/view/OSS_Governance) proposes a methodological framework to implement professional management of open source software within organisations."
msgstr ""

#: ../content/10_introduction.md:block 14 (paragraph)
msgid "The Good Governance initiative is based on a comprehensive model inspired by the popular Abraham Maslow's hierarchy of human needs and motivations, as illustrated by the picture below."
msgstr ""

#: ../content/10_introduction.md:block 15 (paragraph)
msgid "![Maslow and the GGI](resources/images/ggi_maslow.png)"
msgstr ""

#: ../content/10_introduction.md:block 16 (paragraph)
msgid "Through ideas, guidelines and activities the Good Governance initiative provides a blueprint for the implementation of organisational entities tasked with professional management of open source software, what is also called OSPO (for Open Source Program Offices). The methodology is also a management system to define priorities, and monitor and share progress."
msgstr ""

#: ../content/10_introduction.md:block 17 (paragraph)
msgid "As they implement the OSS Good Governance methodology, organisations will enhance their skills in a number of directions, including:"
msgstr ""

#: ../content/10_introduction.md:block 18 (unordered list)
msgid "**using** open source software properly and safely within the company to improve software reuse and maintainability and software development velocity;"
msgstr ""

#: ../content/10_introduction.md:block 18 (unordered list)
msgid "**mitigating** the legal and technical risks associated with external code and collaboration;"
msgstr ""

#: ../content/10_introduction.md:block 18 (unordered list)
msgid "**identifying** required training for teams, from developers to team leaders and managers, so everybody shares the same vision;"
msgstr ""

#: ../content/10_introduction.md:block 18 (unordered list)
msgid "**prioritizing** goals and activities, to develop an efficient open source strategy;"
msgstr ""

#: ../content/10_introduction.md:block 18 (unordered list)
msgid "**communicating** efficiently within the company and to the external world to make the most off the open source strategy;"
msgstr ""

#: ../content/10_introduction.md:block 18 (unordered list)
msgid "**improving** the organisation's competitiveness and attractiveness for top open source talents."
msgstr ""

#: ../content/10_introduction.md:block 19 (header)
msgid "About the OSPO Alliance"
msgstr ""

#: ../content/10_introduction.md:block 20 (paragraph)
msgid "The **OSPO Alliance** was launched by a coalition of leading European open source non-profit organizations, including OW2, Eclipse Foundation, OpenForum Europe, and Foundation for Public Code, with a mission to grow awareness for open source in Europe and globally and to promote the structured and professional management of open source by companies and administrations."
msgstr ""

#: ../content/10_introduction.md:block 21 (paragraph)
msgid "While the OW2 OSS Good Governance initiative is focused on developing a management methodology, the OSPO Alliance has the broader goal to help companies, particularly in non-technology sectors, and public institutions discover and understand open source, start benefiting from it across their activities and grow to host their own OSPOs."
msgstr ""

#: ../content/10_introduction.md:block 22 (paragraph)
msgid "The OSPO Alliance has established the **OSPO.Zone** website hosted at https://ospo.zone. Based on the OW2 Good Governance Initiative, OSPO.Zone is a repository for a comprehensive set of resources for corporations, public institutions, and research and academic organizations. OSPO.Zone enables the Alliance to connect with OSPOs across Europe and the world as well as with supportive community organizations. It encourages best practices and fosters contribution to the sustainability of the open source ecosystem. Check out the [OSPO Zone](https://ospo.zone) website for a quick overview of complementary frameworks of IT management best practices."
msgstr ""

#: ../content/10_introduction.md:block 23 (paragraph)
msgid "The [OSPO Zone](https://ospo.zone) website is also the place where we collect feedback about the initiative and its content (e.g. activities, body of knowledge) from the community at large."
msgstr ""

#: ../content/10_introduction.md:block 24 (header)
msgid "Contributors"
msgstr ""

#: ../content/10_introduction.md:block 25 (paragraph)
msgid "The following great people have contributed to the Good Governance Initiative:"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Frédéric Aatz (Microsoft France)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Boris Baldassari (Castalia Solutions, Eclipse Foundation)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Philippe Bareille (Ville de Paris)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Gaël Blondelle (Eclipse Foundation)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Vicky Brasseur (Wipro)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Philippe Carré (Nokia)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Pierre-Yves Gibello (OW2)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Michael Jaeger (Siemens)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Max Mehl (Free Software Foundation Europe)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Hervé Pacault (Orange)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Stefano Pampaloni (RIOS)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Christian Paterson (OpenUp)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Simon Phipps (Meshed Insights)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Silvério Santos (Orange Business Services)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Cédric Thomas, our master of ceremony (OW2)"
msgstr ""

#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Nicolas Toussaint (Orange Business Services)"
msgstr ""

#: ../content/10_introduction.md:block 27 (header)
msgid "Licence"
msgstr ""

#: ../content/10_introduction.md:block 28 (paragraph)
msgid "This work is licenced under a [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) licence (CC-BY 4.0). From the Creative Commons website:"
msgstr ""

#: ../content/10_introduction.md:block 29 (quote)
msgid "You are free to:"
msgstr ""

#: ../content/10_introduction.md:block 29 (quote)
msgid "Share it — copy and redistribute the material in any medium or format"
msgstr ""

#: ../content/10_introduction.md:block 29 (quote)
msgid "Adapt it — remix, transform, and build upon the material"
msgstr ""

#: ../content/10_introduction.md:block 29 (quote)
msgid "for any purpose, even commercially."
msgstr ""

#: ../content/10_introduction.md:block 29 (quote)
msgid "As long as you give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use."
msgstr ""

#: ../content/10_introduction.md:block 30 (paragraph)
msgid "All content is Copyright: 2020-2021 OW2 & The Good Governance Initiative participants."
msgstr ""

#~ msgid "content/10_introduction.md"
#~ msgstr ""
