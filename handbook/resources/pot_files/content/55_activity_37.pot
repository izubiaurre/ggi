#
msgid ""
msgstr ""
"Language: en\n"
"Content-Type: text/plain; charset=UTF-8\n"

#: ../content/55_activity_37.md:block 1 (header)
msgid "Open source enabling digital transformation"
msgstr ""

#: ../content/55_activity_37.md:block 2 (paragraph)
msgid "Link to GitLab issue: <https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/37>."
msgstr ""

#: ../content/55_activity_37.md:block 3 (header)
msgid "Description"
msgstr ""

#: ../content/55_activity_37.md:block 4 (quote)
msgid "\"Digital Transformation is the adoption of digital technology to transform services or businesses, through replacing non-digital or manual processes with digital processes or replacing older digital technology with newer digital technology.\" (Wikipedia)"
msgstr ""

#: ../content/55_activity_37.md:block 5 (paragraph)
msgid "When the most advanced organisations in Digital Transformation jointly drive change through their Business, IT and Finance to anchor digital in the way, they reconsider:"
msgstr ""

#: ../content/55_activity_37.md:block 6 (unordered list)
msgid "Business model: value chain with ecosystems, as a service, SaaS."
msgstr ""

#: ../content/55_activity_37.md:block 6 (unordered list)
msgid "Finance: opex/capex, people, outsourcing."
msgstr ""

#: ../content/55_activity_37.md:block 6 (unordered list)
msgid "IT: innovation, legacy/asset modernization."
msgstr ""

#: ../content/55_activity_37.md:block 7 (paragraph)
msgid "Open source is at the heart of digital transformation:"
msgstr ""

#: ../content/55_activity_37.md:block 8 (unordered list)
msgid "Technologies, Agile practices, product management."
msgstr ""

#: ../content/55_activity_37.md:block 8 (unordered list)
msgid "People: collaboration, open communication, development/decision cycle."
msgstr ""

#: ../content/55_activity_37.md:block 8 (unordered list)
msgid "Business models: try & buy, open innovation."
msgstr ""

#: ../content/55_activity_37.md:block 9 (paragraph)
msgid "In terms of competitiveness, the most visible processes are probably the processes that directly impact the customer experience. And we have to recognize that the big players, as well as start-up companies, by delivering totally unprecedented customer experience, drastically changed customer expectations."
msgstr ""

#: ../content/55_activity_37.md:block 10 (paragraph)
msgid "Customer experience as well as all the other processes within a company entirely depend on IT. Every company has to transform its IT, this is what the digital transformation is about. Companies that have not done it yet, have now to achieve their digital transformation as fast as possible, otherwise the risk is that they could be wiped out of the market. Digital Transformation is a condition for survival. Since the stakes are so high, a company cannot entirely leave the digital transformation to a supplier. Every company has to get hands on with its IT, which means that every company has to get hands on with open source software because there is no IT without open source software."
msgstr ""

#: ../content/55_activity_37.md:block 11 (paragraph)
msgid "Expected benefits of the digital transformation include:"
msgstr ""

#: ../content/55_activity_37.md:block 12 (unordered list)
msgid "Simplify, automate core processes, make them real-time."
msgstr ""

#: ../content/55_activity_37.md:block 12 (unordered list)
msgid "Enable fast responses to competitive changes."
msgstr ""

#: ../content/55_activity_37.md:block 12 (unordered list)
msgid "Take advantage of Artificial intelligence and big data."
msgstr ""

#: ../content/55_activity_37.md:block 13 (header)
msgid "Opportunity Assessment"
msgstr ""

#: ../content/55_activity_37.md:block 14 (paragraph)
msgid "Digital transformation could be managed by:"
msgstr ""

#: ../content/55_activity_37.md:block 15 (unordered list)
msgid "Segments of the IT: Production IT, Business Support IT (CRM, billing, procurement…), Support IT (HR, Finance, accounting...), Big Data."
msgstr ""

#: ../content/55_activity_37.md:block 15 (unordered list)
msgid "Type of technology or process supporting the IT: Infrastructure (cloud), Artificial Intelligence, Processes (Make-or-Buy, DevSecOps, SaaS)."
msgstr ""

#: ../content/55_activity_37.md:block 16 (paragraph)
msgid "Injecting open source in a particular segment or technology of your IT reveals that you want to get hands on in this segment or technology, because you assessed that this particular segment or technology of your IT is important for the competitiveness of your company. It is important to assess the position of your company compared not only with your competitors, but also with other industries, and key players in terms of customer experience and market solutions."
msgstr ""

#: ../content/55_activity_37.md:block 17 (header)
msgid "Progress Assessment"
msgstr ""

#: ../content/55_activity_37.md:block 18 (ordered list)
msgid "Level 1: Situation assessment"
msgstr ""

#: ../content/55_activity_37.md:block 19 (unordered list)
msgid "I have identified:"
msgstr ""

#: ../content/55_activity_37.md:block 19 (unordered list)
msgid "the segments of IT that are important for the competitiveness of my company, and"
msgstr ""

#: ../content/55_activity_37.md:block 19 (unordered list)
msgid "the open source technologies required to develop applications in these segments. And I have thus decided:"
msgstr ""

#: ../content/55_activity_37.md:block 19 (unordered list)
msgid "on which segments I want to manage in-house the development of projects, and"
msgstr ""

#: ../content/55_activity_37.md:block 19 (unordered list)
msgid "on which open source technologies I need to build in-house expertise."
msgstr ""

#: ../content/55_activity_37.md:block 20 (ordered list)
msgid "Level 2: Engagement"
msgstr ""

#: ../content/55_activity_37.md:block 21 (unordered list)
msgid "On some selected open source technologies used within the company, several developers have been trained and are recognized as valuable contributors by the open source community. In some selected segments, projects based upon open source technologies have been launched."
msgstr ""

#: ../content/55_activity_37.md:block 22 (ordered list)
msgid "Level 3: Generalisation"
msgstr ""

#: ../content/55_activity_37.md:block 23 (unordered list)
msgid "For all projects, an open source alternative is systematically being investigated during the inception stage of the project. To make it easier for the project team to study such open source alternative, a central budget, and a central team of architects, hosted in the IT Department, is dedicated to providing assistance to the projects."
msgstr ""

#: ../content/55_activity_37.md:block 24 (paragraph)
msgid "**KPIs**:"
msgstr ""

#: ../content/55_activity_37.md:block 25 (unordered list)
msgid "KPI 1. Ratio for which an open source alternative was investigated: (Number of projects / Total number of projects)."
msgstr ""

#: ../content/55_activity_37.md:block 25 (unordered list)
msgid "KPI 2. Ratio for which the open source alternative was chosen: (Number of projects / Total number of projects)."
msgstr ""

#: ../content/55_activity_37.md:block 26 (header)
msgid "Recommendations"
msgstr ""

#: ../content/55_activity_37.md:block 27 (paragraph)
msgid "Beyond the headline, Digital Transformation is a mindset that involves some fundamental changes, and this should also (or even mainly) come from the top-level layers of the organisation. Management shall promote initiatives, new ideas, manage risks, and potentially update existing procedures to make them fit new concepts."
msgstr ""

#: ../content/55_activity_37.md:block 28 (paragraph)
msgid "Passion is a huge factor of success. One of the means developed by key players in the field is to set up open spaces for new ideas, where people can submit, and freely work on, their ideas about digital transformation. Management should encourage such initiatives."
msgstr ""

#: ../content/55_activity_37.md:block 29 (header)
msgid "Resources"
msgstr ""

#: ../content/55_activity_37.md:block 30 (unordered list)
msgid "[Eclipse Foundation: Enabling Digital Transformation in Europe Through Global Open Source Collaboration](https://outreach.eclipse.foundation/hubfs/EuropeanOpenSourceWhitePaper-June2021.pdf)."
msgstr ""

#: ../content/55_activity_37.md:block 30 (unordered list)
msgid "[Europe: Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy)."
msgstr ""

#: ../content/55_activity_37.md:block 30 (unordered list)
msgid "[Europe: Open source software strategy 2020-2023](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf)."
msgstr ""

#~ msgid "content/55_activity_37.md"
#~ msgstr ""
