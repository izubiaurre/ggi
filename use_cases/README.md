# GGI Use cases

Use cases moved to the [Communication Task Force repository](https://gitlab.ow2.org/ggi/com-tf/) where they've been rebranded as [**OSPO Stories**](https://gitlab.ow2.org/ggi/com-tf/-/tree/main/resources/ospo_stories)
