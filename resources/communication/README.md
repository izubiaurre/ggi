
This folder holds various material to support the communication and dissemination of the Good Governance Initiative work.

All content stored here defaults to a CC-BY license, excepted when otherwise specified. Copyrights still apply of course, please use wisely.

## Content

* [Eclipse GGI introduction](eclipse_ggi_introduction.pdf) Used to introduce the GGI, last updated 2022-05-08.
* [Eclipse GGI presentation](eclipse_ggi_presentation.pdf) Used to present the GGI, last updated 2022-06-13.
