# Translating

---

## Quick results

(Note: LANGCODE is throughout this document a placeholder the language code of the targeted language, e.g. de for German, or fr for French.)

### TL;DR - I want to translate

In a web browser, open [https://hosted.weblate.org/projects/ospo-zone-ggi/#languages](https://hosted.weblate.org/projects/ospo-zone-ggi/#languages), login or create an account if needed, on the line with the language you will translate to, click on the figure in the "unfinished" column (coordinates: your language/"incomplete") and start translating,
[-> Translate](#Translate).

### TL;DR - I want to add a language to the translations

Log in to [Weblate](https://hosted.weblate.org) and make sure you have the languages you translate to configured in your [account's profile](https://hosted.weblate.org/accounts/profile/). Then go to the [ospo-zone-ggi project](https://hosted.weblate.org/projects/ospo-zone-ggi), click on the languages menu item and click the + sign in front of the language you want to add.
Then add the new language to the list for tex files:

- if you have write rights to the [translations repo](https://gitlab.ow2.org/ggi/ggi/-/tree/translations): add the language code in [`handbook\scripts\latexPo4a.cfg`](#LaTeX) and in [`handbook\scripts\markdown2po.sh`](#AddLangMarkdown), git commit and push.
- if you do not have write rights there: send an email to [OSS governance](mailto:ossgovernance@ow2.org) asking to do that for you.

### TL;DR - I want to create a PDF of the current translated status

Two options to build the translated PDFs: either natively or through Docker. For the native install, see the requirements in the [next section](#localSetup). We recommend using the Docker workflow, as described here:

- Install Docker.
- Open a terminal.
- `cd` into the handbook/scripts subfolder.
- Run `bash convert_translation_docker.sh -l LANGCODE`.

## Workflow

### <a name="localSetup"></a> Local setup

Make sure all the required software for each script from the handbook/scripts subfolder you intend to run is installed.

- System packages: Gettext, Imagemagick, Translation toolkit, LibreOffice, po4a, pip.
- Additionally for the PDF creation: Texlive Xetex, Pandoc, Python's Virtual Environment.
- Python/pip packages: mdpo

To have that easily done on Debian, be sure you have sudo rights, then run `handbook/scripts/setupDebian.sh`. Since installing software and changing configuration files require elevated rights, you might be asked for your password to approve them.

Depending on the translated languages, also install hyphenation files for LaTeX. On Debian they can be found under the naming style, if existing: `texlive-lang-<FULL-LANG>` (where FULL-LANG is the full language name, not the code), e.g. `texlive-lang-german` or `texlive-lang-french`. Use the command `sudo apt-get update && sudo apt-get install texlive-lang-<FULL-LANG>` accordingly.

This process has been tested on Debian and Ubuntu systems.

### <a name="Translate"></a>Translate

Translations are done on [Weblate](https://hosted.weblate.org/projects/ospo-zone-ggi). You can contribute without an account, but these translations are just regarded suggestions; to be confirmed by a logged in user. This means that you have to [create an account](https://docs.weblate.org/en/latest/user/profile.html) and/or login with your existing one to have your translation used.

### <a name="TranslateOnWeblate"></a> Add a new language to translate on Weblate

1. Open <https://hosted.weblate.org/projects/ospo-zone-ggi/#languages>. Create an account and/or authenticate if needed.
2. Request a new language.
3. Wait for the requested language to be (accepted and) set up.
4. Start translating on Weblate.

When the new language is set up in Weblate, GGI OW2's repo will be filled with the still untranslated po-files for all the following file types in a subfolder under translations named after the new language's symbol.
Also add the language to the [LaTeX translation configuration](#LaTeX) and [script to find strings in Markdown files and update POT and PO files](#AddLangMarkdown)

### Add a new language to translate locally

Since the main translation takes place on [Weblate](https://hosted.weblate.org/projects/ospo-zone-ggi) ([see here](#TranslateOnWeblate)), please make sure you have the most current updates from [OW2 GGI repo](https://gitlab.ow2.org/ggi/ggi/-/blob/translations/handbook/translations) AND not yet submitted and/or accepted translations on Weblate. Then translate the .po files and commit and push them into the GGI repo above.

#### <a name="AddLangMarkdown"></a> Add a language for content Markdown files

1. Edit the `markdown2po.sh` script from the handbook/scripts subfolder.
2. In the line `declare -a langs=("`, before the closing parenthesis `)` add a blank space followed by the new language code between 2 single quotes ', e.g ` 'pt'`.
3. Then [update translation files](#UpdTranslat).

#### Add a language for image files

1. Make sure the file structure for the language exists below `handbook/translations/LANGCODE`. If not, perform the steps in [Add a language for content Markdown files](#AddLangMarkdown)
2. Copy cover.pot and maslow.pot from handbook/resources/pot_files/resources/ to handbook/translations/*lang code*/po_files/resources/, renaming the extensions from .pot to .po .
3. Start translating the .po files.

#### <a name="LaTeX"></a>Add a language for LaTeX files

1. Make sure the file structure for the language exists. If not, perform the steps in [Add a language for content Markdown files](#AddLangMarkdown)
2. Open the latexPo4a.cfg configuration file in the handbook/scripts subfolder.
3. Append a blank space and the new 2 letter language code to the \[po4a_langs\] line.

### <a name="UpdTranslat"></a> Update translation files

#### Update content Markdown files

1. Cd into the handbook/scripts subfolder.
2. Run `./markdown2po.sh`.
3. Start translating the .po files.

#### Update LaTeX files

1. Cd into the handbook/scripts subfolder.
2. Run `./latexUpdate2po.sh`, the command both updates the strings to be translated from the source and tries to create the translated target file in one go.

#### Update OpenDocument Text files

1. Make sure the file structure for the language exists. If not, perform the steps in [Adding a new language to markdown files](#AddLangMarkdown)
2. Cd into the handbook/scripts subfolder and run `./scorecard2po.sh`

### Create translated target files

Notes for all the creation (`po2*.sh`) scripts.

- If you want to create the files and speed up the creation process by limiting it to only one language append a blank space, a -l or --language and the language code, e.g. `./po2all -l de` to create all for the German language.
- If you want to have up-to-date files created, either delete target files or append the parameter -o or --overwrite to the po2all.sh command.

#### Run all of the below scripts

1. Cd into the handbook/scripts subfolder.
  1. If you run a Debian OS run `./po2all.sh`. This also works on Windows' WSL.
  2. If you run a Linux OS, but not Debian, install Docker and then run `po2all-Docker.sh`.

When using the -l/--language parameter, the PDF will be created after all the `po2*.sh` scripts were called.

#### Create markdown files

Creates all the Markdown files in the handbook/translations/LANGCODE/content subfolder.

1. Cd into the handbook/scripts subfolder.
2. Run `./po2markdown.sh`.

#### Create image files

Creates the image files handbook/translations/LANGCODE/resources/latex/cover.png and handbook/translations/LANGCODE/resources/images/ggi_maslow.png

1. Cd into the handbook/scripts subfolder.
2. For the cover page run `./po2CvrImg.sh`.
3. For the Maslow image run `./po2MaslowImg,sh`.

#### Create LaTex files

Creates handbook/translations/LANGCODE/resources/latex/customisations.tex

1. Cd into the handbook/scripts subfolder.
2. Run `./latexUpdate2po.sh`, the command both updates the strings to be translated from the source and tries to create the translated target file in one go.

#### Create OpenDocument Text files

Creates customised_activity_scorecard_template.pdf in handbook/translations/LANGCODE/resources/latex/, through the steps Scorecard-Template-v0.odt and Scorecard-Template-v0.pdf

1. Cd into the handbook/scripts subfolder.
2. Run `./po2scorecard.sh`, the command creates translated ODT and PDF files in one go.

## General translation rules

- Use language the audience is used to and try to avoid anglicisms and latinisms to increase readability.
- Build a commonly accepted glossary the other translators orient on to use the same words for the same purpose and meaning.
- Write positively, try to avoid negations, they are less easy to understand.
