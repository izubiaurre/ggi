
# Maintainers

- Boris Baldassari (Eclipse Foundation)
- Catherine Nuel (OW2)
- Florent Zara (Eclipse Foundation)
- Fréderic Aatz (Microsoft France)
- Gerardo Lisboa (ESOP)
- Nicolas Toussaint (Orange Business)
- Pierre-Yves Gibello (OW2)
- Sébastien Lejeune (Thales)
- Silvério Santos (Orange Business)

