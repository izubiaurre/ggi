# Taskforces

There are several dedicated taskforces for contributing to GGI:
- Handbook taskforce
- Communication taskforce

They meet in the following room for video-conferences:
🎥 https://bbb.opencloud.lu/rooms/flo-jqs-nir-elb/join


## [Handbook taskforce](https://bbb.opencloud.lu/rooms/flo-jqs-nir-elb/join)

Leader(s): **Boris Baldassari**

Scheduling: **every 2 weeks on Tuesday 14:00** (in alternance with Consultancy)

- Fréderic Aatz 
- Boris Baldassari 
- Silona Bonewald 
- Gerardo Lisboa
- Sébastien Lejeune 
- Silvério Santos 
- Nicolas Toussaint 
- Igor Zubiaurre

## [Communication taskforce](https://bbb.opencloud.lu/rooms/flo-jqs-nir-elb/join)

Leader(s): **Catherine Nuel** 

Scheduling: **every 2 weeks on Thursday at 14:00**

- Fréderic Aatz 
- Valentina Del Prete
- Silona Bonewald 
- Catherine Nuel 
- Antonio Conti
- Florent Zara 
