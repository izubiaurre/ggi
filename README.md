# Good Governance Initiative

The Good Governance Initiative proposes a methodological framework to assess and improve open-source awareness, compliance and governance within organisations.

Organisations -- may they be small or big companies, city councils, or associations -- maximise the benefits derived from open-source when people, processes and technology are aligned and share the same vision.

This initiative helps organisations to achieve these goals with:
* An easy-to-understand **assessment program** to identify pain points and evaluate the organisation's current practices.
* A **clear and practical path for improvement**, with small, affordable steps to mitigate risks, educate people, adapt processes, communicate inwards and outwards the organisation's realm.
* **Guidance** and a breadth of **curated references** about open-source licencing, practices, training, and ecosystems, to leverage open-source awareness and culture, consolidate internal knowledge and extend leadership.
* A **certifying process** to help you select the right partners and show your commitment to the world.


## Governance

The OSPO Alliance Good Governance Initiative works as an open-source project, and is based on transparency, openness and meritocracy. People get maintainers privileges when they are actively contributing, and participate fairly to the calls. Governance is specified in the [CHARTER.md](https://gitlab.ow2.org/ggi/ggi/-/blob/main/CHARTER.md) document.

The current list of maintainers can be found in file [MAINTAINERS.md](https://gitlab.ow2.org/ggi/ggi/-/blob/main/MAINTAINERS.md).


## Content

In the course of this program you will learn:
* How to **properly and safely use open source software** within the company to improve re-use, maintainability and velocity of development teams.
* About the **legal and technical risks** associated with external code and collaboration, and **how to mitigate them**.
* About the required **training for teams**, from developers to team leaders and managers, so everybody shares the same, unified vision.
* How to **build up your own strategy**, define goals and setup the required processes within the organisation, with real-world use cases and examples, from top experts of the domain.
* How to **properly communicate** within the company and to the external world so as to make the most of your strategy.
* How to **improve competitiveness and attractiveness** for your organisation through open-source usage and evangelism.


## Deployment

The framework uses **Activities** to identify practices relevant to OSS Good Governance.

Please go to [the Level Board](https://gitlab.ow2.org/ggi/ggi-castalia/-/boards/432), or to the [Roles Board](https://gitlab.ow2.org/ggi/ggi-castalia/-/boards/433).

## Collaborative tooling

As a diverse and geographically distributed community, we heavily rely on collaborative tools for our work. Although most of the work happens in this GitLab group, including all its projects, there are a few utilities elsewhere, all listed below:

* GGI main workplace is this very repository: https://gitlab.ow2.org/ggi/ggi
* GGI deployment feature: https://gitlab.ow2.org/ggi/my-ggi-board
* Weblate hosting for translations: https://hosted.weblate.org/projects/ospo-zone-ggi/
* Jenkins server for CI builds: https://karl.castalia.camp/job/GGI%20Handbook/
* OSPO Alliance mailing list: https://accounts.eclipse.org/mailing-list/ospo.zone
* GGI Mailing list: https://mail.ow2.org/wws/info/ossgovernance

We are also using several pads to handle the different taskforces involved into GGI and to share all the meeting minutes:
* OSPO Alliance GGI pads: https://pad.castalia.camp/mypads/?/mypads/group/ggi-ri40n2d/view
* Meeting minutes pads: https://pad.castalia.camp/mypads/?/mypads/group/ggi-minutes-zz1b0nmd/view

Contributions are organized through dedicated [taskforces](https://gitlab.ow2.org/ggi/ggi/-/blob/main/TASKFORCES.md).

## Translations

The GGI handbook is translated using [Weblate](https://hosted.weblate.org/projects/ospo-zone-ggi/), that offers free hosting for open source projects. We highly appreciate your commitment to the open source community.
